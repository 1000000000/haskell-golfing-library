{-# Language PatternSynonyms #-}
{-|
Module :
  P
Description :
  A substitution for Prelude aimed at making programs as small as possible for code golf.
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A substitution for Haskell's Prelude aimed at making programs as small as possible for code golf.

Please do not use this in real code.
-}
module P
  ( pattern (:=)
  , ln
  , ul
  , ix
  , fX
  , rp
  , rl
  , (#*)
  , frl
  , rl2
  , rl3
  , rl4
  , rl5
  , rl6
  , rl7
  , rl8
  , rl9
  , rl0
  , rl1
  , is
  , fis
  , ic
  , fic
  , ss
  , sh
  , cn
  , fcn
  , ce
  , fce
  , nb
  , nW
  , fnW
  , nB
  , fnB
  , or
  , sm
  , pd
  , eu
  -- * Functions
  , yy
  -- ** On
  , on
  , fon
  , (.*)
  , pOn
  , fpO
  , qOn
  , fqO
  , kOn
  , fkO
  , onp
  -- ** Identity
  , id
  , ($)
  , fid
  , (&)
  -- ** Flips
  , pattern F
  , ff
  , pattern F2
  , fF2
  -- * Booleans
  , Bool
  , pattern T
  , pattern B
  , n
  , iF
  , (?!)
  , fiF
  , (!?)
  -- * Tuples
  , st
  , nd
  , pattern Bp
  , pattern Pb
  , jbp
  , pattern Jbp
  , pattern Sw
  -- * Currying
  , pattern Cu
  , fCu
  , pattern Cuf
  , fCf
  , pattern Uc
  , fUc
  , pattern Ucf
  , fUf
  , pattern Cu3
  , fC3
  , pattern Uc3
  , fU3
  -- * Lists
  , List
  , pattern (:>)
  , pattern K
  , pattern Fk
  , dr
  , (#>)
  , fd
  , dr2
  , dr3
  , dr4
  , dr5
  , dr6
  , dr7
  , dr8
  , dr9
  , dr0
  , dr1
  , tl
  , nt
  , sA
  , (#=)
  , fsA
  , dW
  , fdW
  , sp
  , bk
  , sy
  , sl
  , sY
  , (|/)
  , sL
  , (|\)
  , rv
  , pattern Rv
  , pm
  , ƪ
  , pt
  , pa
  , (%%)
  , fpa
  , paf
  , paF
  , pA
  , δ
  , δ'
  , xQ
  , xX
  , r1
  , fr1
  , aL
  , faL
  , pattern (:%)
  , uaL
  , aL'
  , aL3
  , aL4
  , aL5
  , aL6
  , ns
  , fns
  , rn
  , frn
  , gr
  , gW
  , fgW
  , gB
  , fgB
  -- ** Indexing
  , (!)
  , (!!)
  , im
  , (>!)
  , fim
  , iM
  , (!<)
  , fiM
  -- ** Intersections and Differences
  -- | See also 'cX' and 'fcX' in MonadPlus.
  , nx
  , fnx
  , nxw
  , nxW
  , nxb
  , nxB
  , bI
  , fbI
  , bIw
  , bIW
  , bIb
  , bIB
  , df
  , fdf
  , dfw
  , dfW
  , dfb
  , dfB
  , bD
  , fbD
  , bDw
  , bDW
  , bDb
  , bDB
  -- ** Prefixes
  , px
  , pX
  , ƥ
  , mP
  , sw
  , (%<)
  , fsw
  , nsw
  , swW
  , fSW
  , swB
  , fSB
  , lp
  , lpw
  , lpb
  , tk
  , (#<)
  , ft
  , tk1
  , tk2
  , tk3
  , tk4
  , tk5
  , tk6
  , tk7
  , tk8
  , tk9
  , tk0
  , tW
  , ftW
  -- ** Suffixes
  , sx
  , sX
  , ms
  , mS
  , ew
  , (>%)
  , few
  , new
  , ewW
  , fEW
  , ewB
  , fEB
  , lx
  , lsw
  , lsb
  , yv
  , fY
  , yv1
  , yv2
  , yv3
  , yv4
  , yv5
  , yv6
  , yv7
  , yv8
  , yv9
  , yv0
  , xh
  , fxh
  -- ** Infixes
  , cg
  , iw
  , (%=)
  , fiw
  , iwW
  , fIW
  , iWB
  , fIB
  , ih
  , fih
  -- * Zips
  , zW
  , fzW
  , zp
  , fzp
  , uz
  , z3
  , fz3
  , ź
  , fZ3
  , zD
  , zd
  , fzd
  , zd'
  , zdm
  , fZm
  -- * Mostly internal
  , Zip (..)
  , liZ
  -- * Foldable
  , lF
  , flF
  , lF'
  , fLF
  , fo
  , mF
  , fmF
  , mf
  , fmf
  , rF
  , frF
  , rH
  , frH
  , lH
  , flH
  , l
  , ø
  , e
  , (?>)
  , fe
  , ne
  , (?<)
  , fE
  , ay
  , fay
  -- ** Scans
  , lS
  , flS
  , rS
  , frS
  , ls
  , fls
  , rs
  , frs
  , lz
  , flz
  , rz
  , frz
  -- ** Minimums and Maximums
  , mx
  , mn
  , xW
  , fxW
  , mW
  , fmW
  , xB
  , fxB
  , mB
  , fmB
  -- ** Finds
  , gN
  , fgN
  , g1
  , fg1
  , g2
  , fg2
  , g3
  , fg3
  , g4
  , fg4
  , g5
  , fg5
  , g6
  , fg6
  , g7
  , fg7
  , g8
  , fg8
  , g9
  , fg9
  , g0
  , fg0
  -- * Eq
  , Eq
  , (==)
  , eq
  , (/=)
  , nq
  , qb
  , nqb
  , Eq1
  , q1
  -- * Ord
  , Ord
  , (>)
  , gt
  , (>=)
  , ge
  , (<.)
  , lt
  , (<=)
  , le
  , Ordering (..)
  , cp
  , fcp
  , cb
  , pattern RO
  , pattern RC
  -- ** Sorting
  , sr
  , rsr
  , sW
  , fsW
  , rsW
  , fRW
  , sB
  , fsB
  , rsB
  , fRB
  -- * Matrices
  , pattern Tp
  , mC
  -- * Arithmetic
  , Int
  , Prelude.Integer
  , Num
  , Prelude.Integral
  , (+)
  , pl
  , pattern P1
  , pattern P2
  , pattern P3
  , pattern P4
  , pattern P5
  , pattern P6
  , pattern P7
  , pattern P8
  , pattern P9
  , pattern P0
  , (-)
  , sb
  , pattern S1
  , pattern S2
  , pattern S3
  , pattern S4
  , pattern S5
  , pattern S6
  , pattern S7
  , pattern S8
  , pattern S9
  , pattern S0
  , (*)
  , ml
  , db
  , tp
  , m4
  , m5
  , m6
  , m7
  , m8
  , m9
  , m0
  , m1
  , (^)
  , pw
  , fpw
  , pw2
  , pw3
  , pw4
  , pw5
  , pw6
  , pw7
  , pw8
  , pw9
  , pw0
  , pw1
  , sq
  , (%)
  , (//)
  , dv
  , fdv
  , vD
  , fvD
  , av
  , aD
  , (-|)
  , aD1
  , aD2
  , aD3
  , aD4
  , aD5
  , aD6
  , aD7
  , aD8
  , aD9
  , aD0
  -- ** Patterns
  , pattern X
  , pattern N
  , pattern NP
  , pattern NN
  , pattern NZ
  -- ** Divisibility and Primes
  , by
  , fby
  , (|?)
  , nby
  , (/|?)
  , fnb
  , a'
  , i'
  , pattern I'
  -- ** Base Conversion
  , hx
  , bs
  , fbs
  , bS
  , fbS
  , bi
  -- * Constants
  , nn
  , nN
  , β
  -- * Enum
  , Enum
  , pattern Sc
  , pattern Pv
  , eF
  , eF2
  , eF3
  , eF4
  , eF5
  , eF6
  , eF7
  , eF8
  , eF9
  , eF0
  , eF1
  , ef
  , (##)
  , fef
  , e0
  , e1
  , e2
  , e3
  , e4
  , e5
  , e6
  , e7
  , e8
  , e9
  , et0
  , et1
  , et2
  , et3
  , et4
  , et5
  , et6
  , et7
  , et8
  , et9
  -- * Semigroup and Monoid
  , Semigroup
  , Monoid
  , (<>)
  , mp
  , fmp
  , i
  , cy
  , rt
  , frt
  , rt2
  , rt3
  , rt4
  , rt5
  , rt6
  , rt7
  , rt8
  , rt9
  , rt0
  , rt1
  -- ** Min and Max
  , Min (..)
  , Max (..)
  -- ** First and Last
  , First (..)
  , Last (..)
  -- * Char
  , Prelude.Char
  , iC
  , pattern IC
  , iW
  , pattern IW
  , iL
  , pattern IL
  , iU
  , pattern IU
  , α
  , pattern Α
  , iA
  , pattern IA
  , iP
  , pattern IP
  , pattern TU
  , mtU
  , pattern TL
  , mtL
  , pattern Or
  , pattern Ch
  -- * Functor
  , Functor
  , m
  , fm
  , (<)
  , pM
  , (<$)
  , fpM
  , ($>)
  , mm
  , (.^)
  , fM
  , mX
  , (^.)
  , fmX
  , m'
  , fm'
  , mof
  , fMF
  , (.<)
  , m3
  , (.^^)
  , fm3
  , zz
  , (<.<)
  , fzz
  , z2
  , (<.^)
  , fz2
  , mi
  , mI
  , mcm
  , fMM
  , mc2
  , (.!<)
  , fM2
  , dcp
  , fdC
  -- ** Bifunctor
  , bm
  , fbm
  , jB
  , fjB
  , (&@)
  , pjB
  , fpJ
  , qjB
  , fqJ
  -- ** Identity
  , Identity
  , pattern UI
  , pattern Pu
  , pattern Li2
  , pattern UL2
  , pattern La2
  , pattern L2'
  , pattern Ul2
  -- * Applicative
  , Applicative
  , p
  , ap
  , (*^)
  , aT
  , (*>)
  , faT
  , (<*)
  , fA
  , l2
  , fl2
  , (**)
  -- * Alternative
  , Alternative
  , em
  , (++)
  , ao
  , fao
  , so
  , my
  , sO
  , mY
  , gu
  , cx
  , cs
  , (<|)
  , fcs
  , ec
  , (|>)
  , fec
  -- * Monad
  , Prelude.Monad
  , bn
  , (>~)
  , fb
  , (~<)
  , (+>)
  , (<+)
  , jn
  , fjn
  , jj
  , fjj
  , mM
  , fmM
  , m_
  , fm_
  , vd
  -- ** MonadPlus
  -- | No MonadPlus type class has been implemented currently, since it is mostly redundant.
  -- Here we have functions that require both 'Control.Applicative.Alternative' and 'Monad'.
  -- Currently just variants of 'Control.Monad.mfilter'.
  , wh
  , fl
  , (@~)
  , wn
  , fn
  , (@!)
  , er
  , fer
  , (=-)
  , cX
  , fcX
  -- * IO
  , IO
  , io
  -- ** Input
  , gC
  , gO
  , gl
  , gL
  , ga
  -- ** Output
  , ps
  , pS
  , pC
  , pO
  , pr
  , pW
  , fpW
  -- * Parsing
  , Parser
  -- ** Perform a parse
  , pP
  , (-@)
  , cP
  , (-#)
  , x1
  , (-$)
  , gP
  , (-%)
  , gc
  , (-^)
  -- ** Parser combinators
  , lA
  , hd
  , h_
  , h'
  , kB
  , χ
  , x_
  , x'
  , dg
  , al
  , wd
  , ʃ
  , s_
  , s'
  , ν
  -- ** Pre-made Parsers
  , χa
  , ʃa
  , χb
  , ʃb
  , χc
  , ʃc
  , χd
  , ʃd
  , χe
  , ʃe
  , χf
  , ʃf
  , χg
  , ʃg
  , χh
  , ʃh
  , χi
  , ʃi
  , χj
  , ʃj
  , χk
  , ʃk
  , χl
  , ʃl
  , χm
  , ʃm
  , χn
  , ʃn
  , χo
  , ʃo
  , χp
  , ʃp
  , χq
  , ʃq
  , χr
  , ʃr
  , χs
  , ʃs
  , χt
  , ʃt
  , χu
  , ʃu
  , χv
  , ʃv
  , χw
  , ʃw
  , χx
  , ʃx
  , χy
  , ʃy
  , χz
  , ʃz
  , χA
  , ʃA
  , χB
  , ʃB
  , χC
  , ʃC
  , χD
  , ʃD
  , χE
  , ʃE
  , χF
  , ʃF
  , χG
  , ʃG
  , χH
  , ʃH
  , χI
  , ʃI
  , χJ
  , ʃJ
  , χK
  , ʃK
  , χL
  , ʃL
  , χM
  , ʃM
  , χN
  , ʃN
  , χO
  , ʃO
  , χP
  , ʃP
  , χQ
  , ʃQ
  , χR
  , ʃR
  , χS
  , ʃS
  , χT
  , ʃT
  , χU
  , ʃU
  , χV
  , ʃV
  , χW
  , ʃW
  , χX
  , ʃX
  , χY
  , ʃY
  , χZ
  , ʃZ
  , χ0
  , ʃ0
  , χ1
  , ʃ1
  , χ2
  , ʃ2
  , χ3
  , ʃ3
  , χ4
  , ʃ4
  , χ5
  , ʃ5
  , χ6
  , ʃ6
  , χ7
  , ʃ7
  , χ8
  , ʃ8
  , χ9
  , ʃ9
  , χ_
  , ʃ_
  , χ'
  , ʃ'
  , cls
  , opn
  -- * Types
  , String
  -- * Deprecated
  -- | Exports long form versions of functions.
  -- Using these will produce a warning with the name of the short function to use.
  , module P.Deprecated
  )
  where

import qualified Prelude
import Prelude
  ( Num
  , Integral
  , Enum
  , String
  , Int
  )
import qualified Data.List

import P.Aliases
import P.Alternative
import P.Applicative
import P.Arithmetic
import P.Arithmetic.Pattern
import P.Arithmetic.Primes
import P.Bool
import P.Char
import P.Deprecated
import P.Enum
import P.Eq
import P.Foldable
import P.Foldable.MinMax
import P.Function
import P.Function.Curry
import P.Function.Flip
import P.Function.Identity
import P.Functor
import P.Functor.Bifunctor
import P.Functor.Compose
import P.Functor.Identity
import P.IO
import P.Intersection
import P.List
import P.List.Index
import P.List.Infix
import P.List.Prefix
import P.List.Sort
import P.List.Suffix
import P.Matrix
import P.Monad
import P.Monoid
import P.Monoid.MinMax
import P.Monoid.FirstLast
import P.Ord
import P.Parse
import P.Parse.Extra
import P.Pattern
import P.Show
import P.Tuple
import P.Zip

-- |
-- Equivalent to 'Data.List.lines'.
ln ::
  (
  )
    => String -> List String
ln =
  Data.List.lines

-- |
-- Equivalent to 'Data.List.unlines'.
ul ::
  (
  )
    => List String -> String
ul =
  Data.List.unlines

-- |
-- Equivalent to 'Data.List.iterate'.
ix ::
  (
  )
    => (a -> a) -> a -> List a
ix =
  Data.List.iterate

-- | Flip of 'ix'.
fX ::
  (
  )
    => a -> (a -> a) -> List a
fX =
  F ix

-- | Takes a element and makes an infinite list consisting of only that element.
-- Equivalent to 'Data.List.repeat'.
rp ::
  (
  )
    => a -> List a
rp =
  Data.List.repeat

-- | The fixed point of some element of a 'Semigroup'.
--
-- Takes an element of a semigroup @m@
-- and combines an infinite number of @m@s using '(<>)'.
--
-- It is right associative, so:
--
-- @
-- cy m =
--   m <> (m <> (m <> ...))
-- @
--
-- More general version of 'Data.List.cycle'.
--
-- ==== __Examples__
--
-- On list this behaves like 'Data.List.cycle':
--
-- >>> tk 10 $ cy [1,2,3]
-- [1,2,3,1,2,3,1,2,3,1]
--
cy ::
  ( Semigroup m
  )
    => m -> m
cy =
  yy < mp

-- | Takes a size @n@ and an element @x@ and makes a list of length @n@ containing only @x@s.
--
-- Equivalent to 'Data.List.genericReplicate'.
rl ::
  ( Integral i
  )
    => i -> a -> List a
rl =
  Data.List.genericReplicate

-- | Infix version of 'rl'.
(#*) ::
  ( Integral i
  )
    => i -> a -> List a
(#*) =
  rl

-- | Flip of 'rl'.
frl ::
  ( Integral i
  )
    => a -> i -> List a
frl =
  F rl

-- | Take an element @a@ and make a list of length 2 of only @a@s.
-- Shorthand for @'rl' 2@
rl2 ::
  (
  )
    => a -> List a
rl2 =
  rl (2 :: Int)

-- | Take an element @a@ and make a list of length 3 of only @a@s.
-- Shorthand for @'rl' 3@
rl3 ::
  (
  )
    => a -> List a
rl3 =
  rl (3 :: Int)

-- | Take an element @a@ and make a list of length 4 of only @a@s.
-- Shorthand for @'rl' 4@
rl4 ::
  (
  )
    => a -> List a
rl4 =
  rl (4 :: Int)

-- | Take an element @a@ and make a list of length 5 of only @a@s.
-- Shorthand for @'rl' 5@
rl5 ::
  (
  )
    => a -> List a
rl5 =
  rl (5 :: Int)

-- | Take an element @a@ and make a list of length 6 of only @a@s.
-- Shorthand for @'rl' 6@
rl6 ::
  (
  )
    => a -> List a
rl6 =
  rl (6 :: Int)

-- | Take an element @a@ and make a list of length 7 of only @a@s.
-- Shorthand for @'rl' 7@
rl7 ::
  (
  )
    => a -> List a
rl7 =
  rl (7 :: Int)

-- | Take an element @a@ and make a list of length 8 of only @a@s.
-- Shorthand for @'rl' 8@
rl8 ::
  (
  )
    => a -> List a
rl8 =
  rl (8 :: Int)

-- | Take an element @a@ and make a list of length 9 of only @a@s.
-- Shorthand for @'rl' 9@
rl9 ::
  (
  )
    => a -> List a
rl9 =
  rl (9 :: Int)

-- | Take an element @a@ and make a list of length 10 of only @a@s.
-- Shorthand for @'rl' 10@
rl0 ::
  (
  )
    => a -> List a
rl0 =
  rl (10 :: Int)

-- | Take an element @a@ and make a list of length 11 of only @a@s.
-- Shorthand for @'rl' 11@
--
-- For @rl 1@ see 'p'.
rl1 ::
  (
  )
    => a -> List a
rl1 =
  rl (11 :: Int)

-- | Gives all subsequences of a given list.
--
-- Equivalent to 'Data.List.subsequences'.
ss ::
  (
  )
    => List a -> List (List a)
ss =
  Data.List.subsequences

-- | Counts the number of elements of a structure satisfy a predicate.
cn ::
  ( Foldable t
  , Integral i
  )
    => Predicate a -> t a -> i
cn sig =
  rF (\ a count -> if sig a then count + 1 else count) 0

-- | Flip of 'cn'.
fcn ::
  ( Foldable t
  , Integral i
  )
    => t a -> Predicate a -> i
fcn =
  F cn

-- | Counts the number of times an element appears in a structure.
ce ::
  ( Foldable t
  , Eq a
  , Integral i
  )
    => a -> t a -> i
ce =
  cn < (==)

-- | Flip of 'ce'.
fce ::
  ( Foldable t
  , Eq a
  , Integral i
  )
    => t a -> a -> i
fce =
  F ce

-- | Removes duplicate occurences from a list after the first.
--
-- Equivalent to 'Data.List.nub'.
nb ::
  ( Eq a
  )
    => List a -> List a
nb =
  Data.List.nub

-- |
-- Equivalent to 'Data.List.nubBy'.
nW ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a
nW =
  Data.List.nubBy

-- | Flip of 'nW'.
fnW ::
  (
  )
    => List a -> (a -> a -> Bool) -> List a
fnW =
  F nW

-- | Takes a conversion function and a list.
-- Nubs the list as if the values were the result of the conversion function.
--
-- ==== __Examples__
--
-- We can use @nB@ with 'tk1' on a word list to get the first word of each letter.
--
-- >>> nB tk1 ["aardvark", "apple", "beard", "beaver", "cat", "chord"]
-- ["aardvark","beard","cat"]
nB ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a
nB =
  nW < qb

-- | Flip of 'nB'.
fnB ::
  ( Eq b
  )
    => List a -> (a -> b) -> List a
fnB =
  F nB

-- |
-- Equivalent to 'Prelude.or'.
or ::
  ( Foldable t
  )
    => t Bool -> Bool
or =
  Prelude.or

-- |
-- Equivalent to 'Data.List.sum'.
sm ::
  ( Foldable t
  , Num a
  )
    => t a -> a
sm =
  Data.List.sum

-- |
-- Equivalent to 'Data.List.product'.
pd ::
  ( Foldable t
  , Num a
  )
    => t a -> a
pd =
  Data.List.product

-- | Takes a list and pairs each element with it's index.
eu ::
  ( Integral i
  )
    => List a -> List (i, a)
eu =
  zp nn
