module P.Applicative
  ( Applicative
  , p
  , ap
  , (*^)
  , aT
  , (*>)
  , faT
  , (<*)
  , fA
  , l2
  , fl2
  , (**)
  )
  where

-- Needed for documentation
import qualified Prelude
import qualified Control.Applicative
import Control.Applicative
  ( Applicative
  )

import P.Function.Flip

infixl 4 <*, *>, *^

-- |
-- Equivalent to 'Control.Applicative.pure'.
p ::
  ( Applicative f
  )
    => a -> f a
p =
  Control.Applicative.pure

-- |
-- Equivalent to '(Control.Applicative.<*>)'.
ap ::
  ( Applicative f
  )
    => f (a -> b) -> f a -> f b
ap =
  (Control.Applicative.<*>)

-- | Flip of 'ap'.
fA ::
  ( Applicative f
  )
    => f a -> f (a -> b) -> f b
fA =
  F ap

-- | Infix of 'ap'.
(*^) ::
  ( Applicative f
  )
    => f (a -> b) -> f a -> f b
(*^) =
  ap

-- | Perform two applicatives discarding the result of the first.
--
-- Prefix version of '(*>)'.
-- Equivalent to '(Control.Applicative.*>)'
aT ::
  ( Applicative f
  )
    => f a -> f b -> f b
aT =
  (Control.Applicative.*>)

-- | Perform two applicatives discarding the result of the first.
--
-- Not the flip of '(*>)'.
--
-- Equivalent to '(Control.Applicative.*>)'.
-- More general version of '(Prelude.>>)'.
(*>) ::
  ( Applicative f
  )
    => f a -> f b -> f b
(*>) =
  (Control.Applicative.*>)

-- | Flip of 'aT'.
--
-- Not equivalent to '(<*)'
faT ::
  ( Applicative f
  )
    => f b -> f a -> f b
faT =
  F aT

-- | Perform two applicatives discarding the result of the secdond.
--
-- Not the flip of '(*>)'.
--
-- Equivalent to '(Control.Applicative.*>)'.
-- More general version of '(Prelude.>>)'.
(<*) ::
  ( Applicative f
  )
    => f a -> f b -> f a
(<*) =
  (Control.Applicative.<*)

-- |
-- Equivalent to 'Control.Applicative.liftA2'.
-- More general version of 'Prelude.liftM2'.
l2 ::
  ( Applicative f
  )
    => (a -> b -> c) -> f a -> f b -> f c
l2 =
  Control.Applicative.liftA2

-- | Flip of 'l2'.
fl2 ::
  ( Applicative f
  )
    => f a -> (a -> b -> c) -> f b -> f c
fl2 =
  F l2

-- | Infix of 'l2'.
-- Equivalent to 'Control.Applicative.liftA2'.
(**) ::
  ( Applicative f
  )
    => (a -> b -> c) -> f a -> f b -> f c
(**) =
  l2

