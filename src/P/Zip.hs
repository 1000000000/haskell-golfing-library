{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
module P.Zip
  where

import qualified Prelude
import qualified Control.Applicative
import qualified Data.Functor.Classes

import P.Aliases
import P.Applicative
import P.Eq
import P.Function
import P.Function.Flip
import P.Functor.Compose
import P.Ord
import P.Monoid

-- | Zip is a mostly internal type.
-- It appears in the constraints of type signatures but should rarely be dealt with directly.
--
-- The constructors and deconstructors are available for use in the case where you might want to use the applicative properties more generally.
-- For example with @ApplicativeDo@ or just with some more advanced functions.
newtype Zip f a =
  Z
    { uZ ::
      f a
    }

-- | Lift a function up onto Zip.
-- Mostly for internal use.
liZ ::
  (
  )
    => (f a -> g b) -> (Zip f a -> Zip g b)
liZ =
  Z .^ fm uZ

instance Functor f => Functor (Zip f) where
  fmap =
     liZ < m

instance Eq1 f => Eq1 (Zip f) where
  liftEq =
    fkO uZ < q1

instance Ord1 f => Ord1 (Zip f) where
  liftCompare =
    fkO uZ < lcp

instance Eq (f a) => Eq (Zip f a) where
  (==) =
    on eq uZ

instance Ord (f a) => Ord (Zip f a) where
  compare =
    on cp uZ

instance Applicative (Zip []) where
  pure =
    Z < p

  liftA2 =
    Z .^^ fkO uZ < Prelude.zipWith

{-# Deprecated zipWith "Use zW instead" #-}
-- | Long version of 'zW'.
zipWith ::
  ( Applicative (Zip f)
  )
    => (a -> b -> c) -> f a -> f b -> f c
zipWith =
  zW

-- | When given two lists this combines them pairwise using a given binary function.
-- The longer list is trimmed to the length of the shorter list.
--
-- In general this an alternative 'l2', obeying all of the 'Applicative' laws.
-- However we privledge the normal implementation because it has a 'Prelude.Monad' instance while this does not.
--
-- More general version of 'Data.List.zipWith'.
zW ::
  ( Applicative (Zip f)
  )
    => (a -> b -> c) -> f a -> f b -> f c
zW =
  uZ .^^ fkO Z < l2

-- | Flip of 'zW'.
fzW ::
  ( Applicative (Zip f)
  )
    => f a -> (a -> b -> c) -> f b -> f c
fzW =
  F zW

-- | Takes two lists and combines them pairwise into a list of tuples.
--
-- More general version of 'Data.List.zip'.
zp ::
  ( Applicative (Zip f)
  )
    => f a -> f b -> f (a, b)
zp =
  zW (,)

-- | Flip of 'zp'.
fzp ::
  ( Applicative (Zip f)
  )
    => f a -> f b -> f (b, a)
fzp =
  F zp

-- | Takes a functor of pairs and gives a pair of functors.
-- The first one "contains" all the first elements.
-- And the second one "contains" all of the second elements.
--
-- More general version of 'Data.List.unzip'.
uz ::
  ( Functor f
  )
    => f (a, b) -> (f a, f b)
uz start =
  ( m Prelude.fst start
  , m Prelude.snd start
  )

-- | A thre argument version of 'zW'.
z3 ::
  ( Applicative (Zip f)
  )
    => (a -> b -> c -> d) -> f a -> f b -> f c -> f d
z3 func =
  zW (Prelude.uncurry func) .^ zp

-- | The flip of 'z3'.
fz3 ::
  ( Applicative (Zip f)
  )
    => f a -> (a -> b -> c -> d) -> f b -> f c -> f d
fz3 =
  F z3

-- | A version of 'zp' which takes three arguments and makes a 3-tuple.
--
-- Equivalent to 'Data.List.zip3'.
ź ::
  ( Applicative (Zip f)
  )
    => f a -> f b -> f c -> f (a, b, c)
ź =
  z3 (,,)

-- | Flip of 'ź'.
fZ3 ::
  (
  )
    => List b -> List a -> List c -> List (a, b, c)
fZ3 =
  F ź

-- | 'zW' but with default values.
-- When the lists are of different sizes instead of trimming the long one
-- the short one is extended with the default.
--
-- This function is restricted just to lists since it cannot be written in general.
zD ::
  (
  )
    => (a -> b -> c) -> a -> b -> List a -> List b -> List c
zD func defA defB =
  go
  where
    go [] [] =
      []
    go (a : as) [] =
      func a defB : go as []
    go [] (y : ys) =
      func defA y : go [] ys
    go (a : as) (y : ys) =
      func a y : go as ys

-- | 'zd' but it takes one default which it uses for both lists.
zd ::
  (
  )
    => (a -> a -> b) -> a -> List a -> List a -> List b
zd func def =
  zD func def def

-- | Flip of 'zd'.
fzd ::
  (
  )
    => a -> (a -> a -> b) -> List a -> List a -> List b
fzd =
  F zd


-- | A zip which takes a binary function and two lists.
-- It defaults to the present value when a value is missing.
zd' ::
  (
  )
    => (a -> a -> a) -> List a -> List a -> List a
zd' func =
  go
  where
    go (x : xs) (y : ys) =
      func x y : go xs ys
    go [] ys =
      ys
    go xs [] =
      xs

{-# Deprecated fzd' "Use fZd instead" #-}
-- | Long version of 'fZd'
fzd' ::
  (
  )
    => List a -> (a -> a -> a) -> List a -> List a
fzd' =
  F zd'

-- | Flip of 'zd''.
fZd ::
  (
  )
    => List a -> (a -> a -> a) -> List a -> List a
fZd =
  F zd'

-- | A zip which takes a binary function and two lists.
-- It combines elements with the semigroup operation ('mp') and defaults to the present value when a value is missing.
zdm ::
  ( Semigroup a
  )
    => List a -> List a -> List a
zdm =
  zd' mp

-- | Flip of 'zdm'.
fZm ::
  ( Semigroup a
  )
    => List a -> List a -> List a
fZm =
  F zdm

{-# Deprecated fzdm "Use fZm instead" #-}
-- | Long version of 'fZm'.
fzdm ::
  ( Semigroup a
  )
    => List a -> List a -> List a
fzdm =
  F zdm
