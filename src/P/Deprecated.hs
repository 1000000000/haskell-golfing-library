{-# Language PatternSynonyms #-}
{-|
Module :
  P.Deprecated
Description :
  Long form functions for the P library
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A library consisting of functions from P with longer more descriptive names.
All functions here are deprecated with text providing the shorter name to use.
-}
module P.Deprecated
  (
  -- | This module provides longer, more descriptive, and familiar names for certain functions in the P module.
  -- All the functions are deprecated since shorter versions exist which are preferable.
  --
  -- The intention of this library is to provide a way to check the shorter names of functions while writing code.
  -- Ideally you should be able to write your code using long names as placeholders and have the compiler provide you with the short names via deprecation warnings.
  --
  -- Unfortunately this library is missing many functions it should have, so it can't always be used this way.
  -- Hopefully in future updates this issue will become smaller and smaller.
  --
  -- No function here has a name under 3 bytes.
    sort
  , sortOn
  , sortBy
  , frsB
  , snoc
  , cons
  , reverse
  , drop
  , genericDrop
  , tail
  , init
  , splitAt
  , genericSplitAt
  , dropWhile
  , permutations
  , intersperse
  , intercalate
  , group
  , groupBy
  , pattern True
  , pattern False
  , flip
  , find
  , liftEq
  , liftCompare
  , show
  , fzdm
  , zipWith
  , fzd'
  , foldr1
  , foldl1
  )
  where

import Prelude ()

import P.Bool
import P.Eq
import P.Foldable
import P.Function.Flip
import P.List.Sort
import P.List
import P.Ord
import P.Show
import P.Zip
