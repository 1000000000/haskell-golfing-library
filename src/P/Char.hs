{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Char
  where

import qualified Prelude
import Prelude
  ( Char
  , Integral
  )
import qualified Data.Char

import P.Bool
import P.Functor.Compose
import P.Pattern

-- | Equivalent to 'Data.Char.isControl'.
iC ::
  (
  )
    => Char -> Bool
iC =
  Data.Char.isControl

-- | A pattern that matches on a control character.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it's a control character and returns a space otherwise.
--
-- @
-- f x@IC =
--   x
-- f y =
--   ' '
-- @
pattern IC :: Char
pattern IC <- (iC -> T)

-- | Equivalent to 'Data.Char.isSpace'.
iW ::
  (
  )
    => Char -> Bool
iW =
  Data.Char.isSpace

-- | A pattern that matches on whitespace.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it's whitespace and returns a space otherwise.
--
-- @
-- f x@IW =
--   x
-- f y =
--   ' '
-- @
pattern IW :: Char
pattern IW <- (iW -> T)

-- | Equivalent to 'Data.Char.isLower'.
iL ::
  (
  )
    => Char -> Bool
iL =
  Data.Char.isLower

-- | A pattern that matches on lowercase characters.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it's lowercase and returns a space otherwise.
--
-- @
-- f x@IL =
--   x
-- f y =
--   ' '
-- @
pattern IL :: Char
pattern IL <- (iL -> T)

-- | Equivalent to 'Data.Char.isUpper'.
iU ::
  (
  )
    => Char -> Bool
iU =
  Data.Char.isUpper

-- | A pattern that matches on uppercase characters.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it's uppercase and returns a space otherwise.
--
-- @
-- f x@IU =
--   x
-- f y =
--   ' '
-- @
pattern IU :: Char
pattern IU <- (iU -> T)

-- | Equivalent to 'Data.Char.isAlpha'.
α ::
  (
  )
    => Char -> Bool
α =
  Data.Char.isAlpha


-- | A pattern that matches on alphabetic characters.
--
-- Despite its appearance, the name of this function is not an uppercase latin @A@.
-- It is a uppercase greek alpha!
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it is alphabetic and returns a space otherwise.
--
-- @
-- f x@Α =
--   x
-- f y =
--   ' '
-- @
pattern Α :: Char
pattern Α <- (α -> T)

-- | Equivalent to 'Data.Char.isAlphaNum'.
iA ::
  (
  )
    => Char -> Bool
iA =
  Data.Char.isAlphaNum

-- | A pattern that matches on alpha-numeric characters.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it is alpha-numeric and returns a space otherwise.
--
-- @
-- f x@IA =
--   x
-- f y =
--   ' '
-- @
pattern IA :: Char
pattern IA <- (iA -> T)

-- | Equivalent to 'Data.Char.isPrint'.
iP ::
  (
  )
    => Char -> Bool
iP =
  Data.Char.isPrint

-- | A pattern that matches on printable characters.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it is printable and returns a space otherwise.
--
-- @
-- f x@IA =
--   x
-- f y =
--   ' '
-- @
pattern IP :: Char
pattern IP <- (iP -> T)

-- | Equivalent to 'Data.Char.isDigit'.
iD ::
  (
  )
    => Char -> Bool
iD =
  Data.Char.isDigit

-- |
-- === Function
-- As a function this takes a integer and gives the character representing it's last digit.
--
-- === Pattern
-- A pattern that matches on the characters `0-9`.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returning the empty list if it is not a digit and the value otherwise
--
-- @
-- f (ID x) =
--   [ x ]
-- f y =
--   []
-- @
pattern ID ::
  ( Integral i
  )
    => i -> Char
pattern ID x <- (iD -> T) := (Prelude.fromInteger < Prelude.toInteger < Data.Char.digitToInt -> x) where
  ID =
    Prelude.last < Prelude.show < Prelude.toInteger


-- |
-- === Function
-- As a function this converts a character to its uppercase variant.
--
-- Equivalent to 'Data.Char.toUpper'.
--
-- === Pattern
--
-- As a pattern this matches on any character with it's lower case variant.
--
-- ==== __Examples__
--
-- You can implment a 'Data.Char.toLower' function as such:
--
-- @
-- toLower :: Char -> Char
-- toLower (TU x) =
--   x
-- @
--
pattern TU :: Char -> Char
pattern TU x <- (Data.Char.toLower -> x) where
  TU =
    Data.Char.toUpper

-- | Maps 'TU' across a 'Functor'.
mtU ::
  ( Functor f
  )
    => f Char -> f Char
mtU =
  m TU

-- |
-- === Function
-- As a function this converts a character to its lowercase variant.
--
-- Equivalent to 'Data.Char.toLower'.
--
-- === Pattern
--
-- As a pattern this matches on any character with it's uppercase variant.
--
-- ==== __Examples__
--
-- You can implment a 'Data.Char.toUpper' function as such:
--
-- @
-- toUpper :: Char -> Char
-- toUpper (TL x) =
--   x
-- @
--
pattern TL :: Char -> Char
pattern TL x <- (Data.Char.toUpper -> x) where
  TL =
    Data.Char.toLower

-- | Maps 'TL' across a 'Functor'.
mtL ::
  ( Functor f
  )
    => f Char -> f Char
mtL =
  m TL

-- |
-- === Function
-- Takes a code point and gives the code-point at that character.
--
-- More general version of 'Data.Char.ord'.
--
-- === Pattern
--
-- Matches the character that gives the matched code-point.
pattern Or ::
  ( Integral i
  )
    => Char -> i
pattern Or c <- (_ch -> c) where
  Or =
    Prelude.fromInteger < Prelude.toInteger < Data.Char.ord

-- | Internal function.
-- Should not be exported equivalent to 'Ch'.
_ch ::
  ( Integral i
  )
    => i -> Char
_ch =
  Data.Char.chr < Prelude.fromInteger < Prelude.toInteger

{-# Complete Ch #-}
-- |
-- === Function
-- Takes a code point and gives the character at that code-point.
--
-- More general version of 'Data.Char.chr'.
--
-- === Pattern
--
-- Matches the code-point that gives the matched character.
pattern Ch ::
  ( Integral i
  )
    => i -> Char
pattern Ch c <- (Or -> c) where
  Ch =
    _ch

