module P.Monoid.FirstLast
  ( First (..)
  , Last (..)
  )
  where

import Prelude
  ( Traversable
  , Bounded
  , Num
  , Enum
  , Real
  , Integral
  )
import qualified Prelude

import P.Applicative
import P.Function.Flip
import P.Functor.Compose
import P.Functor.Identity
import P.Monad
import P.Monoid
import P.Show

-- | A wrapper which replaces the semigroup instance of 'Prelude.const'.
--
-- Redefined so we can give it a custom 'Show' instance.
newtype First a =
  Fst
    { uFt :: a
    }

instance Show a => Show (First a) where
  show =
    sh < UI

instance Functor First where
  fmap func =
    p < func < UI

instance Applicative First where
  pure =
    Fst
  (<*>) func =
    p < UI func < UI

instance Identity First where
  fromId =
    uFt

instance Monad First where
  (>>=) =
    F (< UI)

instance Semigroup (First a) where
  (<>) =
    Prelude.const

-- | A wrapper which replaces the semigroup instance of 'Prelude.flip Prelude.const'.
--
-- Redefined so we can give it a custom 'Show' instance.
newtype Last a =
  Lst
    { uLs :: a
    }

instance Functor Last where
  fmap func =
    p < func < UI

instance Applicative Last where
  pure =
    Lst
  (<*>) func =
    p < UI func < UI

instance Identity Last where
  fromId =
    uLs

instance Monad Last where
  (>>=) =
    F (< UI)

instance Semigroup (Last a) where
  (<>) =
    F p
