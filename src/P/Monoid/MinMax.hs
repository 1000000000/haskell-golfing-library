module P.Monoid.MinMax
  ( Min (..)
  , Max (..)
  )
  where

import qualified Prelude
import Prelude
  ( Bounded
  )

import P.Applicative
import P.Function.Flip
import P.Functor.Compose
import P.Functor.Identity
import P.Monad
import P.Monoid
import P.Ord
import P.Show

-- | A wrapper which replaces the semigroup instance of an ordered type with 'Prelude.min'.
--
-- Redefined so we can give it a custom 'Show' instance.
newtype Min a =
  Min
    { uMn :: a
    }

instance Show a => Show (Min a) where
  show =
    sh < UI

instance Functor Min where
  fmap func =
    p < func < UI

instance Applicative Min where
  pure =
    Min
  (<*>) func =
    p < UI func < UI

instance Identity Min where
  fromId =
    uMn

instance Monad Min where
  (>>=) =
    F (< UI)

instance Ord a => Semigroup (Min a) where
  (<>) =
    Prelude.min

instance (Bounded a, Ord a) => Monoid (Min a) where
  mempty =
    p Prelude.maxBound

-- | A wrapper which replaces the semigroup instance of an ordered type with 'Prelude.max'.
--
-- Redefined so we can give it a custom 'Show' instance.
newtype Max a =
  Max
    { uMx :: a
    }

instance Show a => Show (Max a) where
  show =
    sh < UI

instance Functor Max where
  fmap func =
    p < func < UI

instance Applicative Max where
  pure =
    Max
  (<*>) func =
    p < UI func < UI

instance Identity Max where
  fromId =
    uMx

instance Monad Max where
  (>>=) =
    F (< UI)

instance Ord a => Semigroup (Max a) where
  (<>) =
    Prelude.max

instance (Bounded a, Ord a) => Monoid (Max a) where
  mempty =
    p Prelude.maxBound
