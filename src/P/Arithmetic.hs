module P.Arithmetic where

import qualified Prelude
import Prelude
  ( Num
  , Integral
  , String
  , Bool
  )
import qualified Data.List

import P.Aliases
import P.Function.Flip
import P.Function.Identity
import P.Functor.Compose

infixl 6  +, -
infixl 7  *

-- | Addition.
--
-- Equivalent to '(Prelude.+)'.
(+) ::
  ( Num a
  )
    => a -> a -> a
(+) =
  (Prelude.+)

-- | Addition.
--
-- Equivalent to '(Prelude.+)'.
-- Prefix version of '(+)'
pl ::
  ( Num a
  )
    => a -> a -> a
pl =
  (+)

-- | Subtraction.
--
-- Equivalent to '(Prelude.-)'.
(-) ::
  ( Num a
  )
    => a -> a -> a
(-) =
  (Prelude.-)

-- | Subtraction.
--
-- Equivalent to 'Prelude.subtract'.
sb ::
  ( Num a
  )
      => a -> a -> a
sb =
  Prelude.subtract

-- | Multiplication.
--
-- Equivalent to '(Prelude.*)'.
(*) ::
  ( Num a
  )
    => a -> a -> a
(*) =
  (Prelude.*)

-- | Multiplication.
--
-- Equivalent to '(Prelude.*)'.
ml ::
  ( Num a
  )
    => a -> a -> a
ml =
  (*)

-- | Double or multiply by 2.
db ::
  ( Num a
  )
    => a -> a
db =
  ml 2

-- | Triple or multiply by 3.
tp ::
  ( Num a
  )
    => a -> a
tp =
  ml 3

-- | Multiply by 4.
m4 ::
  ( Num a
  )
    => a -> a
m4 =
  ml 4

-- | Multiply by 5.
m5 ::
  ( Num a
  )
    => a -> a
m5 =
  ml 5

-- | Multiply by 6.
m6 ::
  ( Num a
  )
    => a -> a
m6 =
  ml 6

-- | Multiply by 7.
m7 ::
  ( Num a
  )
    => a -> a
m7 =
  ml 7

-- | Multiply by 8.
m8 ::
  ( Num a
  )
    => a -> a
m8 =
  ml 8

-- | Multiply by 9.
m9 ::
  ( Num a
  )
    => a -> a
m9 =
  ml 9

-- | Multiply by 10.
--
-- To multiply by 0 use 'p 0'.
m0 ::
  ( Num a
  )
    => a -> a
m0 =
  ml 10

-- | Multiply by 11.
--
-- To multiply by 1 use 'id'.
m1 ::
  ( Num a
  )
    => a -> a
m1 =
  ml 11

-- | Integer division.
-- Result is truncated towards negative infinity.
(//) ::
  ( Integral i
  )
    => i -> i -> i
(//) =
  Prelude.div

-- | Integer division.
-- Result is truncated towards negative infinity.
dv ::
  ( Integral i
  )
    => i -> i -> i
dv =
  Prelude.div

-- | Flip of 'dv'.
fdv ::
  ( Integral i
  )
    => i -> i -> i
fdv =
  Prelude.div

-- | Integer modulus function.
--
-- Equivalent to 'Prelude.mod'.
(%) ::
  ( Integral i
  )
    => i -> i -> i
(%) =
  Prelude.mod

-- | Divmod function.
--
-- Equivalent to 'Prelude.divMod'.
vD ::
  ( Integral i
  )
    => i -> i -> (i, i)
vD =
  Prelude.divMod

-- | Flip of 'vD'.
fvD ::
  ( Integral i
  )
    => i -> i -> (i, i)
fvD =
  F vD

-- | Converts a positive integer to a string of its hexidecimal representation.
hx ::
  ( Integral i
  )
    => i -> String
hx x = do
  val <- bs 16 x & m (F Data.List.genericDrop "0123456789ABCDEF")
  val & Prelude.take 1 & Prelude.reverse

-- | Converts a positive integer to a particular base.
--
-- The outputted sequence is little endian, with the least significant place being the first in the list.
bs ::
  ( Integral i
  )
    => i -> i -> List i
bs base x
  | x Prelude.< 1
  =
    []
  | (div, mod) <- vD x base
  =
    mod : bs base div

-- | Flip of 'bs'.
fbs ::
  ( Integral i
  )
    => i -> i -> List i
fbs =
  F bs

-- | Converts a positive integer to a particular base.
--
-- The outputted sequence is big endian, with the most significant place being the first in the list.
bS ::
  ( Integral i
  )
    => i -> i -> List i
bS =
  Prelude.reverse .^ bs

-- | Flip of 'bS'.
fbS ::
  ( Integral i
  )
    => i -> i -> List i
fbS =
  F bS

-- | Converts an integer to binary in terms of boolean values.
--
-- The outputted sequence is little endian, with the least significant place being the first in the list.
bi ::
  ( Integral i
  )
    => i -> List Bool
bi =
  m (Prelude.> 0) < bs 2

-- | Squares a number.
sq ::
  ( Num a
  )
    => a -> a
sq x =
  x * x

-- | Raise to the power.
--
-- Equivalent to '(Prelude.^)'.
(^) ::
  ( Integral b
  , Num a
  )
    => a -> b -> a
(^) =
  (Prelude.^)

-- | Raise to the power.
--
-- Prefix version of '(^)'.
pw ::
  ( Integral b
  , Num a
  )
    => a -> b -> a
pw =
  (^)

-- | Flip of 'pw'.
fpw ::
  ( Integral b
  , Num a
  )
    => b -> a -> a
fpw =
  F pw

-- | Given @n@ returns 2 to the power of @n@.
pw2 ::
  ( Integral a
  , Num b
  )
    => a -> b
pw2 =
  pw 2


-- | Given @n@ returns 3 to the power of @n@.
pw3 ::
  ( Integral a
  , Num b
  )
    => a -> b
pw3 =
  pw 3


-- | Given @n@ returns 4 to the power of @n@.
pw4 ::
  ( Integral a
  , Num b
  )
    => a -> b
pw4 =
  pw 4


-- | Given @n@ returns 5 to the power of @n@.
pw5 ::
  ( Integral a
  , Num b
  )
    => a -> b
pw5 =
  pw 5


-- | Given @n@ returns 6 to the power of @n@.
pw6 ::
  ( Integral a
  , Num b
  )
    => a -> b
pw6 =
  pw 6


-- | Given @n@ returns 7 to the power of @n@.
pw7 ::
  ( Integral a
  , Num b
  )
    => a -> b
pw7 =
  pw 7


-- | Given @n@ returns 8 to the power of @n@.
pw8 ::
  ( Integral a
  , Num b
  )
    => a -> b
pw8 =
  pw 8


-- | Given @n@ returns 9 to the power of @n@.
pw9 ::
  ( Integral a
  , Num b
  )
    => a -> b
pw9 =
  pw 9


-- | Given @n@ returns 10 to the power of @n@.
pw0 ::
  ( Integral a
  , Num b
  )
    => a -> b
pw0 =
  pw 10


-- | Given @n@ returns 11 to the power of @n@.
pw1 ::
  ( Integral a
  , Num b
  )
    => a -> b
pw1 =
  pw 11



-- | Absolute value.
av ::
  ( Num a
  )
    => a -> a
av =
  Prelude.abs

-- | Absolute difference.
aD ::
  ( Num a
  )
    => a -> a -> a
aD =
  mm av sb

-- | Infix of 'aD'.
(-|) ::
  ( Num a
  )
    => a -> a -> a
(-|) =
  aD

-- | Absolute difference from 1.
aD1 ::
  ( Num a
  )
    => a -> a
aD1 =
  aD 1

-- | Absolute difference from 2.
aD2 ::
  ( Num a
  )
    => a -> a
aD2 =
  aD 2

-- | Absolute difference from 3.
aD3 ::
  ( Num a
  )
    => a -> a
aD3 =
  aD 3

-- | Absolute difference from 4.
aD4 ::
  ( Num a
  )
    => a -> a
aD4 =
  aD 4

-- | Absolute difference from 5.
aD5 ::
  ( Num a
  )
    => a -> a
aD5 =
  aD 5

-- | Absolute difference from 6.
aD6 ::
  ( Num a
  )
    => a -> a
aD6 =
  aD 6

-- | Absolute difference from 7.
aD7 ::
  ( Num a
  )
    => a -> a
aD7 =
  aD 7

-- | Absolute difference from 8.
aD8 ::
  ( Num a
  )
    => a -> a
aD8 =
  aD 8

-- | Absolute difference from 9.
aD9 ::
  ( Num a
  )
    => a -> a
aD9 =
  aD 9

-- | Absolute difference from -1.
aD0 ::
  ( Num a
  )
    => a -> a
aD0 =
  aD (-1)
