module P.Foldable
  ( Foldable
  , lF
  , flF
  , lF'
  , fLF
  , fo
  , mF
  , fmF
  , mf
  , fmf
  , rF
  , frF
  , foldr1
  , rH
  , frH
  , foldl1
  , lH
  , flH
  , l
  , ø
  , e
  , (?>)
  , fe
  , ne
  , (?<)
  , fE
  , ay
  , fay
  , (!!)
  -- * Scans
  , lS
  , flS
  , rS
  , frS
  , ls
  , fls
  , rs
  , frs
  , lz
  , flz
  , rz
  , frz
  -- * Finds
  , gN
  , fgN
  , g1
  , fg1
  , g2
  , fg2
  , g3
  , fg3
  , g4
  , fg4
  , g5
  , fg5
  , g6
  , fg6
  , g7
  , fg7
  , g8
  , fg8
  , g9
  , fg9
  , g0
  , fg0
  -- * Deprecated
  , find
  )
  where

import qualified Prelude
import Prelude
  ( Foldable
  , Monoid
  , Eq
  , Integral
  , Int
  )
import qualified Data.Foldable
import qualified Data.List
import qualified Data.Maybe

import P.Aliases
import P.Applicative
import P.Arithmetic
import P.Arithmetic.Pattern
import P.Bool
import P.Function.Flip
import P.Function.Identity
import P.Functor.Compose

-- | The left fold.
--
-- Equivalent to 'Data.List.foldl'.
lF ::
  ( Foldable t
  )
    => (b -> a -> b) -> b -> t a -> b
lF =
  Data.List.foldl

-- | Flip of 'lF'.
flF ::
  ( Foldable t
  )
    => b -> (b -> a -> b) -> t a -> b
flF =
  F lF

-- | Strict left fold.
-- Equivalent to 'Data.List.foldl''.
lF' ::
  ( Foldable t
  )
    => (b -> a -> b) -> b -> t a -> b
lF' =
  Data.List.foldl'

-- | Flip of 'lF''.
fLF ::
  ( Foldable t
  )
    => b -> (b -> a -> b) -> t a -> b
fLF =
  F lF'

-- | Given a structure whose elements are part of a monoid,
-- combine them together with the monoid operation '(<>)'.
--
-- Equivalent to 'Data.Foldable.fold'.
-- More general version of 'Data.List.concat'.
-- More general version of 'Prelude.mconcat'.
-- More general version of 'Data.List.and'.
fo ::
  ( Monoid m
  , Foldable t
  )
    => t m -> m
fo =
  Data.Foldable.fold

-- |
-- Eqivalent to 'Data.Foldable.foldMap'.
-- More general version of 'Data.List.concatMap'.
-- More general version of 'Data.Foldable.all'.
mF ::
  ( Monoid m
  , Foldable t
  )
    => (a -> m) -> t a -> m
mF =
  Data.Foldable.foldMap

-- | Flip of 'mF'.
fmF ::
  ( Monoid m
  , Foldable t
  )
    => t a -> (a -> m) -> m
fmF =
  F mF

-- |
-- Equivalent to 'Data.Foldable.foldMap''.
mf ::
  ( Monoid m
  , Foldable t
  )
    => (a -> m) -> t a -> m
mf =
  Data.Foldable.foldMap'

-- | Flip of 'mf'.
fmf ::
  ( Monoid m
  , Foldable t
  )
    => t a -> (a -> m) -> m
fmf =
  F mf

-- | The right fold.
--
-- Equivalent to 'Data.List.foldr'
rF ::
  ( Foldable t
  )
    => (a -> b -> b) -> b -> t a -> b
rF =
  Data.List.foldr

-- | Flip of 'rF'.
frF ::
  ( Foldable t
  )
    => b -> (a -> b -> b) -> t a -> b
frF =
  F rF

-- | The left scan.
-- More general version of 'Prelude.scanl'.
lS ::
  ( Foldable t
  )
    => (b -> a -> b) -> b -> t a -> List b
lS func start =
  Prelude.scanl func start < Data.Foldable.toList

-- | Flip of 'lS'.
flS ::
  ( Foldable t
  )
    => b -> (b -> a -> b) -> t a -> List b
flS =
  F lS

-- | The right scan.
-- More general version of 'Prelude.scanr'.
rS ::
  ( Foldable t
  )
    => (a -> b -> b) -> b -> t a -> List b
rS func start =
  Prelude.scanr func start < Data.Foldable.toList

-- | Flip of 'lS'.
frS ::
  ( Foldable t
  )
    => b -> (a -> b -> b) -> t a -> List b
frS =
  F rS

-- | A left scan which does not include the first value in the result.
ls ::
  ( Foldable t
  )
    => (b -> a -> b) -> b -> t a -> List b
ls func start foldable =
  Prelude.drop 1 $ lS func start foldable

-- | Flip of 'ls'.
fls ::
  ( Foldable t
  )
    => b -> (b -> a -> b) -> t a -> List b
fls =
  F ls

-- | A right scan which does not include the initial value in the result.
--
-- ==== __Examples__
--
-- Get all non-empty suffixes of a list
--
-- >>> rs (:) [] [1,2,3,4]
-- [[1,2,3,4],[2,3,4],[3,4],[4]]
--
rs ::
  ( Foldable t
  )
    => (a -> b -> b) -> b -> t a -> List b
rs func start foldable =
  Prelude.init $ rS func start foldable

-- | Flip of 'rs'.
frs ::
  ( Foldable t
  )
    => b -> (a -> b -> b) -> t a -> List b
frs =
  F rs

-- | A left scan using the first element of the input as the start.
--
-- Equivalent to 'Prelude.scanl1'
lz ::
  (
  )
    => (a -> a -> a) -> List a -> List a
lz =
  Prelude.scanl1

-- | Flip of 'lz'.
flz ::
  (
  )
    => List a -> (a -> a -> a) -> List a
flz =
  F lz

-- | A right scan using the first element of the input as the start.
--
-- Equivalent to 'Prelude.scanr1'
rz ::
  (
  )
    => (a -> a -> a) -> List a -> List a
rz =
  Prelude.scanr1

-- | Flip of 'rz'.
frz ::
  (
  )
    => List a -> (a -> a -> a) -> List a
frz =
  F rz

{-# Deprecated foldr1 "Use rH instead" #-}
foldr1 ::
  ( Foldable t
  )
    => (a -> a -> a) -> t a -> a
foldr1 =
  rH

-- | A right fold using the first element of the input as the start.
rH ::
  ( Foldable t
  )
    => (a -> a -> a) -> t a -> a
rH =
  Prelude.foldr1

-- | Flip of 'rH'.
frH ::
  ( Foldable t
  )
    => t a -> (a -> a -> a) -> a
frH =
  F rH

{-# Deprecated foldl1 "Use lH instead" #-}
foldl1 ::
  ( Foldable t
  )
    => (a -> a -> a) -> t a -> a
foldl1 =
  lH

-- | A left fold using the last element of the input as the start.
lH ::
  ( Foldable t
  )
    => (a -> a -> a) -> t a -> a
lH =
  Prelude.foldl1

-- | Flip of 'lH'.
flH ::
  ( Foldable t
  )
    => t a -> (a -> a -> a) -> a
flH =
  F lH

-- | Determines if an element is present in a foldable.
--
-- The negation of 'ne'.
-- Equivalent to 'Data.List.elem'.
e ::
  ( Foldable t
  , Eq a
  )
    => a -> t a -> Bool
e =
  Data.List.elem

-- | Infix version of 'e'.
(?>) ::
  ( Foldable t
  , Eq a
  )
    => a -> t a -> Bool
(?>) =
  e

-- | Flip of 'e'.
fe ::
  ( Foldable t
  , Eq a
  )
    => t a -> a -> Bool
fe =
  F e

-- |
-- The negation of 'e'.
-- Equivalent to 'Data.List.notElem'.
ne ::
  ( Foldable t
  , Eq a
  )
    => a -> t a -> Bool
ne =
  mm Prelude.not e

-- | Infix of 'ne'.
(?<) ::
  ( Foldable t
  , Eq a
  )
    => a -> t a-> Bool
(?<) =
  ne

-- | Flip of 'ne'.
fE ::
  ( Foldable t
  , Eq a
  )
    => t a -> a -> Bool
fE =
  F ne

-- |
-- Equivalent to 'Data.List.any'.
ay ::
  ( Foldable t
  )
    => Predicate a -> t a -> Bool
ay =
  Data.List.any

-- | Flip of 'ay'.
fay ::
  ( Foldable t
  )
    => t a -> Predicate a -> Bool
fay =
  F ay

-- | Gets the length of something.
-- More general than 'Data.List.length' or 'Data.List.genericLength'.
l ::
  ( Integral i
  , Foldable t
  )
    => t a -> i
l =
  rF (p (Prelude.+ 1)) 0

-- | Test whether a structure is empty.
--
-- Equivalent to 'Data.List.null'.
ø ::
  ( Foldable t
  )
    => t a -> Bool
ø =
  Data.List.null

-- | Safe index.
--
-- Indexes a list returning @[]@ if out of bounds.
(!!) ::
  ( Integral i
  , Foldable t
  )
    => t a -> i -> Maybe a
structure !! index =
  go index $ Data.Foldable.toList structure
  where
    go _ [] =
      []
    go N _ =
      []
    go 0 (x : _) =
      [x]
    go index' (_ : xs) =
      xs !! (index' - 1)

-- | Gets the nth element satisfying a predicate.
gN ::
  ( Foldable t
  , Integral i
  )
    => i -> Predicate a -> t a -> Maybe a
gN count pred =
  (!! S1 count) < rF go []
  where
    go x y
      | pred x
      =
        x : y
    go _ y =
      y

-- | Flip of 'gN'.
fgN ::
  ( Foldable t
  , Integral i
  )
    => Predicate a -> i -> t a -> Maybe a
fgN =
  F gN

{-# Deprecated find "Use g1 instead" #-}
-- | Long version of 'g1'
-- Gets the first element satisfying a predicate.
find ::
  ( Foldable t
  )
    => (a -> Bool) -> t a -> Maybe a
find =
  g1

-- | Gets the first element satisfying a predicate.
-- Returns the empty list if there is none and a singleton list if there is.
--
-- Equivalent to 'Data.List.find'.
g1 ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe a
g1 =
  Data.Maybe.maybeToList .^ Data.List.find

-- | Flip of 'g1'.
fg1 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe a
fg1 =
  F g1

-- | Gets the second element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if ther is.
g2 ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe a
g2 =
  gN (2 :: Int)

-- | Flip of 'g2'.
fg2 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe a
fg2 =
  F g2

-- | Gets the third element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if ther is.
g3 ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe a
g3 =
  gN (3 :: Int)

-- | Flip of 'g3'.
fg3 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe a
fg3 =
  F g3

-- | Gets the 4th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if ther is.
g4 ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe a
g4 =
  gN (4 :: Int)

-- | Flip of 'g4'.
fg4 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe a
fg4 =
  F g4

-- | Gets the 5th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if ther is.
g5 ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe a
g5 =
  gN (5 :: Int)

-- | Flip of 'g5'.
fg5 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe a
fg5 =
  F g5

-- | Gets the 6th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if ther is.
g6 ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe a
g6 =
  gN (6 :: Int)

-- | Flip of 'g6'.
fg6 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe a
fg6 =
  F g6

-- | Gets the 7th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if ther is.
g7 ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe a
g7 =
  gN (7 :: Int)

-- | Flip of 'g7'.
fg7 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe a
fg7 =
  F g7

-- | Gets the 8th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if ther is.
g8 ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe a
g8 =
  gN (8 :: Int)

-- | Flip of 'g8'.
fg8 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe a
fg8 =
  F g8

-- | Gets the 9th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if ther is.
g9 ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe a
g9 =
  gN (9 :: Int)

-- | Flip of 'g9'.
fg9 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe a
fg9 =
  F g9

-- | Gets the 10th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if ther is.
g0 ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe a
g0 =
  gN (10 :: Int)

-- | Flip of 'g0'.
fg0 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe a
fg0 =
  F g0
