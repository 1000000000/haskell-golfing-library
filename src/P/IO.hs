{-|
Module :
  P.IO
Description :
  Functions from P having to do with IO.
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Very short names for IO related functions.
Intended for use in code golf.
Please do not use this in real code.
-}
module P.IO
  ( IO
  , io
  -- ** Input
  , gC
  , gO
  , gl
  , gL
  , ga
  -- ** Output
  , ps
  , pS
  , pC
  , pO
  , pr
  , pW
  , fpW
  )
  where

import qualified Prelude
import Prelude
  ( String
  , Char
  , Show
  , Integral
  , IO
  )
import qualified System.IO

import P.Aliases
import P.Function.Flip
import P.Char
import P.Functor.Compose

-- | Output a string.
-- Equivalent to 'System.IO.putStr'.
ps ::
  (
  )
    => String -> IO ()
ps =
  System.IO.putStr

-- | Output a string with a trailing newline.
-- Equivalent to 'System.IO.putStrLn'.
pS ::
  (
  )
    => String -> IO ()
pS =
  System.IO.putStrLn

-- | Output a single character.
-- Equivalent to 'System.IO.putChar'.
pC ::
  (
  )
    => Char -> IO ()
pC =
  System.IO.putChar

-- | Take a codepoint and output it as a character.
pO ::
  ( Integral i
  )
    => i -> IO ()
pO =
  pC < Ch

-- | Format as string and output with a trailing newline.
-- Equivalent to 'System.IO.print'.
pr ::
  ( Show a
  )
    => a -> IO ()
pr =
  System.IO.print

-- | Print a string with a function other than 'Prelude.show'.
pW ::
  (
  )
    => (a -> String) -> a -> IO ()
pW =
  m pS

-- | Flip of 'pW'.
fpW ::
  (
  )
    => a -> (a -> String) -> IO ()
fpW =
  F pW

-- |
-- Equivalent to 'System.IO.interact'.
io ::
  (
  )
    => (String -> String) -> IO ()
io =
  System.IO.interact

-- | Gets a single character from stdin.
--
-- Equivalent to 'System.IO.getChar'.
gC ::
  (
  )
    => IO Char
gC =
  System.IO.getChar

-- | Gets a single character from stdin returning its code point.
gO ::
  ( Integral i
  )
    => IO i
gO =
  Or < gC

-- | Gets a single line from stdin.
--
-- Equivalent to 'System.IO.getLine'.
gl ::
  (
  )
    => IO String
gl =
  System.IO.getLine

-- | Returns all input from stdin as a list of lines.
-- The result is lazy reading only as needed.
gL ::
  (
  )
    => IO (List String)
gL =
  Prelude.lines < ga

{-
-- | Returns all input from stdin as a list of lines.
-- The result is strict reading only as needed.
-- For a lazy version see 'gL'.
gL' ::
  (
  )
    => IO (List String)
gL' =
  Prelude.lines . ga'
-}

-- | Returns all input from stdin as a single string.
-- The result is lazy reading only as needed.
--
-- Equivalent to 'System.IO.getContents'.
ga ::
  (
  )
    => IO String
ga =
  System.IO.getContents

{-
-- | Returns all input from stdin as a single string.
-- The result is strict reading the input fully before return.
-- For a lazy version see 'ga'.
--
-- Equivalent to 'System.IO.getContents''.
ga' ::
  (
  )
    => IO String
ga' =
  System.IO.getContents'
-}
