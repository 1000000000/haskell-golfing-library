{-# Language PatternSynonyms #-}
module P.Bool
  ( pattern T
  , pattern B
  , pattern True
  , pattern False
  , n
  , Bool
  , iF
  , (!?)
  , fiF
  , (?!)
  )
  where

import qualified Prelude
import Prelude
  ( Bool
  , Semigroup
  , Monoid
  )

import P.Function.Flip

instance Semigroup Bool where
  (<>) =
    (Prelude.&&)

instance Monoid Bool where
  mempty =
    T

{-# Deprecated True "Use T instead" #-}
-- | A long version of 'T'.
pattern True :: Bool
pattern True <- Prelude.True where
  True =
    Prelude.True

{-# Deprecated False "Use B instead" #-}
-- | A long version of 'B'.
pattern False :: Bool
pattern False <- Prelude.False where
  False =
    Prelude.False

{-# Complete T, B #-}
{-# Complete T, False #-}
{-# Complete True, B #-}
-- | A shorthand for @True@.
pattern T :: Bool
pattern T <- Prelude.True where
  T =
    Prelude.True

-- | A shorthand for @False@.
pattern B :: Bool
pattern B <- Prelude.False where
  B =
    Prelude.False

-- | Logical not.
-- Takes a boolean and returns the opposite.
--
-- Equivalent to 'Prelude.not'.
n ::
  (
  )
    => Bool -> Bool
n =
  Prelude.not

-- | Conditional branch.
-- Returns the first argument if true, and the second if false.
iF ::
  (
  )
    => a -> a -> Bool -> a
iF x _ T =
  x
iF _ y B =
  y

-- | Infix of 'iF'.
(?!) ::
  (
  )
    => a -> a -> Bool -> a
(?!) =
  iF

-- | Flip of 'iF'.
-- Returns the first argument if true, and the second if false.
fiF ::
  (
  )
    => a -> a -> Bool -> a
fiF =
  F iF

-- | Infix of 'fiF'.
-- Flip of '(?!)'.
(!?) ::
  (
  )
    => a -> a -> Bool -> a
(!?) =
  F (?!)
