module P.Functor
  ( Functor
  , mi
  , mI
  , vd
  )
  where

import qualified Data.Functor

import P.Aliases
import P.Function.Flip

-- | Map a function over the non-negative integers.
mi ::
  ( Integral i
  )
    => (i -> a) -> List a
mi =
  F fmap [0 ..]

-- | Map a function over the positive integers.
mI ::
  ( Integral i
  )
    => (i -> a) -> List a
mI =
  F fmap [1 ..]

-- | Equivalent to 'Data.Functor.void'.
vd ::
  ( Functor f
  )
    => f a -> f ()
vd =
  Data.Functor.void
