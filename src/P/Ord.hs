{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-|
Module :
  P.Ord
Description :
  Functions having to do with the @Ord@ class.
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Functions having to do with the @Ord@ class, for use in code golf.

Please do not use this in real code.
-}
module P.Ord
  ( Ord
  , gt
  , ge
  , lt
  , le
  , (>)
  , (>=)
  , (<.)
  , (<=)
  , Ordering (..)
  , cp
  , fcp
  , cb
  , pattern RO
  , pattern RC
  , Ord1
  , lcp
  , liftCompare
  )
  where

import qualified Prelude
import Prelude
  ( Ord
  , Ordering (..)
  , Bool
  )
import qualified Data.Functor.Classes
import Data.Functor.Classes
  ( Ord1
  )

import P.Function
import P.Function.Flip
import P.Functor.Compose

-- | Greater than.
gt ::
  ( Ord a
  )
    => a -> a -> Bool
gt =
  (>)

-- | Greater than or equal to.
ge ::
  ( Ord a
  )
    => a -> a -> Bool
ge =
  (<=)

-- | Less than.
lt ::
  ( Ord a
  )
    => a -> a -> Bool
lt =
  (<.)

-- | Less than or Equal to.
le ::
  ( Ord a
  )
    => a -> a -> Bool
le =
  (<=)

-- | Greater than.
--
-- Equivalent to '(Prelude.>)'.
(>) ::
  ( Ord a
  )
    => a -> a -> Bool
(>) =
  (Prelude.>)

-- | Greater than or equal to.
--
-- Equivalent to '(Prelude.>=)'.
(>=) ::
  ( Ord a
  )
    => a -> a -> Bool
(>=) =
  (Prelude.>=)

-- | Less than.
--
-- Prefer to use the shorter '(>)'.
--
-- Equivalent to '(Prelude.<)'.
(<.) ::
  ( Ord a
  )
    => a -> a -> Bool
(<.) =
  (Prelude.<)

-- | Less than or equal to.
--
-- Equivalent to '(Prelude.<=)'.
(<=) ::
  ( Ord a
  )
    => a -> a -> Bool
(<=) =
  (Prelude.<=)

-- | Compare two values.
--
-- Equivalent to 'Prelude.compare'.
cp ::
  ( Ord a
  )
    => a -> a -> Ordering
cp =
  Prelude.compare

-- | Flip of 'cp'.
fcp ::
  ( Ord a
  )
    => a -> a -> Ordering
fcp =
  F cp

-- | Compare with a user defined conversion function.
--
-- ==== __Examples__
--
-- To compare two lists by length use @cb l@.
--
cb ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> Ordering
cb =
  on cp

-- | Internal function to swap an 'Ordering' to the opposite value.
--
-- Instead exports the pattern 'RO'.
_rO ::
  (
  )
    => Ordering -> Ordering
_rO LT =
  GT
_rO EQ =
  EQ
_rO GT =
  LT

{-# Complete RO #-}
-- | Swaps an 'Ordering' to the opposite value.
pattern RO ::
  (
  )
    => Ordering -> Ordering
pattern RO x <- (_rO -> x) where
  RO =
    _rO

-- | To be used to take a ordering function and create the opposite ordering function.
--
-- Has a much more general type simply because it can.
--
-- ==== __Examples__
--
-- Reverse the normal comparison:
--
-- >>> cp 1 2
-- LT
-- >>> rC cp 1 2
-- GT
--
-- Reverse a more complicated comparison:
--
-- >>> on cp sh 9 19
-- GT
-- >> rC (on cp sh) 9 19
-- LT
--
-- It's more general type can be used in some circumstances:
--
-- >>> rC [(2, GT), (1, LT), (3, EQ), (3, GT)]
-- [(2,LT),(1,GT),(3,EQ),(3,LT)]
--
pattern RC ::
  ( Functor f
  , Functor g
  )
    => f (g Ordering) -> f (g Ordering)
pattern RC x <- (mm RO -> x) where
  RC =
    mm RO

{-# Deprecated liftCompare "Use lcp instead." #-}
-- | Long version of 'lcp'.
liftCompare ::
  ( Ord1 f
  )
    => (a -> b -> Ordering) -> (f a -> f b -> Ordering)
liftCompare =
  Data.Functor.Classes.liftCompare

-- | Lifts a user defined comparison function up onto the structure.
--
-- Equivalent to 'Data.Functor.Classes.liftCompare'.
lcp ::
  ( Ord1 f
  )
    => (a -> b -> Ordering) -> (f a -> f b -> Ordering)
lcp =
  Data.Functor.Classes.liftCompare
