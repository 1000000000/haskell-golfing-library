{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Tuple
  where

import qualified Prelude
import Prelude
  ( Num
  , (+)
  , (*)
  )

import P.Bool
import P.Eq
import P.Pattern

instance (Num a, Num b) => Num (a, b) where
  (x1, x2) + (y1, y2) =
    ( x1 + y1
    , x2 + y2
    )
  (x1, x2) * (y1, y2) =
    ( x1 * y1
    , x2 * y2
    )
  fromInteger i =
    ( Prelude.fromInteger i
    , Prelude.fromInteger i
    )
  negate (x1, x2) =
    ( Prelude.negate x1
    , Prelude.negate x2
    )
  abs (x1, x2) =
    (Prelude.abs x1, Prelude.abs x2)
  signum (x1, x2) =
    (Prelude.signum x1, Prelude.signum x2)

-- | Get the first element of a 2 element tuple.
-- Equivalent to 'Prelude.fst'.
st ::
  (
  )
    => (a, b) -> a
st =
  Prelude.fst

-- | Get the second element of a 2 element tuple.
-- Equivalent to 'Prelude.snd'.
nd ::
  (
  )
    => (a, b) -> b
nd =
  Prelude.snd

{-# Complete Bp #-}
-- | Makes a pair.
pattern Bp ::
  (
  )
    => a -> b -> (a, b)
pattern Bp x1 x2 =
  (x1, x2)

{-# Complete Pb #-}
-- | Flip of 'Bp'.
pattern Pb ::
  (
  )
    => b -> a -> (a, b)
pattern Pb x1 x2 =
  (x2, x1)

-- | Makes a pair where both elements are the same.
--
-- A slightly less general version of this 'Jbp', also allows pattern matching.
jbp ::
  (
  )
    => a -> (a, a)
jbp x =
  (x, x)

-- | A pattern version of 'jbp'.
-- Since it must go both ways it requires @Eq@ while 'jbp' does not.
pattern Jbp ::
  ( Eq a
  )
    => a -> (a, a)
pattern Jbp a <- (a,_) := (Prelude.uncurry eq -> T) where
  Jbp =
    jbp

-- | Internal version of 'Sw'.
_sw ::
  (
  )
    => (a, b) -> (b, a)
_sw (a, b) =
  (b, a)

{-# Complete Sw #-}
-- | Swaps the elements of a tuple
pattern Sw ::
  (
  )
    => (a, b) -> (b, a)
pattern Sw x <- (_sw -> x) where
  Sw =
    _sw
