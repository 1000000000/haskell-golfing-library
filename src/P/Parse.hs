module P.Parse
  where

import qualified Prelude
import Prelude
  ( Bool
  , String
  , Char
  , Read
  , Maybe (..)
  )
import qualified Control.Applicative
import Control.Applicative
  ( Alternative
  )
import qualified Data.Char
import qualified Text.Read

import P.Aliases
import P.Alternative
import P.Applicative
import P.Eq
import P.Function.Flip
import P.Function.Identity
import P.Functor.Compose
import P.Monad
import P.Monoid

-- | A parser type.
newtype Parser k a =
  P
    { uP ::
      (k -> List (k, a))
    }

instance Functor (Parser k) where
  fmap func =
    P < m3 func < uP

instance Applicative (Parser k) where
  pure =
    P < p .^ F (,)

  liftA2 func (P parser1) (P parser2) =
    P
      ( \ start ->
        do
          (rem1, res1) <- parser1 start
          (rem2, res2) <- parser2 rem1
          p (rem2, func res1 res2)
      )

instance Alternative (Parser k) where
  empty =
    P $ p []

  P parser1 <|> P parser2 =
    P $ l2 (++) parser1 parser2

instance Monad (Parser k) where
  P parser1 >>= func =
    P
      ( \ start ->
        do
          (rem1, res1) <- parser1 start
          uP (func res1) rem1
      )

instance Semigroup m => Semigroup (Parser k m) where
  (<>) =
    l2 (<>)

instance Monoid m => Monoid (Parser k m) where
  mempty =
    p i

-- | Takes a parser and an input and returns @True@ if there is any parse of the input even an incomplete one.
pP ::
  (
  )
    => Parser k a -> k -> Bool
pP =
  Prelude.not .^ Prelude.null .^ uP

-- | Infix of 'pP'.
(-@) ::
  (
  )
    => Parser k a -> k -> Bool
(-@) =
  pP

-- | Takes a parser and an input and returns @True@ if there is at least 1 complete parse.
--
-- A complete parse being a parse where the remainder is the identiy of the canonical monoid.
-- For most cases lists are being parsed so this is the empty list.
cP ::
  ( Monoid k
  , Eq k
  )
    => Parser k a -> k -> Bool
cP =
  Prelude.not .^ Prelude.null .^ gc

-- | Infix of 'cP'.
(-#) ::
  ( Monoid k
  , Eq k
  )
    => Parser k a -> k -> Bool
(-#) =
  cP

-- | Takes a parser and an input and returns @True@ if there is exactly 1 complete parse.
x1 ::
  ( Monoid k
  , Eq k
  )
    => Parser k a -> k -> Bool
x1 =
  eq 1 .^ Prelude.length .^ gc

-- | Infix of 'x1'.
(-$) ::
  ( Monoid k
  , Eq k
  )
    => Parser k a -> k -> Bool
(-$) =
  x1

-- | Gets the results of all parses.
gP ::
  (
  )
    => Parser k a -> k -> List a
gP =
  m Prelude.snd .^ uP

-- | Infix of 'gP'.
(-%) ::
  (
  )
    => Parser k a -> k -> List a
(-%) =
  gP

-- | Gets all complete parses.
gc ::
  ( Monoid k
  , Eq k
  )
    => Parser k a -> k -> List a
gc =
  Prelude.snd .^^ fl (eq i < Prelude.fst) .^ uP

-- | Infix of 'gc'.
(-^) ::
  ( Monoid k
  , Eq k
  )
    => Parser k a -> k -> List a
(-^) =
  gc

-- | A look-ahead.
-- It takes a parser and produces a new parser which creates the same results without consuming the input.
--
-- Useful if you want to run two parsers on the same thing.
lA ::
  (
  )
    => Parser k a -> Parser k a
lA parser =
  P
    ( \ input ->
      do
        (_, res) <- uP parser input
        p (input, res)
    )

-- | A negative look-ahead.
-- I takes a parser and produces a new parser which consumes no input and is successful if an only if the given parser was not.
--
nA ::
  (
  )
    => Parser k a -> Parser k ()
nA parser =
  P
    ( \ input ->
      case
        uP parser input
      of
        [] ->
          p (input, ())
        _ ->
          em
    )

-- | Accepts and returns single character.
hd ::
  (
  )
    => Parser (List a) a
hd =
  P run
  where
    run [] =
      em
    run (head : tail) =
      p (tail, head)

-- | Accepts and returns at least one character.
h_ ::
  (
  )
    => Parser (List a) (List a)
h_ =
  so hd

-- | Accpets and returns any number of characters including none at all.
h' ::
  (
  )
    => Parser (List a) (List a)
h' =
  my hd

-- | Parses a single character accepting if it satisfies some predicate.
kB ::
  (
  )
    => (a -> Bool) -> Parser (List a) a
kB =
  wh hd

-- | Parses a single character accepting it if it is equal to a particular character.
--
-- When parsing a particular character it is often shorter altogether to use 'ʃ'.
-- For example @ʃ\"*\"@ is shorter than @χ \'*\'@ (the space is necessary to split the tokens).
χ ::
  ( Eq a
  )
    => a -> Parser (List a) a
χ char =
  kB (Prelude.== char)

-- | Parses at least one of the given character.
x_ ::
  ( Eq a
  )
    => a -> Parser (List a) (List a)
x_ =
  so < χ

-- | Parses any number of the given character.
x' ::
  ( Eq a
  )
    => a -> Parser (List a) (List a)
x' =
  my < χ

-- | Parses a single digit @0-9@.
dg ::
  (
  )
    => Parser String Char
dg =
  kB (`Prelude.elem` ['0' .. '9'])

-- | Parses a single alphabetic character.
al ::
  (
  )
    => Parser String Char
al =
  kB Data.Char.isAlpha

-- | Parses a complete complete word.
-- That is it parses all alphabetic characters off the front of a string.
-- If there are none it fails.
wd ::
  (
  )
    => Parser String String
wd =
  so al <* nA al

-- | Parses a string accepting it if it is equal to the given string.
ʃ ::
  ( Eq a
  )
    => List a -> Parser (List a) (List a)
ʃ =
  Prelude.traverse χ

-- | Parses at least one of the given string.
s_ ::
  ( Eq a
  )
    => List a -> Parser (List a) (List a)
s_ =
  sO < ʃ

-- | Parses ant number of the given string.
s' ::
  ( Eq a
  )
    => List a -> Parser (List a) (List a)
s' =
  mY < ʃ

-- | Parse a readable.
ν ::
  ( Read a
  )
    => Parser String a
ν =
  do
  prefix <- h_
  case
      Text.Read.readMaybe prefix
    of
      Nothing ->
        em
      Just value ->
        p value
