{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Matrix where

import Prelude
  (
  )
import qualified Data.List
import qualified Data.Foldable
import Data.Foldable
  ( Foldable
  )

import P.Aliases
import P.Functor.Compose

-- | A type for a 2 dimensional rectangular container.
--
-- Functions taking matrices generally assume that they will be rectangular.
-- They may have strange or undocumented behavior when the input is not.
type Matrix a =
  List (List a)

{-# Complete Tp #-}
-- | Transposes a matrix.
--
-- Patterned version of 'Data.List.transpose'.
pattern Tp ::
  (
  )
    => Matrix a -> Matrix a
pattern Tp x <- (Data.List.transpose -> x) where
  Tp =
    Data.List.transpose

-- | Maps over the columns of a matrix.
mC ::
  ( Foldable t
  )
    => (List a -> List a) -> t (List a) -> Matrix a
mC func =
  Tp < mm func Tp < Data.Foldable.toList
