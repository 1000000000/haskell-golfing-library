{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.List
  where

import qualified Prelude
import Prelude
  ( Integral
  , Int
  , Num
  , Bool
  , otherwise
  )
import qualified Data.List

import P.Aliases
import P.Arithmetic
import P.Eq
import P.Function.Curry
import P.Function.Flip
import P.Function.Identity
import P.Functor.Compose
import P.Foldable
import P.Monoid

infixr 5 :%
infixr 5 :>

-- | Internal function used to define '(:>)'.
_rC ::
  (
  )
    => List a -> List (a, List a)
_rC [] =
  []
_rC [x] =
  [(x, [])]
_rC (x : xs) =
  mm (x :) $ _rC xs

{-# Deprecated snoc "Use (:>) instead"#-}
-- | Long version of '(:>)'.
--
-- Adds a single element to the end of a list.
snoc ::
  (
  )
    => a -> List a -> List a
snoc =
  (:>)

{-# Complete [], (:>) #-}
-- | Add a single element to the end of a list.
--
-- Also works as a pattern to match.
--
-- ==== __Examples__
--
-- ===== As a pattern
--
-- Determine if the first and last elements are equal:
--
-- @
-- f (x:_ := y:>_) =
--   x == y
-- f [] =
--   F
-- @
--
-- >>> f [1,2,3]
-- False
-- >>> f [1]
-- True
-- >>> f [3,2,3]
-- True
-- >>> f []
-- False
--
pattern (:>) :: a -> List a -> List a
pattern x :> xs <- (_rC -> [(x, xs)]) where
  x :> xs =
    xs <> [x]

{-# Deprecated cons "Use K instead" #-}
-- | Long version of 'K'.
cons ::
  (
  )
    => a -> List a -> List a
cons =
  (:)

{-# Complete K, [] #-}
-- | List cons.
pattern K ::
  (
  )
    => a -> List a -> List a
pattern K x xs =
  x : xs

{-# Complete Fk, [] #-}
-- | Flip of 'K'.
pattern Fk ::
  (
  )
    => List a -> a -> List a
pattern Fk xs x =
  x : xs

{-# Deprecated reverse "Use rv or Rv instead" #-}
-- | Long version of 'rv'.
-- For a pattern version see also 'Rv'.
-- Reverses a list.
reverse ::
  ( Foldable t
  )
    => t a -> List a
reverse =
  rv

-- | Reverses a foldable structure.
rv ::
  ( Foldable t
  )
    => t a -> List a
rv =
  lF' Fk []

{-# Complete Rv #-}
-- | Reverses a list.
--
-- Equivalent to 'Data.List.reverse'.
pattern Rv ::
  (
  )
    => List a -> List a
pattern Rv x <- (rv -> x) where
  Rv =
    rv

{-# Deprecated drop, genericDrop "Use dr instead" #-}
-- | Long version of 'dr'.
-- Take @i@ and remove the first @i@ elements from a list.
drop ::
  ( Integral i
  )
    => i -> List a -> List a
drop =
  Data.List.genericDrop

-- | Long version of 'dr'.
-- Take @i@ and remove the first @i@ elements from a list.
genericDrop ::
  ( Integral i
  )
    => i -> List a -> List a
genericDrop =
  Data.List.genericDrop

-- | Take @i@ and remove the first @i@ elements from a list.
-- Equivalent to 'Data.List.genericDrop'.
-- More general version of 'Data.List.drop'.
dr ::
  ( Integral i
  )
    => i -> List a -> List a
dr =
  Data.List.genericDrop

-- | Infix version of 'dr'.
(#>) ::
  ( Integral i
  )
    => i -> List a -> List a
(#>) =
  dr

-- | Flip of 'dr'.
fd ::
  ( Integral i
  )
    => List a -> i -> List a
fd =
  F dr

-- | Drops the first 2 elements from a list.
-- Shorthand for @'dr' 2@.
dr2 ::
  (
  )
    => List a -> List a
dr2 =
  dr (2 :: Int)

-- | Drops the first 3 elements from a list.
-- Shorthand for @'dr' 3@.
dr3 ::
  (
  )
    => List a -> List a
dr3 =
  dr (3 :: Int)

-- | Drops the first 4 elements from a list.
-- Shorthand for @'dr' 4@.
dr4 ::
  (
  )
    => List a -> List a
dr4 =
  dr (4 :: Int)

-- | Drops the first 5 elements from a list.
-- Shorthand for @'dr' 5@.
dr5 ::
  (
  )
    => List a -> List a
dr5 =
  dr (5 :: Int)

-- | Drops the first 6 elements from a list.
-- Shorthand for @'dr' 6@.
dr6 ::
  (
  )
    => List a -> List a
dr6 =
  dr (6 :: Int)

-- | Drops the first 7 elements from a list.
-- Shorthand for @'dr' 7@.
dr7 ::
  (
  )
    => List a -> List a
dr7 =
  dr (7 :: Int)

-- | Drops the first 8 elements from a list.
-- Shorthand for @'dr' 8@.
dr8 ::
  (
  )
    => List a -> List a
dr8 =
  dr (8 :: Int)

-- | Drops the first 9 elements from a list.
-- Shorthand for @'dr' 9@.
dr9 ::
  (
  )
    => List a -> List a
dr9 =
  dr (9 :: Int)

-- | Drops the first 10 elements from a list.
-- To drop the first element of a list see 'id'.
dr0 ::
  (
  )
    => List a -> List a
dr0 =
  dr (10 :: Int)

-- | Drops the first 11 elements from a list.
-- To drop the first element of a list see 'tl'.
dr1 ::
  (
  )
    => List a -> List a
dr1 =
  dr (11 :: Int)


{-# Deprecated tail "Use tl instead" #-}
-- | Long version of 'tl'.
-- Gives the tail of a list.
tail ::
  (
  )
    => List a -> List a
tail =
  tl

-- | Drops the first element of a list.
-- Like 'Data.List.tail' but it doesn't error on the empty list.
tl ::
  (
  )
    => List a -> List a
tl =
  dr (1 :: Int)

{-# Deprecated init "Use nt insted" #-}
-- | Long version of 'nt'.
-- Gives all but the last element of a list.
init ::
  (
  )
    => List a -> List a
init =
  nt

-- | Removes the last element of a list.
--
-- Like 'Data.List.init' but it doesn't error on the empty list.
nt ::
  (
  )
    => List a -> List a
nt [] =
  []
nt x =
  Data.List.init x

{-# Deprecated splitAt, genericSplitAt "Use sA instead" #-}
-- | Long version of 'sA'.
-- Splits a list at an index.
splitAt ::
  ( Integral i
  )
    => i -> List a -> (List a, List a)
splitAt =
  sA

-- | Long version of 'sA'.
-- Splits a list at an index.
genericSplitAt ::
  ( Integral i
  )
    => i -> List a -> (List a, List a)
genericSplitAt =
  sA

-- | Splits a list at an index.
-- Equivalent to 'Data.List.genericSplitAt'.
-- More general version of 'Data.List.splitAt'
sA ::
  ( Integral i
  )
    => i -> List a -> (List a, List a)
sA =
  Data.List.genericSplitAt

-- | Infix version of 'sA'.
(#=) ::
  ( Integral i
  )
    => i -> List a -> (List a, List a)
(#=) =
  sA

-- | Flip of 'sA'.
fsA ::
  ( Integral i
  )
    => List a -> i -> (List a, List a)
fsA =
  F sA

{-# Deprecated dropWhile "Use dW instead" #-}
-- | Long version of 'dW'.
-- Removes the largest prefix satisfying a predicate and returns the remaining.
dropWhile ::
  (
  )
    => Predicate a -> List a -> List a
dropWhile =
  dW

-- | Removes the largest prefix satisfying a predicate and returns the remaining.
--
-- Equivalent to 'Data.List.dropWhile'.
dW ::
  (
  )
    => Predicate a -> List a -> List a
dW =
  Data.List.dropWhile

-- | Flip of 'dW'.
fdW ::
  (
  )
    => List a -> Predicate a -> List a
fdW =
  F dW

-- | Takes a predicate and a list and splits at all elements which satisfy the predicate making a list with divisions at the matches.
--
-- ==== __Examples__
--
-- With 'iW' this splits a string into a list of words.
--
-- >>> sY iW "Hello I'm a string!"
-- ["Hello", "I'm", "a", "string!"]
sY ::
  (
  )
    => Predicate a -> List a -> List (List a)
sY _ [] =
  []
sY pred list =
  head : sY pred rest
  where
    (head, rest) =
      (dr1 .^^ Prelude.break) pred list

-- | Infix of 'sY'.
(|/) ::
  ( Eq a
  )
    => Predicate a -> List a -> List (List a)
(|/) =
  sY

-- | Like 'sY' except it takes a value instead of a predicate and splits at elements that are equal to the given value.
sL ::
  ( Eq a
  )
    => a -> List a -> List (List a)
sL =
  sY < eq

-- | Infix of 'sL'.
(|\) ::
  ( Eq a
  )
    => a -> List a -> List (List a)
(|\) =
  sL

{-# Deprecated permutations "Use pm instead" #-}
-- | Long version of 'pm'.
-- Gives all permutations of a given list.
permutations ::
  (
  )
    => List a -> List (List a)
permutations =
  pm

-- | Gives all permutations of a given list.
--
-- Equivalent to 'Data.List.permutations'.
pm ::
  (
  )
    => List a -> List (List a)
pm =
  Data.List.permutations

-- | Maps a function over all permutations of a given list.
ƪ ::
  (
  )
    => (List a -> b) -> List a -> List b
ƪ =
  fM pm

-- | Gives all partitions of a given list into non-empty sublists.
pt ::
  (
  )
    => List a -> List (List (List a))
pt [] =
  [[]]
pt (x1 : xs) =
  do
    parts <- pt xs
    case parts of
      [] ->
        [[[x1]]]
      (part1 : otherParts) ->
        [ [x1] : parts
        , (x1 : part1) : otherParts
        ]

-- | Applies a function to consecutive elements of a list.
--
-- ==== __Examples__
--
-- @pa (-)@ calculates the descending differences or the difference between each element and the previous.
--
-- >>> pa (-) [1,2,4,2,1]
-- [-1,-2,2,1]
--
-- @pa (,)@ gives all pairs of consecutive elements.
-- This is the same as 'pA'.
--
-- >>> pa (,) [1,2,4,2,1]
-- [(1,2),(2,4),(4,2),(2,1)]
--
pa ::
  (
  )
    => (a -> a -> b) -> List a -> List b
pa func (x1 : x2 : xs) =
  func x1 x2 : pa func (x2 : xs)
pa _ _ =
  []

-- | Infix of 'pa'.
(%%) ::
  (
  )
    => (a -> a -> b) -> List a -> List b
(%%) =
  pa

-- | Flip of 'pa'.
fpa ::
  (
  )
    => List a -> (a -> a -> b) -> List b
fpa =
  F pa

-- | 'pa' but using a flipped version of the given function.
--
-- ==== __Examples__
--
-- When calculating the consecutive difference between elements 'pa' will give the descending difference:
--
-- >>> pa (-) [1,3..9]
-- [-2,-2,-2,-2]
--
-- If we want the ascending difference then we need to flip the @(-)@.
--
-- >> paf (-) [1,3..9]
-- [2,2,2,2]
--
paf ::
  (
  )
    => (a -> a -> b) -> List a -> List b
paf =
  pa < F

-- | Flip of 'paf'.
paF ::
  (
  )
    => List a -> (a -> a -> b) -> List b
paF =
  F $ paf

-- | Gives all pairs of consecutive elements in a list.
--
-- ==== __Examples__
--
-- >>> pA [1,2,4,2,1]
-- [(1,2),(2,4),(4,2),(2,1)]
--
pA ::
  (
  )
    => List a -> List (a, a)
pA =
  pa (,)

-- | Gives the deltas of a list.
δ ::
  ( Num i
  )
    => List i -> List i
δ =
  paf (Prelude.-)

-- | Gives the absolute difference between consecutive elements of a list.
δ' ::
  ( Num i
  )
    => List i -> List i
δ' =
  paf (mm Prelude.abs (Prelude.-))

-- | Takes a function and applies to all pairs in which the first element occurs before the second.
--
-- For a commutative function this prevents duplication.
--
-- Outputs in diagonalized order so that it works with infinite lists.
--
-- For a version that also includes applying the function to every element and itself, see 'xX'.
--
-- For a version that just applies to all pairs see 'l2'.
--
-- ==== __Examples__
--
-- Use it to get all numbers with exactly two 1s in their binary representation.
--
-- >>> xQ pl $ m (2^) [0..]
-- [3,6,5,12,9,10,17,24,33,18,65,20,129,34,257,48,513,66,1025,36...
--
xQ ::
  (
  )
    => (a -> a -> b) -> List a -> List b
xQ func =
  go
  where
    go [] =
      []
    go (x : xs) =
      m (func x) xs :% go xs

-- | Takes a function and applies to all pairs in which the first element occurs at or before the second.
--
-- Outputs in diagonalized order so that it works with infinite lists.
--
-- For a commutative function this prevents duplication.
--
-- For a version which does not apply a function to each element and itself see 'xQ'.
xX ::
  (
  )
    => (a -> a -> b) -> List a -> List b
xX func =
  go
  where
    go [] =
      []
    go (x : xs) =
      m (func x) (x : xs) :% go xs

-- | Removes the first element that matches a predicate.
r1 ::
  (
  )
    => Predicate a -> List a -> List a
r1 _ [] =
  []
r1 pred (x : xs)
  | pred x
  =
    xs
  | otherwise
  =
    x : r1 pred xs

-- | Flip of 'r1'.
fr1 ::
  (
  )
    => List a -> Predicate a -> List a
fr1 =
  F r1

-- | Takes two lists and makes a third list alternating elements between them.
aL ::
  (
  )
    => List a -> List a -> List a
aL =
  (:%)

-- | Flip of 'aL'.
faL ::
  (
  )
    => List a -> List a -> List a
faL =
  F aL

{-# Complete (:%) #-}
-- | Infix of 'aL' that also allows pattern matching.
pattern (:%) :: List a -> List a -> List a
pattern xs :% ys <- (uaL -> (xs, ys)) where
  [] :% ys =
    ys
  (x : xs) :% ys =
    x : (ys :% xs)

-- | Takes a list and produces two lists, one of elements at even indexes and one of elements at odd indexes.
uaL ::
  (
  )
    => List a -> (List a, List a)
uaL [] =
  ([], [])
uaL (x : (uaL -> (y1, y2))) =
  ( x : y2
  , y1
  )

-- | Alternative version of 'aL' which truncates the output when one of the lists runs out.
aL' ::
  (
  )
    => List a -> List a -> List a
aL' [] _ =
  []
aL' (x : xs) ys =
  x : aL' ys xs

-- | Takes 3 lists and creates a new list alternating elements between the three.
aL3 ::
  (
  )
    => List a -> List a -> List a -> List a
aL3 [] ys zs =
  aL ys zs
aL3 (x : xs) ys zs =
  x : aL3 ys zs xs

-- | Takes 4 lists and creates a new list alternating elements between the four.
aL4 ::
  (
  )
    => List a -> List a -> List a -> List a -> List a
aL4 [] ys zs ws =
  aL3 ys zs ws
aL4 (x : xs) ys zs ws =
  x : aL4 ys zs ws xs

-- | Takes 5 lists and creates a new list alternating elements between the five.
aL5 ::
  (
  )
    => List a -> List a -> List a -> List a -> List a -> List a
aL5 [] ys zs ws vs =
  aL4 ys zs ws vs
aL5 (x : xs) ys zs ws vs =
  x : aL5 ys zs ws vs xs

-- | Takes 6 lists and creates a new list alternating elements between the six.
aL6 ::
  (
  )
    => List a -> List a -> List a -> List a -> List a -> List a -> List a
aL6 [] ys zs ws vs us =
  aL5 ys zs ws vs us
aL6 (x : xs) ys zs ws vs us =
  x : aL6 ys zs ws vs us xs

{-# Deprecated intersperse "Use is instead" #-}
-- | Long version of 'is'.
intersperse ::
  (
  )
    => a -> List a -> List a
intersperse =
  is

-- |
-- Equivalent to 'Data.List.intersperse'.
is ::
  (
  )
    => a -> List a -> List a
is =
  Data.List.intersperse

-- | Flip of 'is'.
fis ::
  (
  )
    => List a -> a -> List a
fis =
  F is

{-# Deprecated intercalate "Use ic instead" #-}
-- | Long version of 'ic'.
intercalate ::
  (
  )
    => List a -> List (List a) -> List a
intercalate =
  ic

-- |
-- Equivalent to 'Data.List.intercalate'.
ic ::
  (
  )
    => List a -> List (List a) -> List a
ic =
  Data.List.intercalate

-- | Flip of 'ic'.
fic ::
  (
  )
    => List (List a) -> List a -> List a
fic =
  F ic

-- | Inserts a value into a list at a location.
--
-- Does nothing if the index is out of bounds.
ns ::
  ( Integral i
  )
    => i -> a -> List a -> List a
ns _ _ [] =
  []
ns 0 value (_ : xs) =
  value : xs
ns index value (_ : xs) =
  ns (index - 1) value xs

-- | Flip of ns
fns ::
  ( Integral i
  )
    => a -> i -> List a -> List a
fns =
  F ns

-- | Takes a list of indices and values and inserts them into a list with 'ns'.
nS ::
  ( Integral i
  , Foldable t
  )
    => t (i, a) -> List a -> List a
nS =
  F fnS

-- | Flip of 'nS'.
fnS ::
  ( Integral i
  , Foldable t
  )
    => List a -> t (i, a) -> List a
fnS =
  rF $ Uc ns

-- | Takes an integer @n@ and a list and removes every @n@th element from that list.
--
-- ==== __Examples__
--
-- @rn 2@ will remove every other element from a list so, to get a list of all the non-negative even integers you can do @rn 2 [0..]@.
--
-- >>> rn 2 [0..]
-- [0,2,4,6,8,10,12,14,16,18,20...
--
--
rn ::
  ( Integral i
  )
    => i -> List a -> List a
rn cycle =
  go 1
  where
    go _ [] =
      []
    go depth (x : xs)
      | depth == cycle
      =
        go 1 xs
      | otherwise
      =
        x : go (depth + 1) xs

-- | Flip of 'rn'.
frn ::
  ( Integral i
  )
    => List a -> i -> List a
frn =
  F rn

{-# Deprecated group "User gr instead" #-}
-- | Long version of 'gr'.
group ::
  ( Eq a
  )
    => List a -> List (List a)
group =
  gr

-- | Splits a list into sublists with all equal elements.
--
-- Equivalent to 'Data.List.group'.
gr ::
  ( Eq a
  )
    => List a -> List (List a)
gr =
  Data.List.group

{-# Deprecated groupBy "User gW instead" #-}
-- | Long version of 'gW'.
groupBy ::
  (
  )
    => (a -> a -> Bool) -> List a -> List (List a)
groupBy =
  gW

-- |
-- Equivalent to 'Data.List.groupBy'.
gW ::
  (
  )
    => (a -> a -> Bool) -> List a -> List (List a)
gW =
  Data.List.groupBy

-- | Flip of 'gW'.
fgW ::
  (
  )
    => List a -> (a -> a -> Bool) -> List (List a)
fgW =
  F gW

-- | Takes a conversion function and a list.
-- Groups the list as if the values were the result of the conversion function.
--
-- ==== __Examples__
--
-- We can use @gB@ to group strings together based on length:
--
-- >>> gB l ["Hello", "world", "it's", "me"]
-- [["Hello","world"],["it's"],["me"]]
--
-- We could use a predicate to group a list into members that do and don't satisfy it.
-- Here we group the some integers based on whether they are @7@:
--
-- >>> gB (==7) [1 .. 9]
-- [[1,2,3,4,5,6],[7],[8,9]]
gB ::
  ( Eq b
  )
    => (a -> b) -> List a -> List (List a)
gB =
  gW < qb

-- | Flip of 'gB'.
fgB ::
  ( Eq b
  )
    => List a -> (a -> b) -> List (List a)
fgB =
  F gB
