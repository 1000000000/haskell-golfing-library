module P.Monoid
  ( Semigroup
  , Monoid
  , (<>)
  , mp
  , fmp
  , i
  , rt
  , frt
  , rt2
  , rt3
  , rt4
  , rt5
  , rt6
  , rt7
  , rt8
  , rt9
  , rt0
  , rt1
  )
  where

import qualified Prelude
import Prelude
  ( Semigroup
  , Monoid
  , Integral
  , Int
  )
import qualified Data.List

import P.Foldable
import P.Function.Flip
import P.Functor.Compose

infixr 6  <>

-- | Semigroup operation.
-- Equivalent to '(Prelude.<>)'.
-- Infix version of 'mp'.
-- More general version of 'Prelude.mappend'.
(<>) ::
  ( Semigroup a
  )
    => a -> a -> a
(<>) =
  (Prelude.<>)

-- | Semigroup operation.
-- Equivalent to '(Prelude.<>)'.
-- Prefix version of '(P.<>)'.
-- More general version of 'Prelude.mappend'.
mp ::
  ( Semigroup a
  )
    => a -> a -> a
mp =
  (<>)

-- | Flip of 'mp'.
fmp ::
  ( Semigroup a
  )
    => a -> a -> a
fmp =
  F mp

-- | Monoid identity.
-- Equivalent to 'Prelude.mempty'.
i ::
  ( Monoid a
  )
    => a
i =
  Prelude.mempty

-- | Takes an integer @n@ and an element of a 'Monoid' @m@.
-- Combines @n@ copies of @m@ using '(<>)'.
--
-- Returns 'i' if @n < 1@.
--
-- ====  __Examples__
--
-- It can be used on a list to repeat that list:
--
-- >>> rt 8 "AB"
-- "ABABABABABABABAB"
rt ::
  ( Integral i
  , Monoid m
  )
    => i -> m -> m
rt =
  mm fo Data.List.genericReplicate

-- | Flip of 'rt'.
frt ::
  ( Integral i
  , Monoid m
  )
    => m -> i -> m
frt =
  F rt

-- | Combine 2 copies of an element of a monoid with '(<>)'.
--
-- Shorthand of @'rt' 2@
rt2 ::
  ( Monoid m
  )
    => m -> m
rt2 =
  rt (2 :: Int)

-- | Combine 3 copies of an element of a monoid with '(<>)'.
--
-- Shorthand of @'rt' 3@
rt3 ::
  ( Monoid m
  )
    => m -> m
rt3 =
  rt (3 :: Int)

-- | Combine 4 copies of an element of a monoid with '(<>)'.
--
-- Shorthand of @'rt' 4@
rt4 ::
  ( Monoid m
  )
    => m -> m
rt4 =
  rt (4 :: Int)

-- | Combine 5 copies of an element of a monoid with '(<>)'.
--
-- Shorthand of @'rt' 5@
rt5 ::
  ( Monoid m
  )
    => m -> m
rt5 =
  rt (5 :: Int)

-- | Combine 6 copies of an element of a monoid with '(<>)'.
--
-- Shorthand of @'rt' 6@
rt6 ::
  ( Monoid m
  )
    => m -> m
rt6 =
  rt (6 :: Int)

-- | Combine 7 copies of an element of a monoid with '(<>)'.
--
-- Shorthand of @'rt' 7@
rt7 ::
  ( Monoid m
  )
    => m -> m
rt7 =
  rt (7 :: Int)

-- | Combine 8 copies of an element of a monoid with '(<>)'.
--
-- Shorthand of @'rt' 8@
rt8 ::
  ( Monoid m
  )
    => m -> m
rt8 =
  rt (8 :: Int)

-- | Combine 9 copies of an element of a monoid with '(<>)'.
--
-- Shorthand of @'rt' 9@
rt9 ::
  ( Monoid m
  )
    => m -> m
rt9 =
  rt (9 :: Int)

-- | Combine 10 copies of an element of a monoid with '(<>)'.
--
-- Shorthand of @'rt' 10@
--
-- For @rt 0@ do @p i@.
rt0 ::
  ( Monoid m
  )
    => m -> m
rt0 =
  rt (10 :: Int)

-- | Combine 11 copies of an element of a monoid with '(<>)'.
--
-- Shorthand of @'rt' 11@
--
-- For @rt 1@ do @id@.
rt1 ::
  ( Monoid m
  )
    => m -> m
rt1 =
  rt (11 :: Int)
