module P.Monad
  ( Monad
  , bn
  , (>~)
  , fb
  , (~<)
  , (+>)
  , (<+)
  , jn
  , fjn
  , jj
  , fjj
  , mM
  , fmM
  , m_
  , fm_
  -- * MonadPlus
  , wh
  , fl
  , (@~)
  , wn
  , fn
  , (@!)
  , er
  , fer
  , (=-)
  , cX
  , fcX
  )
  where

import qualified Prelude
import Prelude
  ( Monad
  , Traversable
  )
import qualified Control.Monad

import P.Alternative
import P.Applicative
import P.Bool
import P.Eq
import P.Foldable
import P.Function.Flip
import P.Functor.Compose

infixl 1 >~
infixr 1 ~<, +>, <+

-- |
-- Equivalent to 'Control.Monad.join'.
-- More general version of 'Data.List.concat'.
jn ::
  ( Monad m
  )
    => m (m a) -> m a
jn =
  Control.Monad.join

-- | Flip of 'jn'.
fjn ::
  (
  )
    => a -> (a -> a -> b) -> b
fjn =
  F jn

-- | Double join.
-- Applies 'jn' twice.
--
-- ==== __Examples__
--
-- Can be used to concatenate triply nested lists:
--
-- >>> jj [[[1,2,3],[4,5,6]],[[1,2,3],[5],[6,7]]]
-- [1,2,3,4,5,6,1,2,3,5,6,7]
jj ::
  ( Monad m
  )
    => m (m (m a)) -> m a
jj =
  jn < jn

-- | Flip of 'jj'.
fjj ::
  (
  )
    => b -> (b -> b -> b -> c) -> c
fjj =
  F jj

-- | Monadic bind.
-- Equivalent to '(Prelude.>>=)'.
-- More general version of 'Prelude.concatMap'.
bn ::
  ( Monad m
  )
    => m a -> (a -> m b) -> m b
bn =
  (Prelude.>>=)

-- | Monadic bind.
-- Equivalent to '(Prelude.>>=)'.
-- Infix version of 'bn'.
(>~) ::
  ( Monad m
  )
    => m a -> (a -> m b) -> m b
(>~) =
  (Prelude.>>=)

-- | Flip of 'bn'.
-- Equivalent to '(Control.Monad.=<<)'
fb ::
  ( Monad m
  )
    => (a -> m b) -> m a -> m b
fb =
  F bn

-- | Monadic bind.
-- Flip of '(>~)'
-- Equivalent to '(Contro.Monad.=<<)'.
-- Infix version of 'fb'.
(~<) ::
  ( Monad m
  )
    => (a -> m b) -> m a -> m b
(~<) =
  (Control.Monad.=<<)

-- |
-- Equivalent to 'Prelude.mapM'.
mM ::
  ( Monad m
  , Traversable t
  )
    => (a -> m b) -> t a -> m (t b)
mM =
  Prelude.mapM

-- | Flip of 'mM'.
-- Equivalent to 'Prelude.forM'.
fmM ::
  ( Monad m
  , Traversable t
  )
    => t a -> (a -> m b) -> m (t b)
fmM =
  F mM

-- | 'mM' ignoring the return value of the monad.
-- Equivalent to 'Prelude.mapM_'.
m_ ::
  ( Monad m
  , Foldable t
  )
    => (a -> m b) -> t a -> m ()
m_ =
  Prelude.mapM_

-- | Flip of 'm_'.
-- Equivalent to 'Prelude.forM_'.
fm_ ::
  ( Monad m
  , Foldable t
  )
    => t a -> (a -> m b) -> m ()
fm_ =
  F m_

-- | Kleisli composition.
--
-- Equivalent to '(Control.Monad.>=>)'.
-- Flip of '(<+)'.
(+>) ::
  ( Monad m
  )
     => (a -> m b) -> (b -> m c) -> a -> m c
(+>) =
   (Control.Monad.>=>)

-- | Kleisli composition.
--
-- Equivalent to '(Control.Monad.>=>)'.
-- Flip of '(+>)'.
(<+) ::
  ( Monad m
  )
     => (b -> m c) -> (a -> m b) -> a -> m c
(<+) =
   (Control.Monad.<=<)

-- | A positive filter.
--
-- Flip of 'fl'.
wh ::
  ( Alternative t
  , Monad t
  )
    => t a -> (a -> Bool) -> t a
wh monad1 predicate =
  do
    result <- monad1
    gu (predicate result)
    p result

-- | A positive filter.
-- Removes all values that don't pass a predicate.
--
-- Equivalent to 'Control.Monad.mfilter'.
-- More general version of 'Prelude.filter'.
fl ::
  ( Alternative t
  , Monad t
  )
    => (a -> Bool) -> t a -> t a
fl =
  F wh

-- | Infix of 'wh' and 'fl'.
(@~) ::
  ( Alternative t
  , Monad t
  )
    => t a -> (a -> Bool) -> t a
(@~) =
  wh

-- | A negative filter.
-- Removes all values that pass a predicate.
--
-- Flip of 'fn'.
wn ::
  ( Alternative t
  , Monad t
  )
    => t a -> (a -> Bool) -> t a
wn =
  F fn

-- | A negative filter.
-- Removes all values that pass a predicate.
--
-- Flip of 'wn'.
fn ::
  ( Alternative t
  , Monad t
  )
    => (a -> Bool) -> t a -> t a
fn =
  --fl .!< n
  fl < (n <)

-- | Infix of 'wn' and 'fn'.
(@!) ::
  ( Alternative t
  , Monad t
  )
    => t a -> (a -> Bool) -> t a
(@!) =
  wn

-- | Removes all of a particular element.
er ::
  ( Alternative t
  , Monad t
  , Eq a
  )
    => a -> t a -> t a
er =
  fn < (==)

-- | Flip of 'er'.
fer ::
  ( Alternative t
  , Monad t
  , Eq a
  )
    => t a -> a -> t a
fer =
  F er

-- | Infix of 'er' and 'fer'.
(=-) ::
  ( Alternative t
  , Monad t
  , Eq a
  )
    => a -> t a -> t a
(=-) =
  er

-- | Removes all elements in the first input present in the second.
cX ::
  ( Alternative t1
  , Monad t1
  , Foldable t2
  , Eq a
  )
    => t1 a -> t2 a -> t1 a
cX =
  F fcX

-- | Flip of 'cX'.
fcX ::
  ( Alternative t1
  , Monad t1
  , Foldable t2
  , Eq a
  )
    => t2 a -> t1 a -> t1 a
fcX =
  fn < fe
