{-# Language NoMonomorphismRestriction #-}
{-# Language RankNTypes #-}
module P.Function
  where

import Prelude
  (
  )
import qualified Data.Function

import P.Applicative
import P.Function.Flip
import P.Functor.Compose

infixl 5 .*

-- | Composes a function with both inputs.
-- Equivalent to 'Data.Function.on'.
on ::
  ( Functor f
  )
    => (b -> b -> c) -> f b -> f (f c)
on bifunc func =
  dcp bifunc func func

-- | Infix version of 'on'.
(.*) ::
  ( Functor f
  )
    => (b -> b -> c) -> f b -> f (f c)
(.*) =
  on

-- | Flip of 'on'.
fon ::
  ( Functor f
  )
    => f b -> (b -> b -> c) -> f (f c)
fon =
  F on

-- | If you define the function 'on' as
--
-- @
-- on biFunc func x y =
--   biFunc (func x) (func y)
-- @
--
-- There is no single most general type in Haskell.
--
-- The type inferred by Haskell is
--
-- @
-- on :: (b -> b -> c) -> (a -> b) -> a -> a -> c
-- @
--
-- (The default 'on' uses a generalization of this type.)
--
-- However we can notice that the two 'b' inputs to 'biFunc' no not need to be the same type.
-- For example we had @bifunc :: [a] -> [b] -> Int@ then our @func@ could have the type @c -> [c]@.
--
-- @pon@ is one possible other type which could be given to 'on'.
--
pOn ::
  (
  )
    => (f a -> f b -> c) -> (forall k. k -> f k) -> a -> b -> c
pOn biFunc func x y =
  biFunc (func x) (func y)

-- | Flip of 'pOn'.
fpO ::
  (
  )
    => (forall k. k -> f k) -> (f a -> f b -> c) -> a -> b -> c
-- Defined this way since 'F' doesn't play nice with RankNTypes
fpO func =
  (`pOn` func)

{-# Deprecated fpOn "Use fpO instead"#-}
-- | Long version of 'fpO'.
fpOn ::
  (
  )
    => (forall k. k -> f k) -> (f a -> f b -> c) -> a -> b -> c
-- Defined this way since 'F' doesn't play nice with RankNTypes
fpOn =
  fpO

{-# Deprecated fpOp "Use onp instead" #-}
-- | Long version of 'onp'.
fpOp ::
  ( Applicative f
  )
    => (f a -> f b -> c) -> a -> b -> c
fpOp =
  onp

-- | Takes a function on applicative functors and composes 'p' on both arguments.
onp ::
  ( Applicative f
  )
    => (f a -> f b -> c) -> a -> b -> c
onp =
  fpO p

-- | @qOn@ is an alternative minimal type for the 'on' function.
-- See 'pOn' for more information.
qOn ::
  (
  )
    => (a -> b -> c) -> (forall k. f k -> k) -> f a -> f b -> c
qOn biFunc func x =
  biFunc (func x) < func

-- | Flip of 'qOn'.
fqO ::
  (
  )
    => (forall k. f k -> k) -> (a -> b -> c) -> f a -> f b -> c
-- Defined this way since 'F' doesn't play nice with RankNTypes
fqO func biFunc x =
  biFunc (func x) < func

{-# Deprecated fqOn "Use fqO instead" #-}
-- | Long version of 'fqO'.
fqOn ::
  (
  )
    => (forall k. f k -> k) -> (a -> b -> c) -> f a -> f b -> c
-- Defined this way since 'F' doesn't play nice with RankNTypes
fqOn =
  fqO

-- | @kOn@ is an alternative minimal type for the 'on' function.
-- See 'pOn' for more information.
kOn ::
  (
  )
    => (g a -> g b -> c) -> (forall k. f k -> g k) -> f a -> f b -> c
kOn biFunc func x =
  biFunc (func x) < func

-- | Flip of 'kOn'.
fkO ::
  (
  )
    => (forall k. f k -> g k) -> (g a -> g b -> c) -> f a -> f b -> c
-- Defined this way since 'F' doesn't play nice with RankNTypes
fkO func biFunc x =
  biFunc (func x) < func

{-# Deprecated fkOn "Use fkO instead" #-}
-- | Long version of 'fkO'.
fkOn ::
  (
  )
    => (forall k. f k -> g k) -> (g a -> g b -> c) -> f a -> f b -> c
-- Defined this way since 'F' doesn't play nice with RankNTypes
fkOn =
  fkO

-- | The Y combinator.
--
-- Equivalent to 'Data.Function.fix'.
yy ::
  (
  )
    => (a -> a) -> a
yy =
  Data.Function.fix
