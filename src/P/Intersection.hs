module P.Intersection
  where

import Prelude
  ( Bool
  , otherwise
  )

import P.Aliases
import P.Eq
import P.Foldable
import P.Function.Flip
import P.Function.Identity
import P.Functor.Compose
import P.List
import P.Monad

-- | Gets the intersection of two lists.
-- The resulting list may have duplicate items,
-- and preserves the order of the first list.
--
-- For a version which does not give duplicate items see 'bI'.
--
-- ==== __Examples__
--
-- >>> nx [2,3,4,2] [4,2,1,4,2,6]
-- [2,4,2]
--
-- 2 appears twice since it appears twice in both lists.
-- Even though 4 appears twice in the second list it only appears once in the first list so it only appears once in the output.
--
-- Compare with 'fnx', which gives @[4,2,2]@.
nx ::
  ( Eq a
  )
    => List a -> List a -> List a
nx =
  nxw eq

-- | Flip of 'nx'.
-- The only difference between the two is in the ordering of the elements.
-- 'nx' gives a list where the ordering makes it a sublist of the first list, while 'fnx' makes it a sublist of the /second/ list.
--
-- ==== __Examples__
--
-- >>> fnx [2,3,4,2] [4,2,1,4,2,6]
-- [4,2,2]
--
-- Compare with 'nx', which gives @[2,4,2]@.
fnx ::
  ( Eq a
  )
    => List a -> List a -> List a
fnx =
  F nx

-- | Like 'nx' but with a user defined equality function.
nxw ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a -> List a
nxw userEq =
  go
  where
    go [] _ =
      []
    go _ [] =
      []
    go (x : xs) ys
      | ay (userEq x) ys
      =
        x : go xs (r1 (userEq x) ys)
      | otherwise
      =
        go xs ys

-- | Like 'fnx' but with a user defined equality function.
nxW ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a -> List a
nxW =
  F < nxw

-- | Like 'nx' but uses a substitution function to determine equality.
nxb ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> List a
nxb =
  nxw < qb

-- | Like 'fnx' but uses a substitution function to determine equality.
nxB ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> List a
nxB =
  F < nxb

-- | Gets the intersection of two lists.
-- The resulting list is unique on it's elements,
-- and preserves the order of the first list.
--
-- Sometimes called "bag intersection".
--
-- ==== __Examples__
--
-- >>> bI [2,3,4,2] [4,2,1,4,2,6]
-- [2,4]
--
-- Compare with 'fbI', which gives @[4,2]@.
bI ::
  ( Eq a
  )
    => List a -> List a -> List a
bI =
  bIw eq

-- | Flip of 'bI'.
-- The only difference between the two is in the ordering of the elements.
-- 'bI' gives a list where the ordering is the first occurence of each element in the first list, while 'fbI' uses the first occurence of each element in the /second/ list.
--
-- ==== __Examples__
--
-- >>> fbI [2,3,4,2] [4,2,1,4,2,6]
-- [4,2]
--
-- Compare with 'bI', which gives @[2,4]@.
fbI ::
  ( Eq a
  )
    => List a -> List a -> List a
fbI =
  F bI

-- | Like 'bI' but with a user defined equality function.
bIw ::
  (
  )
    => (a -> b -> Bool) -> List a -> List b -> List a
bIw userEq =
  go
  where
    go [] _ =
      []
    go _ [] =
      []
    go (x : xs) ys
      | ay (userEq x) ys
      =
        x : go xs (fl (userEq x) ys)
      | otherwise
      =
        go xs ys

-- | Like 'fbI' but with a user defined equality function.
bIW ::
  (
  )
    => (a -> b -> Bool) -> List a -> List b -> List b
bIW =
  F < bIw < F

-- | Like 'bI' but uses a substitution function to determine equality.
bIb ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> List a
bIb =
  bIW < qb

-- | Like 'fbI' but uses a substitution function to determine equality.
bIB ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> List a
bIB =
  bIw < qb

-- | A list difference.
-- Takes two lists and for every element of the second list
-- removes an equal element of the first list if it exists.
--
df ::
  ( Eq a
  )
    => List a -> List a -> List a
df =
  dfw eq

-- | Flip of 'df'.
fdf ::
  ( Eq a
  )
    => List a -> List a -> List a
fdf =
  F df

-- | Like 'df' but with a user defined equality function.
dfw ::
  (
  )
    => (a -> b -> Bool) -> List a -> List b -> List a
dfw userEq =
  go
  where
    go xs [] =
      xs
    go [] _ =
      []
    go (x : xs) ys
      | ay (userEq x) ys
      =
        go xs $ r1 (userEq x) ys
      | otherwise
      =
        x : go xs ys

-- | Like 'fdf' but with a user defined equality function.
dfW ::
  (
  )
    => (a -> b -> Bool) -> List a -> List b -> List b
dfW =
  F < dfw < F

-- | Like 'df' but uses a substitution function to determine equality.
dfb ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> List a
dfb =
  dfw < qb

-- | Like 'fdf' but uses a substitution function to determine equality.
dfB ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> List a
dfB =
  dfW < qb

-- | Takes lists @l1@ and @l2@ and gives all elements of @l1@ not in @l2@.
-- The resulting list contains only unique elements.
--
-- Sometimes called "bag difference".
bD ::
  ( Eq a
  , Foldable t
  )
    => List a -> t a -> List a
bD =
  bDw eq

-- | Flip of 'bD'.
fbD ::
  ( Eq a
  , Foldable t
  )
    => t a -> List a -> List a
fbD =
  F bD

-- | Like 'bD' but with a user defined equality function.
bDw ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> List a -> t a -> List a
bDw userEq =
  go
  where
    go [] _ =
      []
    go (x : xs) ys
      | ay (userEq x) ys
      =
        go xs ys
      | otherwise
      =
        x : go (fn (userEq x) xs) ys

-- | Like 'fbD' but with a user defined equality function.
bDW ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> List a -> List a
bDW =
  F < bDw

-- | Like 'bD' but uses a substitution function to determine equality.
bDb ::
  ( Eq b
  , Foldable t
  )
    => (a -> b) -> List a -> t a -> List a
bDb =
  bDw < qb

-- | Like 'fbD' but uses a substitution function to determine equality.
bDB ::
  ( Eq b
  , Foldable t
  )
    => (a -> b) -> t a -> List a -> List a
bDB =
  bDW < qb
