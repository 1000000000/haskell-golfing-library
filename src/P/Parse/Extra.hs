{-|
Module :
  P.Parse.Extra
Description :
  Additional functions for the P.Parse library
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions for parsing that have been moved to this file to reduce clutter in the main P.Parse library.
Mostly this is a large collection of parsers for a specific character.
-}
module P.Parse.Extra
  where

import Prelude
 ( Char
 , String
 )
import P.Parse

-- | Parses a single @a@ character.
--
-- Shorthand for @'χ' \'a\'@.
χa ::
  (
  )
    => Parser String Char
χa =
  χ 'a'

-- | Parses a single @a@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"a\"@.
ʃa ::
  (
  )
    => Parser String String
ʃa =
  ʃ "a"


-- | Parses a single @b@ character.
--
-- Shorthand for @'χ' \'b\'@.
χb ::
  (
  )
    => Parser String Char
χb =
  χ 'b'

-- | Parses a single @b@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"b\"@.
ʃb ::
  (
  )
    => Parser String String
ʃb =
  ʃ "b"


-- | Parses a single @c@ character.
--
-- Shorthand for @'χ' \'c\'@.
χc ::
  (
  )
    => Parser String Char
χc =
  χ 'c'

-- | Parses a single @c@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"c\"@.
ʃc ::
  (
  )
    => Parser String String
ʃc =
  ʃ "c"


-- | Parses a single @d@ character.
--
-- Shorthand for @'χ' \'d\'@.
χd ::
  (
  )
    => Parser String Char
χd =
  χ 'd'

-- | Parses a single @d@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"d\"@.
ʃd ::
  (
  )
    => Parser String String
ʃd =
  ʃ "d"


-- | Parses a single @e@ character.
--
-- Shorthand for @'χ' \'e\'@.
χe ::
  (
  )
    => Parser String Char
χe =
  χ 'e'

-- | Parses a single @e@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"e\"@.
ʃe ::
  (
  )
    => Parser String String
ʃe =
  ʃ "e"


-- | Parses a single @f@ character.
--
-- Shorthand for @'χ' \'f\'@.
χf ::
  (
  )
    => Parser String Char
χf =
  χ 'f'

-- | Parses a single @f@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"f\"@.
ʃf ::
  (
  )
    => Parser String String
ʃf =
  ʃ "f"


-- | Parses a single @g@ character.
--
-- Shorthand for @'χ' \'g\'@.
χg ::
  (
  )
    => Parser String Char
χg =
  χ 'g'

-- | Parses a single @g@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"g\"@.
ʃg ::
  (
  )
    => Parser String String
ʃg =
  ʃ "g"


-- | Parses a single @h@ character.
--
-- Shorthand for @'χ' \'h\'@.
χh ::
  (
  )
    => Parser String Char
χh =
  χ 'h'

-- | Parses a single @h@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"h\"@.
ʃh ::
  (
  )
    => Parser String String
ʃh =
  ʃ "h"


-- | Parses a single @i@ character.
--
-- Shorthand for @'χ' \'i\'@.
χi ::
  (
  )
    => Parser String Char
χi =
  χ 'i'

-- | Parses a single @i@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"i\"@.
ʃi ::
  (
  )
    => Parser String String
ʃi =
  ʃ "i"


-- | Parses a single @j@ character.
--
-- Shorthand for @'χ' \'j\'@.
χj ::
  (
  )
    => Parser String Char
χj =
  χ 'j'

-- | Parses a single @j@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"j\"@.
ʃj ::
  (
  )
    => Parser String String
ʃj =
  ʃ "j"


-- | Parses a single @k@ character.
--
-- Shorthand for @'χ' \'k\'@.
χk ::
  (
  )
    => Parser String Char
χk =
  χ 'k'

-- | Parses a single @k@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"k\"@.
ʃk ::
  (
  )
    => Parser String String
ʃk =
  ʃ "k"


-- | Parses a single @l@ character.
--
-- Shorthand for @'χ' \'l\'@.
χl ::
  (
  )
    => Parser String Char
χl =
  χ 'l'

-- | Parses a single @l@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"l\"@.
ʃl ::
  (
  )
    => Parser String String
ʃl =
  ʃ "l"


-- | Parses a single @m@ character.
--
-- Shorthand for @'χ' \'m\'@.
χm ::
  (
  )
    => Parser String Char
χm =
  χ 'm'

-- | Parses a single @m@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"m\"@.
ʃm ::
  (
  )
    => Parser String String
ʃm =
  ʃ "m"


-- | Parses a single @n@ character.
--
-- Shorthand for @'χ' \'n\'@.
χn ::
  (
  )
    => Parser String Char
χn =
  χ 'n'

-- | Parses a single @n@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"n\"@.
ʃn ::
  (
  )
    => Parser String String
ʃn =
  ʃ "n"


-- | Parses a single @o@ character.
--
-- Shorthand for @'χ' \'o\'@.
χo ::
  (
  )
    => Parser String Char
χo =
  χ 'o'

-- | Parses a single @o@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"o\"@.
ʃo ::
  (
  )
    => Parser String String
ʃo =
  ʃ "o"


-- | Parses a single @p@ character.
--
-- Shorthand for @'χ' \'p\'@.
χp ::
  (
  )
    => Parser String Char
χp =
  χ 'p'

-- | Parses a single @p@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"p\"@.
ʃp ::
  (
  )
    => Parser String String
ʃp =
  ʃ "p"


-- | Parses a single @q@ character.
--
-- Shorthand for @'χ' \'q\'@.
χq ::
  (
  )
    => Parser String Char
χq =
  χ 'q'

-- | Parses a single @q@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"q\"@.
ʃq ::
  (
  )
    => Parser String String
ʃq =
  ʃ "q"


-- | Parses a single @r@ character.
--
-- Shorthand for @'χ' \'r\'@.
χr ::
  (
  )
    => Parser String Char
χr =
  χ 'r'

-- | Parses a single @r@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"r\"@.
ʃr ::
  (
  )
    => Parser String String
ʃr =
  ʃ "r"


-- | Parses a single @s@ character.
--
-- Shorthand for @'χ' \'s\'@.
χs ::
  (
  )
    => Parser String Char
χs =
  χ 's'

-- | Parses a single @s@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"s\"@.
ʃs ::
  (
  )
    => Parser String String
ʃs =
  ʃ "s"


-- | Parses a single @t@ character.
--
-- Shorthand for @'χ' \'t\'@.
χt ::
  (
  )
    => Parser String Char
χt =
  χ 't'

-- | Parses a single @t@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"t\"@.
ʃt ::
  (
  )
    => Parser String String
ʃt =
  ʃ "t"


-- | Parses a single @u@ character.
--
-- Shorthand for @'χ' \'u\'@.
χu ::
  (
  )
    => Parser String Char
χu =
  χ 'u'

-- | Parses a single @u@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"u\"@.
ʃu ::
  (
  )
    => Parser String String
ʃu =
  ʃ "u"


-- | Parses a single @v@ character.
--
-- Shorthand for @'χ' \'v\'@.
χv ::
  (
  )
    => Parser String Char
χv =
  χ 'v'

-- | Parses a single @v@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"v\"@.
ʃv ::
  (
  )
    => Parser String String
ʃv =
  ʃ "v"


-- | Parses a single @w@ character.
--
-- Shorthand for @'χ' \'w\'@.
χw ::
  (
  )
    => Parser String Char
χw =
  χ 'w'

-- | Parses a single @w@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"w\"@.
ʃw ::
  (
  )
    => Parser String String
ʃw =
  ʃ "w"


-- | Parses a single @x@ character.
--
-- Shorthand for @'χ' \'x\'@.
χx ::
  (
  )
    => Parser String Char
χx =
  χ 'x'

-- | Parses a single @x@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"x\"@.
ʃx ::
  (
  )
    => Parser String String
ʃx =
  ʃ "x"


-- | Parses a single @y@ character.
--
-- Shorthand for @'χ' \'y\'@.
χy ::
  (
  )
    => Parser String Char
χy =
  χ 'y'

-- | Parses a single @y@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"y\"@.
ʃy ::
  (
  )
    => Parser String String
ʃy =
  ʃ "y"


-- | Parses a single @z@ character.
--
-- Shorthand for @'χ' \'z\'@.
χz ::
  (
  )
    => Parser String Char
χz =
  χ 'z'

-- | Parses a single @z@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"z\"@.
ʃz ::
  (
  )
    => Parser String String
ʃz =
  ʃ "z"


-- | Parses a single @A@ character.
--
-- Shorthand for @'χ' \'A\'@.
χA ::
  (
  )
    => Parser String Char
χA =
  χ 'A'

-- | Parses a single @A@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"A\"@.
ʃA ::
  (
  )
    => Parser String String
ʃA =
  ʃ "A"


-- | Parses a single @B@ character.
--
-- Shorthand for @'χ' \'B\'@.
χB ::
  (
  )
    => Parser String Char
χB =
  χ 'B'

-- | Parses a single @B@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"B\"@.
ʃB ::
  (
  )
    => Parser String String
ʃB =
  ʃ "B"


-- | Parses a single @C@ character.
--
-- Shorthand for @'χ' \'C\'@.
χC ::
  (
  )
    => Parser String Char
χC =
  χ 'C'

-- | Parses a single @C@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"C\"@.
ʃC ::
  (
  )
    => Parser String String
ʃC =
  ʃ "C"


-- | Parses a single @D@ character.
--
-- Shorthand for @'χ' \'D\'@.
χD ::
  (
  )
    => Parser String Char
χD =
  χ 'D'

-- | Parses a single @D@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"D\"@.
ʃD ::
  (
  )
    => Parser String String
ʃD =
  ʃ "D"


-- | Parses a single @E@ character.
--
-- Shorthand for @'χ' \'E\'@.
χE ::
  (
  )
    => Parser String Char
χE =
  χ 'E'

-- | Parses a single @E@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"E\"@.
ʃE ::
  (
  )
    => Parser String String
ʃE =
  ʃ "E"


-- | Parses a single @F@ character.
--
-- Shorthand for @'χ' \'F\'@.
χF ::
  (
  )
    => Parser String Char
χF =
  χ 'F'

-- | Parses a single @F@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"F\"@.
ʃF ::
  (
  )
    => Parser String String
ʃF =
  ʃ "F"


-- | Parses a single @G@ character.
--
-- Shorthand for @'χ' \'G\'@.
χG ::
  (
  )
    => Parser String Char
χG =
  χ 'G'

-- | Parses a single @G@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"G\"@.
ʃG ::
  (
  )
    => Parser String String
ʃG =
  ʃ "G"


-- | Parses a single @H@ character.
--
-- Shorthand for @'χ' \'H\'@.
χH ::
  (
  )
    => Parser String Char
χH =
  χ 'H'

-- | Parses a single @H@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"H\"@.
ʃH ::
  (
  )
    => Parser String String
ʃH =
  ʃ "H"


-- | Parses a single @I@ character.
--
-- Shorthand for @'χ' \'I\'@.
χI ::
  (
  )
    => Parser String Char
χI =
  χ 'I'

-- | Parses a single @I@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"I\"@.
ʃI ::
  (
  )
    => Parser String String
ʃI =
  ʃ "I"


-- | Parses a single @J@ character.
--
-- Shorthand for @'χ' \'J\'@.
χJ ::
  (
  )
    => Parser String Char
χJ =
  χ 'J'

-- | Parses a single @J@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"J\"@.
ʃJ ::
  (
  )
    => Parser String String
ʃJ =
  ʃ "J"


-- | Parses a single @K@ character.
--
-- Shorthand for @'χ' \'K\'@.
χK ::
  (
  )
    => Parser String Char
χK =
  χ 'K'

-- | Parses a single @K@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"K\"@.
ʃK ::
  (
  )
    => Parser String String
ʃK =
  ʃ "K"


-- | Parses a single @L@ character.
--
-- Shorthand for @'χ' \'L\'@.
χL ::
  (
  )
    => Parser String Char
χL =
  χ 'L'

-- | Parses a single @L@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"L\"@.
ʃL ::
  (
  )
    => Parser String String
ʃL =
  ʃ "L"


-- | Parses a single @M@ character.
--
-- Shorthand for @'χ' \'M\'@.
χM ::
  (
  )
    => Parser String Char
χM =
  χ 'M'

-- | Parses a single @M@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"M\"@.
ʃM ::
  (
  )
    => Parser String String
ʃM =
  ʃ "M"


-- | Parses a single @N@ character.
--
-- Shorthand for @'χ' \'N\'@.
χN ::
  (
  )
    => Parser String Char
χN =
  χ 'N'

-- | Parses a single @N@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"N\"@.
ʃN ::
  (
  )
    => Parser String String
ʃN =
  ʃ "N"


-- | Parses a single @O@ character.
--
-- Shorthand for @'χ' \'O\'@.
χO ::
  (
  )
    => Parser String Char
χO =
  χ 'O'

-- | Parses a single @O@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"O\"@.
ʃO ::
  (
  )
    => Parser String String
ʃO =
  ʃ "O"


-- | Parses a single @P@ character.
--
-- Shorthand for @'χ' \'P\'@.
χP ::
  (
  )
    => Parser String Char
χP =
  χ 'P'

-- | Parses a single @P@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"P\"@.
ʃP ::
  (
  )
    => Parser String String
ʃP =
  ʃ "P"


-- | Parses a single @Q@ character.
--
-- Shorthand for @'χ' \'Q\'@.
χQ ::
  (
  )
    => Parser String Char
χQ =
  χ 'Q'

-- | Parses a single @Q@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"Q\"@.
ʃQ ::
  (
  )
    => Parser String String
ʃQ =
  ʃ "Q"


-- | Parses a single @R@ character.
--
-- Shorthand for @'χ' \'R\'@.
χR ::
  (
  )
    => Parser String Char
χR =
  χ 'R'

-- | Parses a single @R@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"R\"@.
ʃR ::
  (
  )
    => Parser String String
ʃR =
  ʃ "R"


-- | Parses a single @S@ character.
--
-- Shorthand for @'χ' \'S\'@.
χS ::
  (
  )
    => Parser String Char
χS =
  χ 'S'

-- | Parses a single @S@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"S\"@.
ʃS ::
  (
  )
    => Parser String String
ʃS =
  ʃ "S"


-- | Parses a single @T@ character.
--
-- Shorthand for @'χ' \'T\'@.
χT ::
  (
  )
    => Parser String Char
χT =
  χ 'T'

-- | Parses a single @T@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"T\"@.
ʃT ::
  (
  )
    => Parser String String
ʃT =
  ʃ "T"


-- | Parses a single @U@ character.
--
-- Shorthand for @'χ' \'U\'@.
χU ::
  (
  )
    => Parser String Char
χU =
  χ 'U'

-- | Parses a single @U@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"U\"@.
ʃU ::
  (
  )
    => Parser String String
ʃU =
  ʃ "U"


-- | Parses a single @V@ character.
--
-- Shorthand for @'χ' \'V\'@.
χV ::
  (
  )
    => Parser String Char
χV =
  χ 'V'

-- | Parses a single @V@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"V\"@.
ʃV ::
  (
  )
    => Parser String String
ʃV =
  ʃ "V"


-- | Parses a single @W@ character.
--
-- Shorthand for @'χ' \'W\'@.
χW ::
  (
  )
    => Parser String Char
χW =
  χ 'W'

-- | Parses a single @W@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"W\"@.
ʃW ::
  (
  )
    => Parser String String
ʃW =
  ʃ "W"


-- | Parses a single @X@ character.
--
-- Shorthand for @'χ' \'X\'@.
χX ::
  (
  )
    => Parser String Char
χX =
  χ 'X'

-- | Parses a single @X@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"X\"@.
ʃX ::
  (
  )
    => Parser String String
ʃX =
  ʃ "X"


-- | Parses a single @Y@ character.
--
-- Shorthand for @'χ' \'Y\'@.
χY ::
  (
  )
    => Parser String Char
χY =
  χ 'Y'

-- | Parses a single @Y@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"Y\"@.
ʃY ::
  (
  )
    => Parser String String
ʃY =
  ʃ "Y"


-- | Parses a single @Z@ character.
--
-- Shorthand for @'χ' \'Z\'@.
χZ ::
  (
  )
    => Parser String Char
χZ =
  χ 'Z'

-- | Parses a single @Z@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"Z\"@.
ʃZ ::
  (
  )
    => Parser String String
ʃZ =
  ʃ "Z"


-- | Parses a single @0@ character.
--
-- Shorthand for @'χ' \'0\'@.
χ0 ::
  (
  )
    => Parser String Char
χ0 =
  χ '0'

-- | Parses a single @0@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"0\"@.
ʃ0 ::
  (
  )
    => Parser String String
ʃ0 =
  ʃ "0"


-- | Parses a single @1@ character.
--
-- Shorthand for @'χ' \'1\'@.
χ1 ::
  (
  )
    => Parser String Char
χ1 =
  χ '1'

-- | Parses a single @1@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"1\"@.
ʃ1 ::
  (
  )
    => Parser String String
ʃ1 =
  ʃ "1"


-- | Parses a single @2@ character.
--
-- Shorthand for @'χ' \'2\'@.
χ2 ::
  (
  )
    => Parser String Char
χ2 =
  χ '2'

-- | Parses a single @2@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"2\"@.
ʃ2 ::
  (
  )
    => Parser String String
ʃ2 =
  ʃ "2"


-- | Parses a single @3@ character.
--
-- Shorthand for @'χ' \'3\'@.
χ3 ::
  (
  )
    => Parser String Char
χ3 =
  χ '3'

-- | Parses a single @3@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"3\"@.
ʃ3 ::
  (
  )
    => Parser String String
ʃ3 =
  ʃ "3"


-- | Parses a single @4@ character.
--
-- Shorthand for @'χ' \'4\'@.
χ4 ::
  (
  )
    => Parser String Char
χ4 =
  χ '4'

-- | Parses a single @4@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"4\"@.
ʃ4 ::
  (
  )
    => Parser String String
ʃ4 =
  ʃ "4"


-- | Parses a single @5@ character.
--
-- Shorthand for @'χ' \'5\'@.
χ5 ::
  (
  )
    => Parser String Char
χ5 =
  χ '5'

-- | Parses a single @5@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"5\"@.
ʃ5 ::
  (
  )
    => Parser String String
ʃ5 =
  ʃ "5"


-- | Parses a single @6@ character.
--
-- Shorthand for @'χ' \'6\'@.
χ6 ::
  (
  )
    => Parser String Char
χ6 =
  χ '6'

-- | Parses a single @6@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"6\"@.
ʃ6 ::
  (
  )
    => Parser String String
ʃ6 =
  ʃ "6"


-- | Parses a single @7@ character.
--
-- Shorthand for @'χ' \'7\'@.
χ7 ::
  (
  )
    => Parser String Char
χ7 =
  χ '7'

-- | Parses a single @7@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"7\"@.
ʃ7 ::
  (
  )
    => Parser String String
ʃ7 =
  ʃ "7"


-- | Parses a single @8@ character.
--
-- Shorthand for @'χ' \'8\'@.
χ8 ::
  (
  )
    => Parser String Char
χ8 =
  χ '8'

-- | Parses a single @8@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"8\"@.
ʃ8 ::
  (
  )
    => Parser String String
ʃ8 =
  ʃ "8"


-- | Parses a single @9@ character.
--
-- Shorthand for @'χ' \'9\'@.
χ9 ::
  (
  )
    => Parser String Char
χ9 =
  χ '9'

-- | Parses a single @9@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"9\"@.
ʃ9 ::
  (
  )
    => Parser String String
ʃ9 =
  ʃ "9"


-- | Parses a single @_@ character.
--
-- Shorthand for @'χ' \'_\'@.
χ_ ::
  (
  )
    => Parser String Char
χ_ =
  χ '_'

-- | Parses a single @_@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"_\"@.
ʃ_ ::
  (
  )
    => Parser String String
ʃ_ =
  ʃ "_"


-- | Parses a single @'@ character.
--
-- Shorthand for @'χ' \''\'@.
χ' ::
  (
  )
    => Parser String Char
χ' =
  χ '\''

-- | Parses a single @'@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"'\"@.
ʃ' ::
  (
  )
    => Parser String String
ʃ' =
  ʃ "'"

-- | Parses a single @)@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\")\"@.
cls ::
  (
  )
    => Parser String String
cls =
  ʃ ")"

-- | Parses a single @(@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"(\"@.
opn ::
  (
  )
    => Parser String String
opn =
  ʃ "("


