{-# Language PatternSynonyms #-}
{-|
Module :
  P.Pattern
Description :
  A collection of pattern synonyms for use in code golf.
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A collection of pattern synonyms for use in code golf.
-}
module P.Pattern
  where

import P.Aliases

infixr 0 :=

-- Can't make this complete for all the types but here are a couple of common types
{-# Complete (:=) :: (->) #-}
{-# Complete (:=) :: Bool #-}
{-# Complete (:=) :: List #-}
{-# Complete (:=) :: Int #-}
{-# Complete (:=) :: Integer #-}
{-# Complete (:=) :: Char #-}
{-# Complete (:=) :: (,) #-}
{-# Complete (:=) :: (,,) #-}
-- | Allows you to match two patterns at once on a parameter.
--
-- If one of the parmeters is trivial try @\@@ instead.
--
-- For example
--
-- @
-- f[](x:=1:_)
-- @
-- @
-- f[]x@(1:_)
-- @
--
-- ==== __Examples__
--
-- If you want to match the first and last elements of a list you can try
--
-- @
-- f (x : y :> z)
-- @
--
-- However this only matches lists of length 2 or greater, if you want it to also match on lists of length 1 you can use @(:=)@
--
-- @
-- f (x : y := z :> _)
-- @
--
pattern (:=) :: a -> a -> a
pattern x := y <- x@y
