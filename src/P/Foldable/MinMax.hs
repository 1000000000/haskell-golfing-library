module P.Foldable.MinMax
  where

import Prelude
  ( Foldable
  )
import qualified Data.List

import P.Function.Flip
import P.Functor.Compose
import P.Ord

-- | Takes a foldable and returns the maximum element.
--
-- Equivalent to 'Data.List.maximum'.
--
-- This errors on an empty structure.
mx ::
  ( Ord a
  , Foldable t
  )
    => t a -> a
mx =
  Data.List.maximum

-- | Takes a foldable and returns the minimum element.
--
-- Equivalent to 'Data.List.maximum'.
--
-- This errors on an empty structure.
mn ::
  ( Ord a
  , Foldable t
  )
    => t a -> a
mn =
  Data.List.minimum

-- | Gives the largest element of a structure with respect to a provided comparison.
--
-- Equivalent to 'Data.List.maximumBy'.
--
-- This errors on an empty structure.
xW ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> t a -> a
xW =
  Data.List.maximumBy

-- | Flip of 'xW'.
fxW ::
  ( Foldable t
  )
    => t a -> (a -> a -> Ordering) -> a
fxW =
  F xW

-- | Gives the smallest element of a structure with respect to a provided comparison.
--
-- Equivalent to 'Data.List.minimumBy'.
--
-- This errors on an empty structure.
mW ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> t a -> a
mW =
  Data.List.minimumBy

-- | Flip of 'mW'.
fmW ::
  ( Foldable t
  )
    => t a -> (a -> a -> Ordering) -> a
fmW =
  F mW

-- | Takes a conversion function and a list.
-- Gives the maximum element of the list as if the elements were the result of the conversion function.
--
-- This errors on an empty structure.
--
-- ==== __Examples__
--
-- We can use @xB sh@ to get the lexographical maximum:
--
-- >>> xB sh (0 ## 25)
-- 9
--
-- We can use @xB l@ to get the longest string:
--
-- >>> xB l ["aardvark", "apple", "beard", "beaver", "cat", "chord"]
-- "aardvark"
xB ::
  ( Ord b
  , Foldable t
  )
    => (a -> b) -> t a -> a
xB =
  xW < cb

-- | Flip of 'xB'.
fxB ::
  ( Ord b
  , Foldable t
  )
    => t a -> (a -> b) -> a
fxB =
  F xB

-- | Takes a conversion function and a list.
-- Gives the minimum element of the list as if the elements were the result of the conversion function.
--
-- This errors on an empty structure.
--
-- ==== __Examples__
--
-- We can use @mB sh@ to get the lexographical minimum:
--
-- >>> mB sh (5 ## 25)
-- 10
--
-- We can use @mB l@ to get the shortest string:
--
-- >>> mB l ["aardvark", "apple", "beard", "beaver", "cat", "chord"]
-- "cat"
mB ::
  ( Ord b
  , Foldable t
  )
    => (a -> b) -> t a -> a
mB =
  mW < cb

-- | Flip of 'mB'.
fmB ::
  ( Ord b
  , Foldable t
  )
    => t a -> (a -> b) -> a
fmB =
  F mB
