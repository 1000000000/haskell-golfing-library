module P.Eq
  ( Eq
  , (==)
  , eq
  , (/=)
  , nq
  , qb
  , nqb
  , Eq1
  , q1
  , liftEq
  )
  where

import qualified Prelude
import Prelude
  ( Eq
  , Bool
  )
import qualified Data.Functor.Classes
import Data.Functor.Classes
  ( Eq1
  )

import P.Function

infixr 4  ==, /=

-- | Equality.
--
-- Equivalent to '(Prelude.==)'.
(==) ::
  ( Eq a
  )
    => a -> a -> Bool
(==) =
  (Prelude.==)

-- | Equality.
eq ::
  ( Eq a
  )
    => a -> a -> Bool
eq =
  (==)

-- | Inequality.
--
-- Equivalent to '(Prelude./=)'
(/=) ::
  ( Eq a
  )
    => a -> a -> Bool
(/=) =
  (Prelude./=)

-- | Inequality.
nq ::
  ( Eq a
  )
    => a -> a -> Bool
nq =
  (/=)

-- | Checks equality after applying a substitution function.
--
-- ==== __Examples__
--
-- @qb(%2)@ checks equality mod 2
--
-- >>> qb (% 2) 11 95
-- True
--
-- >>> qb (% 2) 54 17
-- False
--
qb ::
  ( Eq b
  )
    => (a -> b) -> a -> a -> Bool
qb =
  on eq

-- | Checks inequality after applying a substitution function.
--
-- Negation of 'qb'.
--
-- ==== __Examples__
--
-- @nqb(%2)@ checks inequality mod 2
--
-- >>> nqb (% 2) 11 95
-- False
--
-- >>> nqb (% 2) 54 17
-- True
--
nqb ::
  ( Eq b
  )
    => (a -> b) -> a -> a -> Bool
nqb =
  on nq

{-# Deprecated liftEq "Use q1 instead" #-}
-- | long version of 'q1'.
liftEq ::
  ( Eq1 f
  )
    => (a -> b -> Bool) -> f a -> f b -> Bool
liftEq =
  Data.Functor.Classes.liftEq

-- | Equivalent to 'liftEq'.
q1 ::
  ( Eq1 f
  )
    => (a -> b -> Bool) -> f a -> f b -> Bool
q1 =
  Data.Functor.Classes.liftEq

