module P.Alternative
  ( Alternative
  , em
  , (++)
  , ao
  , fao
  , so
  , my
  , sO
  , mY
  , gu
  , cx
  , cs
  , (<|)
  , fcs
  , ec
  , (|>)
  , fec
  )
  where

import Prelude
  ( Bool
  , Monoid
  )
import qualified Control.Applicative
import Control.Applicative
  ( Alternative
  )
import qualified Control.Monad

import P.Aliases
import P.Foldable
import P.Function.Flip
import P.Functor.Compose
import P.Applicative

infixr 5 ++

-- | 'Alternative' is a monoid on applicative functors.
-- 'em' is the identity of that monoid.
--
-- Equivalent to 'Control.Applicative.empty'.
em ::
  ( Alternative t
  )
    => t a
em =
  Control.Applicative.empty

-- | 'Alternative' is a monoid on applicative functors.
-- '(++)' is the associative action of that monoid.
--
-- Equivalent to '(Control.Applicative.<|>)'.
--
-- More general version of '(Prelude.++)'.
(++) ::
  ( Alternative t
  )
    => t a -> t a -> t a
(++) =
  (Control.Applicative.<|>)

-- | 'Alternative' is a monoid on applicative functors.
-- 'ao' is the associative action of that monoid.
--
-- Equivalent to '(Control.Applicative.<|>)'.
--
-- More general version of '(Prelude.++)'.
ao ::
  ( Alternative t
  )
    => t a -> t a -> t a
ao =
  (++)

-- | Flip of 'ao'.
fao ::
  ( Alternative t
  )
    => t a -> t a -> t a
fao =
  F ao

-- |
-- Equivalent to 'Control.Applicative.some'.
so ::
  ( Alternative t
  )
    => t a -> t (List a)
so =
  Control.Applicative.some

-- | Equivalent to 'Control.Applicative.many'.
my ::
  ( Alternative t
  )
    => t a -> t (List a)
my =
  Control.Applicative.many

-- | Works like 'so', but the result is combined into one element using the monoidal product.
sO ::
  ( Monoid a
  , Alternative t
  )
    => t a -> t a
sO =
  m fo < so

-- | Works like 'my', but the result is combined into one element using the monoidal product.
mY ::
  ( Alternative t
  , Monoid a
  )
    => t a -> t a
mY =
  m fo < so

-- | Takes a boolean and is 'em' when false and @p ()@ when true.
--
-- Equivalent to 'Control.Monad.guard'.
gu ::
  ( Alternative t
  )
    => Bool -> t ()
gu =
  Control.Monad.guard

-- | Takes multiple alternatives and combines them with `(++)`.
--
-- More general version of 'Prelude.concat'.
--
-- ==== __Examples__
--
-- Can be used to concat lists:
--
-- >>> cx [[1,2,3],[2,3,3],[2,2]]
-- [1,2,3,2,3,3,2,2]
--
cx ::
  ( Alternative m
  , Foldable t
  )
    => t (m a) -> m a
cx =
  rF (++) em

-- | Adds an element to the front of a structure; usually a list.
cs ::
  ( Alternative f
  )
    => a -> f a -> f a
cs =
  (++) < p

-- | Infix of 'cs'.
--
-- For 'List's use @(:)@ instead.
(<|) ::
  ( Alternative f
  )
    => a -> f a -> f a
(<|) =
  cs

-- | Flip of 'cs'.
fcs ::
  ( Alternative f
  )
    => f a -> a -> f a
fcs =
  F cs

-- | Adds an element to the end of a structure; usually a list.
ec ::
  ( Alternative f
  )
    => f a -> a -> f a
ec =
  fm p < (++)

-- | Infix of 'ec'.
(|>) ::
  ( Alternative f
  )
    => f a -> a -> f a
(|>) =
  ec

-- | Flip of 'ec'
fec ::
  ( Alternative f
  )
    => a -> f a -> f a
fec =
  F ec
