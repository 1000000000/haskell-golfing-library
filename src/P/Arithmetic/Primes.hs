{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Arithmetic.Primes
  where

import P.Aliases
import P.Bool
import P.Function.Flip
import P.Functor.Compose
import P.List

-- | An infinite list type to reduce warnings in this module
data IList a =
  a :.. IList a

-- | An internal version of 'a'' using an infinite list to reduce warnings.
_a' ::
  ( Integral i
  )
    => IList i
_a' =
  2 :.. go [3,5..]
  where
    go (x : xs) =
      x :.. go (rn x xs)

-- | An infinite list of all prime numbers.
--
-- Becomes time and memory intensive the farther into the list you read.
a' ::
  ( Integral i
  )
    => List i
a' =
  go _a'
  where
    go (x :.. xs) =
      x : go xs

-- | Determines if the first argument is divisible by the second.
by ::
  ( Integral i
  )
    => i -> i -> Bool
by =
  (== 0) .^ mod

-- | Determines if the first argument is divisible by the second.
(|?) ::
  ( Integral i
  )
    => i -> i -> Bool
(|?) =
  by

-- | Flip of 'by'.
fby ::
  ( Integral i
  )
    => i -> i -> Bool
fby =
  F by

-- | Determines if the first argument is not divisible by the second.
--
-- Gives the opposite result as 'by'.
nby ::
  ( Integral i
  )
    => i -> i -> Bool
nby =
  n .^ by

-- | Infix of 'nby'.
(/|?) ::
  ( Integral i
  )
    => i -> i -> Bool
(/|?) =
  nby

-- | Flip of 'nby'.
fnb ::
  ( Integral i
  )
    => i -> i -> Bool
fnb =
  F nby

-- | Determines if an integer is prime.
--
-- Uses trial division up to the square root.
-- Not efficient for primes with no small factors.
i' ::
  ( Integral i
  )
    => i -> Bool
i' =
  go _a'
  where
    go (prime :.. primes) x
      | prime * prime > x
      =
        T
      | otherwise
      =
        x /|? prime && go primes x

-- |
-- === Function ===
-- As a function this determines the prime factors of a positive integer.
--
-- Result is in ascending order.
--
-- Uses trial division not efficient for primes with no small factors.
--
-- === Pattern ===
-- As a pattern this matches the product of a list.
--
-- It does not require all the elements to be prime nor does it require them to be in ascending order.
pattern F' ::
  ( Integral i
  )
    => i -> List i
pattern F' x <- (product -> x) where
  F' =
    go _a'
    where
      go (prime :.. primes) x
        | prime * prime > x
        =
          [x]
        | (factor, 0) <- divMod x prime
        =
          prime : go (prime :.. primes) factor
        | otherwise
        =
          go primes x

-- | A pattern which matches only primes.
pattern I' ::
  ( Integral i
  )
    => i
pattern I' <- (i' -> T)
