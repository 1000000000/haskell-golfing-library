{-# Language ViewPatterns #-}
{-# Language PatternSynonyms #-}
{-|
Module :
  P.Arithmetic.Pattern
Description :
  Short patterns to aid with golfing arithmetic.
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

  Short patterns to aid with golfing arithmetic.
-}
module P.Arithmetic.Pattern
  where

import P.Arithmetic
import P.Bool

{-# Complete X, NP :: Integer #-}
{-# Complete X, NP :: Int #-}
{-# Complete N, NN :: Integer #-}
{-# Complete N, NN :: Int #-}
-- | A pattern which matches any positive number
--
-- ==== __Examples__
--
-- You can implement a sign function using @X@
--
-- @
-- sign 0 = 0
-- sign X = 1
-- sign _ = -1
-- @
--
pattern X ::
  ( Num a
  , Ord a
  )
    => a
pattern X <- ((Prelude.> 0) -> T)

-- | A pattern which matches any negative number
--
-- ==== __Examples__
--
-- You can implement a sign function using @N@
--
-- @
-- sign 0 = 0
-- sign N = -1
-- sign _ = 1
-- @
--
pattern N ::
  ( Num a
  , Ord a
  )
    => a
pattern N <- ((Prelude.< 0) -> T)

-- | A pattern which matches any non-positive number.
pattern NP ::
  ( Num a
  , Ord a
  )
    => a
pattern NP <- ((Prelude.<= 0) -> T)

-- | A pattern which matches any non-negative number.
pattern NN ::
  ( Num a
  , Ord a
  )
    => a
pattern NN <- ((Prelude.>= 0) -> T)

-- | A pattern which matches any non-zero number.
pattern NZ ::
  ( Num a
  , Eq a
  )
    => a
pattern NZ <- ((Prelude./= 0) -> T)

{-# Complete P1 :: Int #-}
{-# Complete P1 :: Integer #-}
-- | Add 1 to a number.
--
-- Also functions as a pattern.
--
-- ==== __Examples__
--
-- Here's a definition of function @x#n@ which makes a list of @n@ copies of @x@.
--
-- @
-- x#0=[];x#P1 y=x:x#y
-- @
--
-- Here's the same function defined without the pattern.
--
-- @
-- x?0=[];x?y=x:x?(y-1)
-- @
--
-- The pattern saves a byte here.
pattern P1 ::
  ( Num a
  )
    => a -> a
pattern P1 x <- (sb 1 -> x) where
  P1 =
    pl 1

{-# Complete P2 :: Int #-}
{-# Complete P2 :: Integer #-}
-- | Add 2 to a number.
--
-- Also functions as a pattern
--
pattern P2 ::
  ( Num a
  )
    => a -> a
pattern P2 x <- (sb 2 -> x) where
  P2 =
    pl 2

{-# Complete P3 :: Integer #-}
{-# Complete P3 :: Int #-}
-- | Add 3 to a number.
--
-- Also functions as a pattern
--
pattern P3 ::
  ( Num a
  )
    => a -> a
pattern P3 x <- (sb 3 -> x) where
  P3 =
    pl 3

{-# Complete P4 :: Integer #-}
{-# Complete P4 :: Int #-}
-- | Add 4 to a number.
--
-- Also functions as a pattern
--
pattern P4 ::
  ( Num a
  )
    => a -> a
pattern P4 x <- (sb 4 -> x) where
  P4 =
    pl 4

{-# Complete P5 :: Integer #-}
{-# Complete P5 :: Int #-}
-- | Add 5 to a number.
--
-- Also functions as a pattern
--
pattern P5 ::
  ( Num a
  )
    => a -> a
pattern P5 x <- (sb 5 -> x) where
  P5 =
    pl 5

{-# Complete P6 :: Integer #-}
{-# Complete P6 :: Int #-}
-- | Add 6 to a number.
--
-- Also functions as a pattern
--
pattern P6 ::
  ( Num a
  )
    => a -> a
pattern P6 x <- (sb 6 -> x) where
  P6 =
    pl 6

{-# Complete P7 :: Integer #-}
{-# Complete P7 :: Int #-}
-- | Add 7 to a number.
--
-- Also functions as a pattern
--
pattern P7 ::
  ( Num a
  )
    => a -> a
pattern P7 x <- (sb 7 -> x) where
  P7 =
    pl 7

{-# Complete P8 :: Integer #-}
{-# Complete P8 :: Int #-}
-- | Add 8 to a number.
--
-- Also functions as a pattern
--
pattern P8 ::
  ( Num a
  )
    => a -> a
pattern P8 x <- (sb 8 -> x) where
  P8 =
    pl 8

{-# Complete P9 :: Integer #-}
{-# Complete P9 :: Int #-}
-- | Add 9 to a number.
--
-- Also functions as a pattern
--
pattern P9 ::
  ( Num a
  )
    => a -> a
pattern P9 x <- (sb 9 -> x) where
  P9 =
    pl 9

{-# Complete P0 :: Integer #-}
{-# Complete P0 :: Int #-}
-- | Add 10 to a number.
--
-- Also functions as a pattern
--
pattern P0 ::
  ( Num a
  )
    => a -> a
pattern P0 x <- (sb 10 -> x) where
  P0 =
    pl 10

{-# Complete S1 :: Integer #-}
{-# Complete S1 :: Int #-}
-- | Subtract 1 from a number.
--
-- Also functions as a pattern.
pattern S1 ::
  ( Num a
  )
    => a -> a
pattern S1 x <- (pl 1 -> x) where
  S1 =
    sb 1

{-# Complete S2 :: Integer #-}
{-# Complete S2 :: Int #-}
-- | Subtract 2 from a number.
--
-- Also functions as a pattern.
pattern S2 ::
  ( Num a
  )
    => a -> a
pattern S2 x <- (pl 2 -> x) where
  S2 =
    sb 2

{-# Complete S3 :: Integer #-}
{-# Complete S3 :: Int #-}
-- | Subtract 3 from a number.
--
-- Also functions as a pattern.
pattern S3 ::
  ( Num a
  )
    => a -> a
pattern S3 x <- (pl 3 -> x) where
  S3 =
    sb 3

{-# Complete S4 :: Integer #-}
{-# Complete S4 :: Int #-}
-- | Subtract 4 from a number.
--
-- Also functions as a pattern.
pattern S4 ::
  ( Num a
  )
    => a -> a
pattern S4 x <- (pl 4 -> x) where
  S4 =
    sb 4

{-# Complete S5 :: Integer #-}
{-# Complete S5 :: Int #-}
-- | Subtract 5 from a number.
--
-- Also functions as a pattern.
pattern S5 ::
  ( Num a
  )
    => a -> a
pattern S5 x <- (pl 5 -> x) where
  S5 =
    sb 5

{-# Complete S6 :: Integer #-}
{-# Complete S6 :: Int #-}
-- | Subtract 6 from a number.
--
-- Also functions as a pattern.
pattern S6 ::
  ( Num a
  )
    => a -> a
pattern S6 x <- (pl 6 -> x) where
  S6 =
    sb 6

{-# Complete S7 :: Integer #-}
{-# Complete S7 :: Int #-}
-- | Subtract 7 from a number.
--
-- Also functions as a pattern.
pattern S7 ::
  ( Num a
  )
    => a -> a
pattern S7 x <- (pl 7 -> x) where
  S7 =
    sb 7

{-# Complete S8 :: Integer #-}
{-# Complete S8 :: Int #-}
-- | Subtract 8 from a number.
--
-- Also functions as a pattern.
pattern S8 ::
  ( Num a
  )
    => a -> a
pattern S8 x <- (pl 8 -> x) where
  S8 =
    sb 8

{-# Complete S9 :: Integer #-}
{-# Complete S9 :: Int #-}
-- | Subtract 9 from a number.
--
-- Also functions as a pattern.
pattern S9 ::
  ( Num a
  )
    => a -> a
pattern S9 x <- (pl 9 -> x) where
  S9 =
    sb 9

{-# Complete S0 :: Integer #-}
{-# Complete S0 :: Int #-}
-- | Subtract 10 from a number.
--
-- To subtract 0 use 'id'.
--
-- Also functions as a pattern.
pattern S0 ::
  ( Num a
  )
    => a -> a
pattern S0 x <- (pl 10 -> x) where
  S0 =
    sb 10
