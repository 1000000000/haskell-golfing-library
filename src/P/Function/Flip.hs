{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Function.Flip
  where

import qualified Prelude
import Prelude
  ( Functor
  )

import P.Aliases

{-# Deprecated flip "Use F instead" #-}
-- | Long version of 'F'.
-- Takes a function and swaps the first two arguments.
flip ::
  (
  )
    => (a -> b -> c) -> (b -> a -> c)
flip =
  F

{-# Complete F #-}
-- | Takes a function and swaps the first two arguments.
--
-- Equivalent to 'Prelude.flip'.
pattern F ::
  (
  )
    => (a -> b -> c) -> (b -> a -> c)
pattern F x <- (Prelude.flip -> x) where
  F =
    Prelude.flip

-- | Flip of 'F'
ff ::
  (
  )
    => b -> (a -> b -> c) -> a -> c
ff =
  F F

{-# Complete F2 :: List #-}
{-# Complete F2 :: (->) #-}
-- | A mapped flip.
--
-- Intended to be used to swap the second and third arguments of a function.
--
-- @
-- F2 :: (a -> b -> c -> d) -> (a -> c -> b -> d)
-- @
--
-- However it has a more general type signature.
pattern F2 ::
  ( Functor f
  )
    => f (a -> b -> c) -> f (b -> a -> c)
pattern F2 x <- (Prelude.fmap F -> x) where
  F2 =
    Prelude.fmap F

-- | The flip of 'F2'.
fF2 ::
  (
  )
    => a -> (a -> b -> c -> d) -> c -> b -> d
fF2 =
  F F2
