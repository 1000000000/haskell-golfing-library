module P.Function.Identity
  where

-- Needed for documentation
import qualified Prelude
import Prelude
  (
  )

import P.Function.Flip

infixr 0  $
infixl 1  &

-- | Identity function.
--
-- Equivalent to 'Prelude.id'.
id ::
  (
  )
    => a -> a
id x =
  x

-- | Identity infix.
--
-- More general version of '(Prelude.$)' with the same precedence.
($) ::
  (
  )
    => a -> a
($) =
  id

-- | Flip of identity.
fid ::
  (
  )
    => a -> (a -> b) -> b
fid =
  F id

-- | Elm's pipe operator and the flip of '($)'.
--
-- ==== __Examples__
--
-- >>> [1 ..] & dr 12 & tk 5
-- [13,14,15,16,17]
(&) ::
  (
  )
    => a -> (a -> b) -> b
(&) =
  F id
