{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Function.Curry
  where

import qualified Prelude
import Prelude
  (
  )

import P.Function.Flip

{-# Complete Cu #-}
-- |
-- Equivalent to 'Prelude.curry'.
pattern Cu ::
  (
  )
    => ((a, b) -> c) -> (a -> b -> c)
pattern Cu x <- (Prelude.uncurry -> x) where
  Cu =
    Prelude.curry

-- | Flip of 'Cu'.
fCu ::
  (
  )
    => a -> ((a, b) -> c) -> b -> c
fCu =
  F Cu

{-# Complete Cuf #-}
-- | Curries with 'Cu' then flips result.
pattern Cuf ::
  (
  )
    => ((a, b) -> c) -> (b -> a -> c)
pattern Cuf x =
  F (Cu x)

-- | Flip of 'Cuf'.
fCf ::
  (
  )
    => b -> ((a, b) -> c) -> a -> c
fCf =
  F Cuf

{-# Complete Uc #-}
-- |
-- Equivalent to 'Prelude.uncurry'.
pattern Uc ::
  (
  )
    => (a -> b -> c) -> (a, b) -> c
pattern Uc x <- (Prelude.curry -> x) where
  Uc =
    Prelude.uncurry

-- | Flip of 'Uc'.
fUc ::
  (
  )
    => (a, b) -> (a -> b -> c) -> c
fUc =
  F Uc

{-# Complete Ucf #-}
-- | Flips the input and uncurries the result with 'Uc'.
pattern Ucf ::
  (
  )
    => (a -> b -> c) -> ((b, a) -> c)
pattern Ucf x =
  Uc (F x)

-- | Flip of 'Ucf'.
fUf ::
  (
  )
    => (b, a) -> (a -> b -> c) -> c
fUf =
  F Ucf

-- | Internal function.
_cu3 ::
  (
  )
    => ((a, b, c) -> d) -> (a -> b -> c -> d)
_cu3 func x1 x2 x3 =
  func (x1, x2, x3)

-- | Internal function
_uc3 ::
  (
  )
    => (a -> b -> c -> d) -> ((a, b, c) -> d)
_uc3 func (x1, x2, x3) =
  func x1 x2 x3

-- | Curry a 3 tuple
{-# Complete Cu3 #-}
pattern Cu3 ::
  (
  )
    => ((a, b, c) -> d) -> (a -> b -> c -> d)
pattern Cu3 x <- (_uc3 -> x) where
  Cu3 =
    _cu3

-- | Flip of 'Cu3'.
fC3 ::
  (
  )
    => a -> ((a, b, c) -> d) -> b -> c -> d
fC3 =
  F Cu3

-- | Uncurry a function which takes 3 arguments to function that takes a 3 tuple.
{-# Complete Uc3 #-}
pattern Uc3 ::
  (
  )
    => (a -> b -> c -> d) -> ((a, b, c) -> d)
pattern Uc3 x <- (_cu3 -> x) where
  Uc3 =
    _uc3

-- | Flip of 'Uc3'.
fU3 ::
  (
  )
    => (a, b, c) -> (a -> b -> c -> d) -> d
fU3 =
  F Uc3
