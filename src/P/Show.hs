module P.Show
  ( Show
  , sh
  , show
  )
  where

import qualified Prelude
import Prelude
  ( Show
  , String
  )

{-# Deprecated show "Use sh instead" #-}
-- | A long version of 'sh'.
show ::
  ( Show a
  )
    => a -> String
show =
  Prelude.show

-- | Converts to string.
sh ::
  ( Show a
  )
    => a -> String
sh =
  Prelude.show
