module P.List.Sort
  where

import Prelude
  (
  )
import qualified Data.List

import P.Aliases
import P.Function.Flip
import P.Function
import P.Functor.Compose
import P.Ord

{-# Deprecated sort "Use sr instead"#-}
-- | Long version of 'sr'.
-- Sorts a list.
sort ::
  ( Ord a
  )
    => List a -> List a
sort =
  sr

-- | Sorts a list in ascending order.
-- Equivalent to 'Data.List.sort'.
sr ::
  ( Ord a
  )
    => List a -> List a
sr =
  Data.List.sort

-- | Sorts a list in descending order.
rsr ::
  ( Ord a
  )
    => List a -> List a
rsr =
  rsW cp

{-# Deprecated sortBy "Use sW instead"#-}
-- | Long version of 'sW'.
-- Sorts a list using a user defined comparison.
sortBy ::
  (
  )
    => (a -> a -> Ordering) -> List a -> List a
sortBy =
  sW

-- | Sorts a list using a user defined comparison.
--
-- Equivalent to 'Data.List.sortBy'.
sW ::
  (
  )
    => (a -> a -> Ordering) -> List a -> List a
sW =
  Data.List.sortBy

-- | Flip of 'sW'.
fsW ::
  (
  )
    => List a -> (a -> a -> Ordering) -> List a
fsW =
  F sW

-- | Sorts a list in descending order list using a ordering function.
rsW ::
  (
  )
    => (a -> a -> Ordering) -> List a -> List a
rsW  =
  sW < RC

-- | Flip of 'rsW'.
fRW ::
  (
  )
    => List a -> (a -> a -> Ordering) -> List a
fRW  =
  F rsW

{-# Deprecated sortOn "Use sB instead"#-}
-- | Long version of 'sB'.
-- Sorts the list in ascending order as if the values were the result of the conversion function.
sortOn ::
  ( Ord b
  )
    => (a -> b) -> List a -> List a
sortOn =
  sB

-- | Takes a conversion function and a list.
-- Sorts the list in ascending order as if the values were the result of the conversion function.
--
-- Equivalent to 'Data.List.sortOn'.
--
-- ==== __Examples__
--
-- If we use 'sh', then we sort by lexographical order:
--
-- >>> sB sh (0 ## 25)
-- [0,1,10,11,12,13,14,15,16,17,18,19,2,20,21,22,23,24,25,3,4,5,6,7,8,9]
--
-- If we use 'l', then we sort by length:
--
-- >>> sB l ["Sort", "me", "by", "length", "!"]
-- ["!","me","by","Sort","length"]
sB ::
  ( Ord b
  )
    => (a -> b) -> List a -> List a
sB =
  Data.List.sortOn

-- | Flip of 'sB'.
fsB ::
  ( Ord b
  )
    => List a -> (a -> b) -> List a
fsB =
  F sB

-- | Takes a conversion function and a list.
-- Sorts the list in descending order as if the values were the result of the conversion function.
--
-- ==== __Examples__
--
-- If we use `sh`, then we sort by reverse lexographical order:
--
-- >>> rsB sh (0 '##' 25)
-- [9,8,7,6,5,4,3,25,24,23,22,21,20,2,19,18,17,16,15,14,13,12,11,10,1,0]
--
-- If we use 'l', then we sort by length:
--
-- >>> rsB l ["Sort", "me", "by", "length", "!"]
-- ["length","Sort","me","by","!"]
rsB ::
  ( Ord b
  )
    => (a -> b) -> List a -> List a
rsB =
  sW < on fcp

-- | Flip of 'rsB'.
fRB ::
  ( Ord b
  )
    => List a -> (a -> b) -> List a
fRB =
  F rsB

{-# Deprecated frsB "Use fRB instead" #-}
-- | Long version of 'fRB'.
-- Flip of 'rsB'.
frsB ::
  ( Ord b
  )
    => List a -> (a -> b) -> List a
frsB =
  fRB
