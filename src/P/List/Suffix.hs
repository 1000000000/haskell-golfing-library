{-# Language NoMonomorphismRestriction #-}
module P.List.Suffix
  where

import qualified Prelude
import Prelude
  ( Bool
  , Integral
  , Int
  , otherwise
  )
import qualified Data.Function

import P.Aliases
import P.Eq
import P.Foldable
import P.Function.Flip
import P.Function.Identity
import P.Functor.Compose
import P.List
import P.List.Prefix

-- | Give all suffixes of a given list in order of decreasing size.
sx ::
  (
  )
    => List a -> List (List a)
sx =
  rS (:) []

-- | Give all suffixes of a given list in order of increasing size.
-- Will stall after the first suffix for infinite lists.
sX ::
  (
  )
    => List a -> List (List a)
sX =
  ([] :) < tl < rv < sx

-- | Maps a function over the prefixes of a list in order of decreasing size.
ms ::
  (
  )
    => (List a -> b) -> List a -> List b
ms func =
  m func < sx

-- | Maps a function over the suffixes of a list in order of increasing size.
mS ::
  (
  )
    => (List a -> b) -> List a -> List b
mS func =
  m func < sX

-- | Takes two lists and determines whether the first list ends with the second.
ew ::
  ( Eq a
  )
    => List a -> List a -> Bool
ew =
  F e < sx

-- | Infix version of 'ew'.
(>%) ::
  ( Eq a
  )
    => List a -> List a -> Bool
(>%) =
  ew

-- | Flip of 'ew'.
few ::
  ( Eq a
  )
    => List a -> List a -> Bool
few =
  F ew

-- | Negation of 'ew'.
new ::
  ( Eq a
  )
    => List a -> List a -> Bool
new =
  mm Prelude.not ew

-- | Works like 'ew' but with a user defined equality function.
ewW ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a -> Bool
ewW equals =
  F (ay < q1 equals) < sx

-- | Flip of 'ewW'.
fEW ::
  (
  )
    => List a -> (a -> a -> Bool) -> List a -> Bool
fEW =
  F ewW

-- | Works like 'ew' but with a substitution function mapped across both inputs.
ewB ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> Bool
ewB =
  Data.Function.on ew < m

-- | Flip of 'ewB'.
fEB ::
  ( Eq b
  )
    => List a -> (a -> b) -> List a -> Bool
fEB =
  F ewB

-- | Gives the longest common suffix of two lists.
lx ::
  ( Eq a
  )
    => List a -> List a -> List a
lx =
  lsw eq

-- | 'lx' but with a user defined comparison.
lsw ::
  (
  )
    => (a -> b -> Bool) -> List a -> List b -> List a
lsw predicate list1 list2 =
  rv $ lpw predicate (rv list1) (rv list2)

-- | Works like 'lx' but with a substitution function mapped across both inputs.
lsb ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> List a
lsb =
  lsw < qb

-- | Given @n@ and @xs@ gives a suffix of length @n@ from @xs@, or all of @xs@ if @n@ is greater than the length of @xs@.
yv ::
  ( Integral i
  )
    => i -> List a -> List a
yv n list
  -- For the case of infinite lists since they cannot be reversed.
  | n Prelude.< 1
  =
    []
  | otherwise
  =
    rv $ tk n $ rv list

-- | Flip of 'yv'.
fY ::
  ( Integral i
  )
    => List a -> i -> List a
fY =
  F yv

-- | Gets the last 1 element from a list.
yv1 ::
  (
  )
    => List a -> List a
yv1 =
  yv (1 :: Int)

-- | Gets the last 2 elements from a list.
yv2 ::
  (
  )
    => List a -> List a
yv2 =
  yv (2 :: Int)

-- | Gets the last 3 elements from a list.
yv3 ::
  (
  )
    => List a -> List a
yv3 =
  yv (3 :: Int)

-- | Gets the last 4 elements from a list.
yv4 ::
  (
  )
    => List a -> List a
yv4 =
  yv (4 :: Int)

-- | Gets the last 5 elements from a list.
yv5 ::
  (
  )
    => List a -> List a
yv5 =
  yv (5 :: Int)

-- | Gets the last 6 elements from a list.
yv6 ::
  (
  )
    => List a -> List a
yv6 =
  yv (6 :: Int)

-- | Gets the last 7 elements from a list.
yv7 ::
  (
  )
    => List a -> List a
yv7 =
  yv (7 :: Int)

-- | Gets the last 8 elements from a list.
yv8 ::
  (
  )
    => List a -> List a
yv8 =
  yv (8 :: Int)

-- | Gets the last 9 elements from a list.
yv9 ::
  (
  )
    => List a -> List a
yv9 =
  yv (9 :: Int)

-- | Gets the last 10 elements from a list.
yv0 ::
  (
  )
    => List a -> List a
yv0 =
  yv (10 :: Int)

-- | Gives the longest suffix where all elements satisfy a predicate.
xh ::
  (
  )
    => Predicate a -> List a -> List a
xh =
  tW <.^ Rv

-- | Flip of 'xh'.
fxh ::
  (
  )
    => List a -> Predicate a -> List a
fxh =
  F xh
