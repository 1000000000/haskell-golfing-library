module P.List.Infix
  where

import qualified Prelude
import Prelude
  ( Bool
    (..)
  )
import qualified Data.Function

import P.Aliases
import P.Eq
import P.Function.Flip
import P.Function.Identity
import P.Functor.Compose
import P.Foldable
import P.Foldable.MinMax
import P.List
import P.List.Suffix
import P.List.Prefix
import P.Monad

-- | Gives all non-empty contiguous substrings of a given string.
--
-- === __Examples
--
-- >>> cg $ 1 ## 3
-- [[1],[2],[1,2],[3],[2,3],[1,2,3]]
cg ::
  (
  )
    => List a -> List (List a)
cg =
  (tl < px) +> (tl < sX)

-- | Takes two lists and determines whether the first list contains the second as an infix.
--
-- Equivalent to 'Data.List.isInfixOf'.
iw ::
  ( Eq a
  )
    => List a -> List a -> Bool
iw =
  iwW (==)

-- | Infix version of 'iw'.
(%=) ::
  ( Eq a
  )
    => List a -> List a -> Bool
(%=) =
  iw

-- | Flip of 'iw'.
fiw ::
  ( Eq a
  )
    => List a -> List a -> Bool
fiw =
  F iw

-- | Negation of 'iw'.
niw ::
  ( Eq a
  )
    => List a -> List a -> Bool
niw =
  mm Prelude.not iw

-- | Works like 'iw' but with a user defined equality function.
iwW ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a -> Bool
iwW equals =
  F $ sx .< ay < F (swW equals)

-- | Flip of 'iwW'.
fIW ::
  (
  )
    => List a -> (a -> a -> Bool) -> List a -> Bool
fIW =
  F iwW

-- | Works like 'iw' but with a substitution function mapped across both inputs.
iWB ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> Bool
iWB =
  Data.Function.on iw < m

-- | Flip of 'iWB'.
fIB ::
  ( Eq b
  )
    => List a -> (a -> b) -> List a -> Bool
fIB =
  F iWB

-- | Gives the largest infix where all elements satisfy a certain predicate.
ih ::
  (
  )
    => Predicate a -> List a -> List a
ih =
  xB l .^ gB

-- | Flip of 'ih'.
fih ::
  (
  )
    => List a -> Predicate a -> List a
fih =
  F ih
