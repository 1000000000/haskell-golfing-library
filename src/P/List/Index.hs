module P.List.Index
  where

import qualified Prelude
import Prelude
  ( Integral
  )

import P.Aliases
import P.Arithmetic
import P.Arithmetic.Pattern
import P.Foldable
import P.Function.Flip
import P.Function.Identity
import P.Functor.Compose
import P.Monad

-- | Indexes a list.
--
-- Takes the modulo of the index by the length of the list,
-- so that indices that would be out of bounds still give results.
--
-- It tries to index once and only tries the modulo if that fails,
-- so this will work if you try to index an infinite list with a non-negative number.
--
-- Indexing an empty list causes an error.
(!) ::
  ( Integral i
  )
    => List a -> i -> a
list ! index@N =
  list ! (index % l list)
list ! index =
  case
    list !! index
  of
    [ result ] ->
      result
    _ ->
      (Prelude.!!) list $
      Prelude.fromInteger $
      Prelude.toInteger $
      index % l list

-- | Takes a list of indexes and a list to index and produces a list of results.
--
-- Uses '(!)' for indexing so out of bounds indices will wrap around.
--
-- Errors when the list to be indexed is empty.
--
-- ==== __Examples__
--
-- >>> im [0,1,-2,0,4] [1,2,3]
-- [1,2,2,1,1]
im ::
  ( Integral i
  )
    => List i -> List a -> List a
im =
  F fim

-- | Flip of 'im'.
fim ::
  ( Integral i
  )
    => List a -> List i -> List a
fim =
  m < (!)

-- | Infix of 'im' and 'fim'.
(>!) ::
  ( Integral i
  )
    => List a -> List i -> List a
(>!) =
  fim

-- | Takes a list of indexes and a list to index and produces a list of results.
--
-- Uses '(!!)' for indexing so out of bounds indices will be ignored in the output.
--
-- ==== __Examples__
--
-- >>> iM [0,1,-2,0,4] [1,2,3]
-- [1,2,1]
--
-- >>> iM [1,2,3,6,0,-4] []
-- []
iM ::
  ( Integral i
  )
    => List i -> List a -> List a
iM =
  F fiM

-- | Flip of 'iM'
fiM ::
  ( Integral i
  )
    => List a -> List i -> List a
fiM =
  fb < (!!)

-- | Infix of 'iM' and 'fiM'.
(!<) ::
  ( Integral i
  )
    => List a -> List i -> List a
(!<) =
  fiM
