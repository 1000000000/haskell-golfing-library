module P.List.Prefix
  where

import qualified Prelude
import Prelude
  ( Bool
    (..)
  , Integral
  , Int
  , otherwise
  )
import qualified Data.Function
import qualified Data.List

import P.Aliases
import P.Applicative
import P.Eq
import P.Foldable
import P.Function.Flip
import P.Function.Identity
import P.Functor.Compose
import P.List
import P.Monoid

-- | Give all prefixes of a given list in order of increasing size.
px ::
  (
  )
    => List a -> List (List a)
px =
  lS (F $ F (<>) < p) []

-- | Give all prefixes of a given list in order of decreasing size.
pX ::
  (
  )
    => List a -> List (List a)
pX [] =
  [[]]
pX x =
  x : pX (nt x)

-- | Maps a function over the prefixes of a list in order of decreasing size.
ƥ ::
  (
  )
    => (List a -> b) -> List a -> List b
ƥ func =
  m func < px

-- | Maps a function over the prefixes of a list in order of increasing size.
mP ::
  (
  )
    => (List a -> b) -> List a -> List b
mP func =
  m func < pX

-- | Takes two lists and determines whether the first list starts with the second.
sw ::
  ( Eq a
  )
    => List a -> List a -> Bool
sw =
  swW (==)

-- | Infix version of 'sw'.
(%<) ::
  ( Eq a
  )
    => List a -> List a -> Bool
(%<) =
  sw

-- | Flip of 'sw'.
fsw ::
  ( Eq a
  )
    => List a -> List a -> Bool
fsw =
  F sw

-- | Negation of 'sw'.
nsw ::
  ( Eq a
  )
    => List a -> List a -> Bool
nsw =
  mm Prelude.not sw

-- | Works like 'sw' but with a user defined equality function.
swW ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a -> Bool
swW _ _ [] =
  True
swW _ [] _ =
  False
swW equals (x : xs) (y : ys) =
  (equals x y) Prelude.&& (swW equals xs ys)

-- | Flip of 'swW'.
fSW ::
  (
  )
    => List a -> (a -> a -> Bool) -> List a -> Bool
fSW =
  F swW

-- | Works like 'sw' but with a substitution function mapped across both inputs.
swB ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> Bool
swB =
  Data.Function.on sw < m

-- | Flip of 'swB'.
fSB ::
  ( Eq b
  )
    => List a -> (a -> b) -> List a -> Bool
fSB =
  F swB

-- | Gives the longest common prefix of two lists.
lp ::
  ( Eq a
  )
    => List a -> List a -> List a
lp =
  lpw eq

-- | 'lp' but with a user defined comparison.
--
-- ==== __Examples__
--
-- >>> lpw (>=) [9..] [0,2..]
-- [9,10,11,12,13,14,15,16,17,18]
lpw ::
  (
  )
    => (a -> b -> Bool) -> List a -> List b -> List a
lpw predicate =
  go
  where
    go [] _ =
      []
    go _ [] =
      []
    go (x1 : xs) (y1 : ys)
      | predicate x1 y1
      =
        x1 : go xs ys
      | otherwise
      =
        []

-- | Works like 'lp' but with a substitution function mapped across both inputs.
lpb ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> List a
lpb =
  lpw < qb

-- | Given @n@ and @xs@ gives a prefix of length @n@ from @xs@, or all of @xs@ if @n@ is greater than the length of @xs@.
--
-- Equivalent to 'Data.List.genericTake'.
tk ::
  ( Integral i
  )
    => i -> List a -> List a
tk =
  Data.List.genericTake

-- | Infix version of 'tk'.
(#<) ::
  ( Integral i
  )
    => i -> List a -> List a
(#<) =
  tk

-- | Flip of 'tk'.
ft ::
  ( Integral i
  )
    => List a -> i -> List a
ft =
  F tk

-- | Takes 1 element from a list.
tk1 ::
  (
  )
    => List a -> List a
tk1 =
  tk (1 :: Int)

-- | Takes 2 elements from a list.
tk2 ::
  (
  )
    => List a -> List a
tk2 =
  tk (2 :: Int)

-- | Takes 3 elements from a list.
tk3 ::
  (
  )
    => List a -> List a
tk3 =
  tk (3 :: Int)

-- | Takes 4 elements from a list.
tk4 ::
  (
  )
    => List a -> List a
tk4 =
  tk (4 :: Int)

-- | Takes 5 elements from a list.
tk5 ::
  (
  )
    => List a -> List a
tk5 =
  tk (5 :: Int)

-- | Takes 6 elements from a list.
tk6 ::
  (
  )
    => List a -> List a
tk6 =
  tk (6 :: Int)

-- | Takes 7 elements from a list.
tk7 ::
  (
  )
    => List a -> List a
tk7 =
  tk (7 :: Int)

-- | Takes 8 elements from a list.
tk8 ::
  (
  )
    => List a -> List a
tk8 =
  tk (8 :: Int)

-- | Takes 9 elements from a list.
tk9 ::
  (
  )
    => List a -> List a
tk9 =
  tk (9 :: Int)

-- | Takes 10 elements from a list.
tk0 ::
  (
  )
    => List a -> List a
tk0 =
  tk (10 :: Int)

-- | Takes a predicate and a list and returns the largest prefix of the list such that every element satisfies the given predicate.
--
-- Equivalent to 'Data.List.takeWhile'.
tW ::
  (
  )
    => Predicate a -> List a -> List a
tW =
  Data.List.takeWhile

-- | Flip of 'tW'.
ftW ::
  (
  )
    => List a -> Predicate a -> List a
ftW =
  F tW

-- | Takes a predicate and a list giving a tuple containing the longest prefix for which every element satisfies the predicate and the remainder of the list.
--
-- Equivalent to 'Prelude.span'
--
-- ==== __Examples__
--
-- >>> sp (> 3) [8,6,7,2,3,4]
-- ([8,6,7],[2,3,4])
--
sp ::
  (
  )
    => Predicate a -> List a -> (List a, List a)
sp =
  Prelude.span

-- | Takes a predicate and a list giving a tuple containing the longest prefix for which every element fails the predicate and the remainder of the list.
--
-- Equivalent to 'Prelude.break'.
--
-- ==== __Examples__
--
-- >>> bk (<= 3) [8,6,7,2,3,4]
-- ([8,6,7],[2,3,4])
--
bk ::
  (
  )
    => Predicate a -> List a -> (List a, List a)
bk =
  Prelude.break

-- | Similar to 'sp' except it drops the element that passes entirely.
-- This means it takes predicate and a list and returns the longest prefix of elements that don't satisfy the predicate and all the elements after the first element that satisfies the predicate.
--
-- ==== __Examples__
--
-- Combine with 'iW' to split off the first word of a string.
--
-- >>> sy iW "Hello I'm a string!"
-- ("Hello", "I'm a string!")
--
sy ::
  (
  )
    => Predicate a -> List a -> (List a, List a)
sy =
  dr1 .^^ bk

-- | Like 'sy' except instead of a predicate it takes a value and finds the first element equal to the value to split at.
sl ::
  ( Eq a
  )
    => a -> List a -> (List a, List a)
sl =
  sy < eq
