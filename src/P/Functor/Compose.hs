{-|
Module :
  P.Functor.Compose Description :
  A set of functor combinators for the P library
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Please do not use this in real code.
-}
module P.Functor.Compose
  ( Functor
  , m
  , fm
  , (<)
  , pM
  , (<$)
  , fpM
  , ($>)
  , mm
  , (.^)
  , fM
  , mX
  , (^.)
  , fmX
  , m'
  , fm'
  , mof
  , m3
  , (.^^)
  , fm3
  , fMF
  , (.<)
  , zz
  , (<.<)
  , fzz
  , z2
  , (<.^)
  , fz2
  , mcm
  , fMM
  , mc2
  , (.!<)
  , fM2
  , dcp
  , fdC
  )
  where

-- Needed for documentation
import qualified Prelude
import qualified Control.Monad
import qualified Data.Functor
import Data.Functor
  ( Functor
  )

import P.Function.Flip

infixr 9 <, .<, .^, .^^, .!<, <.<, <.^
infixl 4 <$

-- |
-- Equivalent to 'Data.Functor.fmap'.
m ::
  ( Functor f
  )
    => (a -> b) -> f a -> f b
m =
  Data.Functor.fmap

-- | Flip of 'm'.
fm ::
  ( Functor f
  )
    => f a -> (a -> b) -> f b
fm =
  F m

-- | Infix version of 'm'.
--
-- Not named @(.)@ to avoid issues with module name parsing.
--
-- More general version of '(Prelude..)'.
-- Equivalent to '(Data.Functor.<$>)'
(<) ::
  ( Functor f
  )
    => (a -> b) -> f a -> f b
(<) =
  m

-- | Prefix version of '(<$)'.
pM ::
  ( Functor f
  )
    => a -> f b -> f a
pM =
  (<$)

-- | Replace all elements of a functor with some fixed element.
--
-- Equivalent to '(Data.Functor.<$)'.
--
-- ==== __Examples__
--
-- >>> "hi" <$ (0 ## 5)
-- ["hi","hi","hi","hi","hi","hi"]
--
(<$) ::
  ( Functor f
  )
    => a -> f b -> f a
(<$) =
  (Data.Functor.<$)

-- | Flip of 'pM'.
fpM ::
  ( Functor f
  )
    => f a -> b -> f b
fpM =
  F pM

-- | Flip of '(<$)'.
($>) ::
  ( Functor f
  )
    => f a -> b -> f b
($>) =
  (Data.Functor.$>)

-- | A double map.
-- Maps two levels deep into a functor.
mm ::
  ( Functor f
  , Functor g
  )
    => (a -> b) -> f (g a) -> f (g b)
mm =
  m' m

-- | Infix version of 'mm'.
--
-- ==== __Examples__
--
-- Can be used to compose functions over the second argument.
--
-- For example @mp@ takes two lists and concatenates them.
-- If we want a function that takes two lists, concats them and then takes the first 3 we can do @tk 3 .^ mp@.
--
-- >>> (tk 3 .^ mp) [1,2] [3,4]
-- [1,2,3]
--
(.^) ::
  ( Functor f
  , Functor g
  )
    => (a -> b) -> f (g a) -> f (g b)
(.^) =
  mm

-- | Flip of 'mm'.
fM ::
  ( Functor f
  , Functor g
  )
    => f (g a) -> (a -> b) -> f (g b)
fM =
  F mm

-- | Composes a function on the second argument of a binary function.
--
-- This has a more general type but can be used as:
--
-- @
-- mX :: (a -> b) -> (c -> b -> d) -> c -> a -> d
-- @
mX ::
  ( Functor f
  , Functor g
  )
    => g a -> f (a -> b) -> f (g b)
mX =
  m < fm

-- | Flip of 'mX'.
fmX ::
  ( Functor f
  , Functor g
  )
    => f (a -> b) -> g a -> f (g b)
fmX =
  F mX

-- | Infix of 'mX' and 'fmX'.
(^.) ::
  ( Functor f
  , Functor g
  )
    => f (a -> b) -> g a -> f (g b)
(^.) =
  F mX

-- | Maps map.
m' ::
  ( Functor f
  , Functor g
  )
    => f (a -> b) -> f (g a -> g b)
m' =
  m m

-- | Flip of 'm''.
fm' ::
  ( Functor f
  )
    => a -> (a -> b -> c) -> f b -> f c
fm' =
  F m'

-- | Works like 'm'' works on functions, but with the first argument of a binary function instead of the second.
--
-- Compare the specialized type of 'm''
--
-- @
-- ( Functor f
-- )
--   => (a -> b -> c) -> a -> f a -> f b
-- @
--
-- With the type of 'mof'.
mof ::
  ( Functor f
  )
    => (a -> b -> c) -> f a -> b -> f c
mof =
  F < m' < F

-- | Flip of 'mof'.
fMF ::
  ( Functor f
  )
    => f a -> (a -> b -> c) -> b -> f c
fMF =
  F mof

-- | A combinator for a common point-free idiom.
--
-- While this has a more general type signature this is most useful in the form:
--
-- @
-- (.\<) :: (a -> b) -> (c -> b -> d) -> (c -> a -> d)
-- x .< y =
--   (. x) . y
-- @
--
-- Here it allows you to compose a function on the second input of another function.
--
-- ==== __Examples__
--
-- If I want a function that adds two strings together separated by a @-@,
-- I can first use @(:)@ to add a comma to the second input and then combine the two.
--
-- >>> (('-' :) .< mp) "Hello" "world"
-- "Hello-world"
--
-- If I want the function:
--
-- @
-- \ x y -> x * (y + 1)
-- @
--
-- I can write it with @(.<)@ as:
--
-- >>> ((+1) .< (*)) 5 1
-- 10
--
(.<) ::
  ( Functor f
  , Functor g
  )
    => f a -> g (a -> b) -> g (f b)
(.<) =
  m' fm

-- | "Map contramap", a weird functor combinator.
--
-- Broadly speaking if we consider @(-> a)@ to be a contravariant functor for all @a@, the type can be generalized to
--
-- @
-- mcm ::
--   ( Functor f
--   , Contravariant g
--   )
--     => g (f a -> f b) -> g (a -> b)
-- @
--
-- Which makes it analygous to 'mm' but with the outer map being a contramap.
--
-- If we have the more categorical functor class,
-- we could also further generalize the type to:
--
-- @
-- mcm ::
--   ( Functor' p1 q1
--   , Functor' (->) q2
--   )
--     => q2 (p1 a b) (q1 a b)
-- @
--
-- In this sense it's just the map of the first functor instance lifted into the @q2@ category.
--
-- This also acts as a generalization of 'mm'.
--
-- ==== __Examples__
--
-- If we have a @filter@ function we can make another filter which has the opposite behavior (keeps what it discards, discards what it keeps):
--
-- @
-- negFilter =
--   mcm mcm filter n
-- @
--
-- Note that @mcm mcm@ is just 'mc2', so in this case that is better to use.
mcm ::
  ( Functor f
  )
    => ((f a -> f b) -> c) -> (a -> b) -> c
mcm =
  fm m

-- | Flip of 'mcm'
fMM ::
  ( Functor f
  )
    => (a -> b) -> ((f a -> f b) -> c) -> c
fMM =
  F mcm

-- | Maps across the argument of a function.
--
-- On functions this has the type:
--
-- @
-- (.!<) :: (a -> b -> c) -> (d -> b) -> (a -> d -> c)
-- @
--
-- And acts to compose across the second argument of a function.
--
-- ==== __Examples__
--
-- If we have a @filter@ function we can make another filter which has the opposite behavior (keeps what it discards, discards what it keeps):
--
-- @
-- negFilter =
--   mc2 filter n
-- @
mc2 ::
  ( Functor f
  )
    => (f b -> c) -> (a -> b) -> (f a -> c)
mc2 =
  mcm mcm

-- | Infix version of 'mc2'.
(.!<) ::
  ( Functor f
  )
    => (f b -> c) -> (a -> b) -> (f a -> c)
(.!<) =
  mc2

-- | Flip of 'mc2'.
fM2 ::
  ( Functor f
  )
    => (a -> b) -> (f b -> c) -> f a -> c
fM2 =
  F mc2

-- | A triple map.
-- Maps three levels deep into functors.
--
-- To multiply a number by three use @tp@.
m3 ::
  ( Functor f
  , Functor g
  , Functor h
  )
    => (a -> b) -> f (g (h a)) -> f (g (h b))
m3 =
  m' mm

-- | Infix of 'm3'
(.^^) ::
  ( Functor f
  , Functor g
  , Functor h
  )
    => (a -> b) -> f (g (h a)) -> f (g (h b))
(.^^) =
  m3

-- | Flip of 'm3'.
fm3 ::
  ( Functor f
  , Functor g
  , Functor h
  )
    => f (g (h a)) -> (a -> b) -> f (g (h b))
fm3 =
  F m3

-- | The "zigzag compose".
--
-- Takes a function and composes it on both ends of another.
zz ::
  (
  )
    => (a -> b) -> (b -> a) -> b -> a
zz =
  Control.Monad.ap m < m

-- | Flip of 'zz'.
fzz ::
  (
  )
    => (a -> b) -> (b -> a) -> a -> b
fzz =
  F zz

-- | Infix of 'zz' and 'fzz'.
--
-- Takes a function and composes it on both ends of another.
(<.<) ::
  (
  )
    => (a -> b) -> (b -> a) -> a -> b
(<.<) =
  fzz

-- | 'zz' on the second argument of a function
z2 ::
  ( Functor f
  )
    => f (a -> b) -> (b -> a) -> f (b -> a)
z2 =
  mof zz

-- | Flip of 'z2'.
fz2 ::
  ( Functor f
  )
    => (b -> a) -> f (a -> b) -> f (b -> a)
fz2 =
  F z2

-- | Infix of 'z2'.
(<.^) ::
  ( Functor f
  )
    => f (a -> b) -> (b -> a) -> f (b -> a)
(<.^) =
  z2

-- | Takes a function on two arguments and two functions to compose on each of its arguments.
dcp ::
  ( Functor f
  , Functor g
  )
    => (a -> b -> c) -> f a -> g b -> f (g c)
dcp =
  F < m' < F mX

-- | Flip of dcp
fdC ::
  ( Functor f
  , Functor g
  )
    => f a -> (a -> b -> c) -> g b -> f (g c)
fdC =
  F dcp
