{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language FlexibleInstances #-}
{-# Language UndecidableInstances #-}
module P.Functor.Identity
  ( Identity (..)
  , pattern UI
  , pattern Pu
  , pattern Li2
  , pattern UL2
  , pattern La2
  , pattern L2'
  , pattern Ul2
  )
  where

import qualified Prelude
import Prelude
  ( Traversable
  , Bounded
  , Num
  , Enum
  , Real
  , Integral
  )
import qualified Data.Functor.Classes

import P.Applicative
import P.Eq
import P.Foldable
import P.Function
import P.Function.Identity
import P.Functor.Bifunctor
import P.Functor.Compose
import P.Ord

-- | A class for functors which are equivalent to the identity functor.
--
-- These occur commonly in newtype wrappers.
class Applicative f => Identity f where
  fromId :: f a -> a

instance Identity ((->) ()) where
  fromId =
    ($ ())

{-
instance Identity f => Foldable f where
  foldMap =
    fm UI

instance (Functor f, Identity f) => Traversable f where
  sequenceA =
    m p < UI
-}

instance (Identity f, Eq a) => Eq (f a) where
  (==) =
    qb UI

instance (Identity f, Ord a) => Ord (f a) where
  compare =
    on cp UI

instance Identity f => Eq1 f where
  liftEq =
    Li2

instance Identity f => Ord1 f where
  liftCompare =
    Li2

instance (Bounded a, Identity f) => Bounded (f a) where
  maxBound =
    p Prelude.maxBound
  minBound =
    p Prelude.minBound

instance (Num a, Identity f) => Num (f a) where
  (+) =
    l2 (Prelude.+)
  (-) =
    l2 (Prelude.-)
  (*) =
    l2 (Prelude.*)
  abs =
    m Prelude.abs
  signum =
    m Prelude.signum
  fromInteger =
    p < Prelude.fromInteger

instance (Enum a, Identity f) => Enum (f a) where
  fromEnum =
    Prelude.fromEnum < UI
  toEnum =
    p < Prelude.toEnum

instance (Real a, Identity f) => Real (f a) where
  toRational =
    Prelude.toRational < UI

instance (Integral a, Identity f) => Integral (f a) where
  toInteger =
    Prelude.toInteger < UI
  quotRem =
    jB p .^ Li2 Prelude.quotRem

-- | A pattern of fromId.
pattern UI ::
  ( Identity f
  )
    => f a -> a
pattern UI x <- (p -> x) where
  UI =
    fromId

-- | A pattern for pure.
--
-- For the function use the shorter 'p'.
pattern Pu ::
  ( Identity f
  )
    => a -> f a
pattern Pu x <- (UI -> x) where
  Pu =
    p

-- | An internal function.
-- Use the exported 'UL2' instead.
_unLi2 ::
  ( Identity f
  , Identity g
  )
    => (f a -> g b -> c) -> a -> b -> c
_unLi2 func x y =
  func (p x) (p y)

{-# Complete Li2 #-}
-- | Lifts a function on two variables to act on identity functors.
pattern Li2 ::
  ( Identity f
  , Identity g
  )
    => (a -> b -> c) -> f a -> g b -> c
pattern Li2 x <- (_unLi2 -> x) where
  Li2 =
    fm UI < mX UI

{-# Complete UL2 #-}
-- | Takes a function on two Identity wrapped variables and converts it to one on unwrapped values.
pattern UL2 ::
  ( Identity f
  , Identity g
  )
    => (f a -> g b -> c) -> a -> b -> c
pattern UL2 x <- (Li2 -> x) where
  UL2 =
    _unLi2

-- | Specialized version of 
pattern Di2 ::
  ( Identity f
  )
    => (f a -> f b -> f c) -> a -> b -> f c
pattern Di2 x <- (Li2 -> x) where
  Di2 func x y =
    func (p x) (p y)

-- | Internal function.
_unLa2 ::
  ( Identity f
  , Identity g
  , Identity h
  )
    => (f a -> g b -> h c) -> a -> b -> c
_unLa2 func x y=
  UI $ func (p x) (p y)

{-# Complete La2 #-}
-- | A version of 'l2' which functions as a pattern match when the functor is an identity.
pattern La2 ::
  ( Identity f
  )
    => (a -> b -> c) -> f a -> f b -> f c
pattern La2 x <- (_unLa2 -> x) where
  La2 =
    l2

{-# Complete L2' #-}
-- | A generalized version of 'La2' which allows the three identity functors to be different.
-- This may require some coaxing to get the right types.
pattern L2' ::
  ( Identity f
  , Identity g
  , Identity h
  )
    => (a -> b -> c) -> f a -> g b -> h c
pattern L2' x <- (_unLa2 -> x) where
  L2' func x y =
    p $ func (UI x) (UI y)

{-# Complete Ul2 #-}
-- | The reverse of both 'La2' and 'L2''.
--
-- Takes a function on wrapped values and creates a function on unwrapped values.
pattern Ul2 ::
  ( Identity f
  , Identity g
  , Identity h
  )
    => (f a -> g b -> h c) -> a -> b -> c
pattern Ul2 x <- (L2' -> x) where
  Ul2 =
    _unLa2
