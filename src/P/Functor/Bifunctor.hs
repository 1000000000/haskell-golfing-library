{-# Language RankNTypes #-}
module P.Functor.Bifunctor
  ( Bifunctor
  , bm
  , fbm
  , jB
  , (&@)
  , fjB
  , pjB
  , fpJ
  , qjB
  , fqJ
  )
  where

import Data.Bifunctor

import P.Function.Flip

-- | Map over a bifunctor.
-- Equivalent to 'bimap'.
bm ::
  ( Bifunctor p
  )
    => (a -> b) -> (c -> d) -> p a c -> p b d
bm =
  bimap

-- | Flip of 'bm'.
fbm ::
  ( Bifunctor p
  )
    => (c -> d) -> (a -> b) -> p a c -> p b d
fbm =
  F bm

-- | Map over both parts of a bifunctor with the same function.
--
-- ==== __Examples__
--
-- This can be used on tuples to perform an action on both elements.
--
-- >>> jB (+3) (1, 2)
-- (5, 5)
--
jB ::
  ( Bifunctor p
  )
    => (a -> b) -> p a a -> p b b
jB func =
  bm func func

-- | Infix version of 'jB'.
(&@) ::
  ( Bifunctor p
  )
    => (a -> b) -> p a a -> p b b
(&@) =
  jB

-- | Flip of 'jB'.
fjB ::
  ( Bifunctor p
  )
    => p a a -> (a -> b) -> p b b
fjB =
  F jB

-- | If we define 'jB' as follows
--
-- @
-- jB func =
--   bm func func
-- @
--
-- Then in Haskell there is no single most general type for 'jB'.
-- 'jB' is defined with the type that Haskell infers by default.
-- 'pjB' is defined with a different potentially useful type.
pjB ::
  ( Bifunctor p
  )
    => (forall k . k -> f k) -> p a b -> p (f a) (f b)
pjB func =
  bm func func

-- | Flip of 'pjB'.
fpJ ::
  ( Bifunctor p
  )
    => p a b -> (forall k . k -> f k) -> p (f a) (f b)
-- Defined this way since 'F' doesn't play nice with RankNTypes until ghc supports impredictive polymorphism
fpJ func =
  (`pjB` func)


-- | A version of 'jB' with a different incompatible type.
-- See 'pjB' for an explanation.
qjB ::
  ( Bifunctor p
  )
    => (forall k . f k -> k) -> p (f a) (f b) -> p a b
qjB func =
  bm func func

-- | Flip of 'qjB'.
fqJ ::
  ( Bifunctor p
  )
    => p (f a) (f b) -> (forall k . f k -> k) -> p a b
-- Defined this way since 'F' doesn't play nice with RankNTypes until ghc supports impredictive polymorphism
fqJ func =
  (`qjB` func)
