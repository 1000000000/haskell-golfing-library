The haskell golfing library (also **hgl** or **P**) is a replacement for Haskell's Prelude library with the goal of making programs as small as possible for code golf.

Please do not use this for actual code.

## Building

To build this library or the documentation you will need stack.

You can play around with the library using

```
stack ghci
```

And you can build the documentation with:

```
stack haddock
```

To use this library in code add it as a dependency import it

```
import qualified Prelude
import P
```

You need to import Prelude qualified to prevent name clashes between the two.
